%module RTXBigUMesh

%{
/* Common */
#include "RTXBigUMesh/RTXBigUMesh.hxx"
#include "RTXBigUMesh/Tools/StaticFactory.hxx"
using namespace RTXBigUMesh;

/* Libraries */
#include "RTXBigUMesh/Libraries/GLFW.hxx"
using namespace Libraries;

/* Systems */
#include "RTXBigUMesh/Systems/RenderSystem.hxx"
using namespace Systems;

/* Components */
#include "RTXBigUMesh/Components/Entity.hxx"
#include "RTXBigUMesh/Components/Transform.hxx"
#include "RTXBigUMesh/Components/BUMesh.hxx"

%}

%feature("autodoc","4");
%feature("kwargs") Initialize;

/* Required on windows... */
%include <windows.i>

%include "exception.i"
%exception {
  try {
	$action
  } catch (const std::exception& e) {
	SWIG_exception(SWIG_RuntimeError, e.what());
  }
}

%include "attribute.i"

%include "std_deque.i"
%include "std_list.i"
%include "std_map.i"
%include "std_unordered_map.i"
%include "std_pair.i"
%include "std_set.i"
%include "std_string.i"
%include "std_vector.i"
%include "std_array.i"
%include "std_shared_ptr.i"

%include "stdint.i"

namespace std {
%template(UInt32Vector) vector<uint32_t>;
%template(UInt16Vector) vector<uint16_t>;
%template(UInt8Vector) vector<uint8_t>;

%template(Int32Vector) vector<int32_t>;
%template(Int16Vector) vector<int16_t>;
%template(Int8Vector) vector<int8_t>;

%template(DoubleVector) vector<double>;
%template(FloatVector) vector<float>;
%template(StringVector) vector<string>;

%template(IntSet) set<int>;
%template(DoubleSet) set<double>;
%template(FloatSet) set<float>;
%template(StringSet) set<string>;
};


%shared_ptr(StaticFactory)
%shared_ptr(Transform)
%shared_ptr(Entity)
%shared_ptr(BUMesh)
// %shared_ptr(Singleton)
// %shared_ptr(System)
// %shared_ptr(RenderSystem)


// Ignores
%nodefaultctor GLFW;
%nodefaultdtor GLFW;
%nodefaultctor System;
%nodefaultdtor System;
%nodefaultctor RenderSystem;
%nodefaultdtor RenderSystem;
%nodefaultctor EventSystem;
%nodefaultdtor EventSystem;

%nodefaultctor Entity;
%nodefaultdtor Entity;

%nodefaultctor Transform;
%nodefaultdtor Transform;

%nodefaultctor BUMesh;
%nodefaultdtor BUMesh;

%ignore CommandQueueItem;

%attribute(Entity, Transform*, transform, get_transform, set_transform);

%include "RTXBigUMesh/RTXBigUMesh.hxx"
%include "RTXBigUMesh/Tools/Singleton.hxx"
%include "RTXBigUMesh/Tools/System.hxx"
%include "RTXBigUMesh/Tools/StaticFactory.hxx"

// Libraries
%import "RTXBigUMesh/Libraries/GLM/glm.i"
%include "RTXBigUMesh/Libraries/GLFW.hxx";

// Systems
%include "RTXBigUMesh/Systems/RenderSystem.hxx"

// Components
%include "RTXBigUMesh/Components/Entity.hxx"
%include "RTXBigUMesh/Components/Transform.hxx"
%include "RTXBigUMesh/Components/BUMeshStruct.hxx"
%include "RTXBigUMesh/Components/BUMesh.hxx"


/* Representations */
%extend Entity {
	%feature("python:slot", "tp_repr", functype="reprfunc") __repr__;
	std::string __repr__() { return $self->to_string(); }
}
%extend Transform {
	%feature("python:slot", "tp_repr", functype="reprfunc") __repr__;
	std::string __repr__() { return $self->to_string(); }
}
%extend BUMesh {
	%feature("python:slot", "tp_repr", functype="reprfunc") __repr__;
	std::string __repr__() { return $self->to_string(); }
}

%template(TransformVector) vector<Transform*>;
%template(EntityVector) vector<Entity*>;
%template(BUMeshVector) vector<BUMesh*>;
%template(BUMeshPartInfoVector) vector<PartInfo>;
%template(BUMeshMultiLeafVector) vector<MultiLeaf>;
// %template(BUMeshMultiLeafVector2D) vector<vector<MultiLeaf>>;

// %extend Entity{
// 	%pythoncode %{		
// 		__swig_getmethods__["transform"] = get_transform
// 		__swig_setmethods__["transform"] = set_transform
// 		if _newclass: transform = property(get_transform, set_transform)
// 	%}
// };
