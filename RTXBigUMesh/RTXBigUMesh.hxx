#pragma once

namespace RTXBigUMesh {
    // Called by main executable
    void StartSystems();
    void StopSystems();

    // Called by python interpreter system
    void Initialize();
    void CleanUp();
}
