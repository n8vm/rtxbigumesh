#include <cuda_runtime_api.h>
#include <cuda.h>

#include "RenderSystem.hxx"
#include "PythonSystem.hxx"

#include "RTXBigUMesh/Libraries/GLFW.hxx"
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "ImGuizmo.h"
#include "imgui_knob.h"
// #include "imgui_rangeslider.h"
// #include "imguivariouscontrols.h"
#include "owl/owl.h"

#include "DeviceCode.hxx"

#include "RTXBigUMesh/Tools/ParallelFor.hxx"

#include <iostream>
#include <deque>
#include <atomic>
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image.h"
#include "stb_image_write.h"

#include <cuda_gl_interop.h>

#include "glm/gtx/string_cast.hpp"
#include "glm/gtc/matrix_access.hpp"

#include "RTXBigUMesh/Tools/Options.hxx"

#include "RTXBigUMesh/Tools/IO.hxx"

/* Embedded via cmake */
extern "C" char ptxCode[];

namespace Systems 
{
    std::condition_variable RenderSystem::cv;
    std::mutex RenderSystem::qMutex;
    std::queue<RenderSystem::Command> RenderSystem::commandQueue = {};

    RenderSystem* RenderSystem::Get() {
        static RenderSystem instance;
        return &instance;
    }

    bool RenderSystem::initialize() {
        if (initialized) return false;        
        initialized = true;
        render_thread_id = std::this_thread::get_id();

        context = owlContextCreate(/*requested Device IDs*/ nullptr, /* Num Devices */ 0);
        // owlContextSetRayTypeCount(context, 2); // for both "feeler" and query rays on the same accel.
        module = owlModuleCreate(context, ptxCode);
        
        // entitiesBuffer = owlManagedMemoryBufferCreate(context, OWL_USER_TYPE(EntityStruct), MAX_ENTITIES, nullptr);
        // transformsBuffer = owlManagedMemoryBufferCreate(context, OWL_USER_TYPE(TransformStruct), MAX_TRANSFORMS, nullptr);

        cudaDeviceSynchronize();

        camera_transform = Transform::Create("camera_transform");
        camera = Entity::Create("camera", camera_transform);
        bumesh_transform = Transform::Create("bumesh_transform");
        tri_mesh_transform = Transform::Create("tri_mesh_transform");

        LP.camera_entity = camera->get_struct();
        LP.camera_transform = camera_transform->get_struct();
        LP.bumesh_transform = bumesh_transform->get_struct();
        LP.tri_mesh_transform = tri_mesh_transform->get_struct();
        return true;
    }

    #define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
    inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
    {
        if (code != cudaSuccess) 
        {
            fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
            if (abort) throw std::runtime_error(std::string("CUDA error : file ") + std::string(file) + std::string(" line : ") + std::to_string(line) + std::string(" code: ") + cudaGetErrorString(code));
        }
    }

    void RenderSystem::set_bumesh_transform(Transform *transform)
    {
      bumesh_transform = transform;   
    }

    void RenderSystem::set_tri_mesh_transform(Transform *transform)
    {
      tri_mesh_transform = transform;   
    }

    static std::mutex print_mu;
    static void thread_safe_print(std::string text) {
        std::lock_guard<std::mutex> lock(print_mu);
        std::cout<<text;
    }

    void RenderSystem::set_bumesh(BUMesh *mesh, bool mirror)
    {
        auto setBUMesh = [this, mesh, mirror]() {
            /* Release any unstructured mesh that might be on card */
            if (bumesh != nullptr) {
            //     size_t num_parts = bumesh->get_part_list().size();
            //     for (size_t i = 0; i < num_parts; ++i) {
            //         auto test1 = LP.nodeListsBuffer[i];
            //         auto test2 = LP.vertexListsBuffer[i];
            //         auto test3 = LP.indexListsBuffer[i];
            //         gpuErrchk(cudaFree(test1));
            //         gpuErrchk(cudaFree(test2));
            //         gpuErrchk(cudaFree(test3));
            //     }
            //     gpuErrchk(cudaFree(LP.partInfoBuffer));
            //     gpuErrchk(cudaFree(LP.nodeListsBuffer));
            //     gpuErrchk(cudaFree(LP.vertexListsBuffer));
                // gpuErrchk(cudaFree(LP.indexListsBuffer));
                // owlBufferRelease(bumesh_part_info_buffer);
            }
            
            /* Malloc/fill updated unstructured mesh data */
            bumesh = mesh;

            // auto buffer = owlDeviceBufferCreate(context, OWL_USER_TYPE(uint64_t), 100000000, nullptr);
            // owlBufferRelease(buffer);
            
            // owlLaunchParamsSetBuffer(launchParams, "partInfoBuffer", bumesh_part_info_buffer);

            // ------------------------------------------------------------------
            // the group/accel for that mesh
            // ------------------------------------------------------------------
            
            /* "Parts" serve as the top level, then within each part are several "multi-leaf" primitives */
            /* Each multi-leaves contain several unstructured elements */
            glm::vec4 global_bbmin, global_bbmax;

            /* Part info buffer */
            auto &parts_list = bumesh->get_part_list();
            size_t num_parts = size_t(bumesh->get_part_list().size());
            auto parts_buffer = owlDeviceBufferCreate(context, OWL_USER_TYPE(PartInfo), num_parts, nullptr);
            // auto parts_buffer_ptr = (PartInfo*)owlBufferGetPointer(parts_buffer, 0);
            // memcpy(parts_buffer_ptr, parts_list.data(), parts_list.size() * sizeof(PartInfo));
            owlBufferUpload(parts_buffer, parts_list.data());

            /* Part accel */
            OWLGeom partsAccelGeom = owlGeomCreate(context, partsGeomType);
            owlGeomSetPrimCount(partsAccelGeom, num_parts);
            owlGeomSetBuffer(partsAccelGeom, "parts_buffer", parts_buffer);
            OWLGroup partsGroup = owlUserGeomGroupCreate(context, 1, &partsAccelGeom);
            owlGroupBuildAccel(partsGroup);
            auto partsAccel = owlInstanceGroupCreate(context, (mirror) ? 2 : 1);
            owlInstanceGroupSetChild(partsAccel, 0, partsGroup);
            if (mirror) {
                owlInstanceGroupSetChild(partsAccel, 1, partsGroup);
                owl4x3f tfm;
                //exajet kludge
                tfm.t = owl3f{0.f, 2555890.f, 0.f};
                tfm.vx = owl3f{1.f, 0.f, 0.f};
                tfm.vy = owl3f{0.f,-1.f, 0.f};
                tfm.vz = owl3f{0.f, 0.f, 1.f};
                owlInstanceGroupSetTransform(partsAccel, 1, &tfm);
            }
            owlGroupBuildAccel(partsAccel);
            owlLaunchParamsSetGroup(launchParams, "partsAccel", partsAccel);

            // compute global bounds of all parts, as well as transfered max value
            for (size_t i = 0; i < num_parts; ++i) {
                global_bbmin = (i == 0) ? parts_list[i].bounds_min : glm::min(parts_list[i].bounds_min, global_bbmin);
                global_bbmax = (i == 0) ? parts_list[i].bounds_max : glm::max(parts_list[i].bounds_max, global_bbmax);
            }

            if (mirror) {
                global_bbmax.y += (global_bbmax.y - global_bbmin.y);
                // global_bbmin.x = -global_bbmax.x;
                // global_bbmin.y = -global_bbmax.y;
                // global_bbmin.z = -global_bbmax.z;
            }
            
            if (bumesh->multileaves_loaded()) {
                uint32_t presplit_max = 1000;
                uint32_t num_presplit_groups = (num_parts + presplit_max - 1) / presplit_max;
                std::vector<OWLGroup> multiLeafGroups(num_presplit_groups);
                auto multiLeavesAccel = owlInstanceGroupCreate(context, (mirror) ? 2 * num_presplit_groups : num_presplit_groups);

                /* MultiLeaf accel */
                auto &multileaf_lists = bumesh->get_multileaf_lists();

                // note, may be empty if elements not loaded
                auto &index_lists = bumesh->get_index_lists();
                auto &vertex_lists = bumesh->get_vertex_lists();            
                
                /* For each presplit BLAS... */
                uint32_t start = 0, end = 0;
                std::atomic_int32_t count = 0;
                for (uint32_t bID = 0; bID < num_presplit_groups; ++bID) {
                    start = end;
                    end = std::min((uint32_t)start + presplit_max, (uint32_t)num_parts);
                    uint32_t num_parts_in_blas = end - start;

                    std::vector<OWLGeom> multiLeavesAccelGeom(num_parts_in_blas);
                    std::vector<OWLBuffer> leaf_qsmin_buffers(num_parts_in_blas);
                    std::vector<OWLBuffer> leaf_qsmax_buffers(num_parts_in_blas);
                    std::vector<OWLBuffer> leaf_qbbmin_buffers(num_parts_in_blas);
                    std::vector<OWLBuffer> leaf_qbbmax_buffers(num_parts_in_blas);

                    /* Create a blas level geometry for each part */                    
                    for (size_t i = 0; i < num_parts_in_blas; ++i) {
                        count++;
                        thread_safe_print(std::string("\r#rs: Uploading ") + std::to_string(float(count) / num_parts) + std::string("%"));

                        multiLeavesAccelGeom[i] = owlGeomCreate(context, multiLeavesGeomType);

                        /* Each part has one primitive per multi-leaf */
                        int num_prims = multileaf_lists[i + start].size();
                        if (num_prims == 0) {
                            std::cout<<"ERROR, part " << i + start << " has no multileaves in list?! Falling back to part bounds..."<< std::endl;
                            num_prims = 1;
                            MultiLeaf place_holder_multileaf;
                            for (int cID = 0; cID < MultiLeaf::WIDTH; ++cID) {
                                place_holder_multileaf.children.dim[0].child[cID].bounds_hi = 
                                place_holder_multileaf.children.dim[0].child[cID].bounds_hi = 0;
                            }
                            multileaf_lists[i + start].push_back(place_holder_multileaf);
                        }
                        
                        // shouldn't have to do this, but finding some (very few) parts have no multileaves...
                        owlGeomSetPrimCount(multiLeavesAccelGeom[i], std::max((uint64_t)multileaf_lists[i + start].size(), (uint64_t)1)); 
                        if (!bumesh->leaves_loaded()) {
                            /* Upload just the quantized bounds, and nothing else... */
                            leaf_qbbmin_buffers[i] = owlDeviceBufferCreate(context, OWL_USER_TYPE(glm::u16vec4), multileaf_lists[i + start].size(), nullptr);
                            leaf_qbbmax_buffers[i] = owlDeviceBufferCreate(context, OWL_USER_TYPE(glm::u16vec4), multileaf_lists[i + start].size(), nullptr);
                            leaf_qsmin_buffers[i] = owlDeviceBufferCreate(context, OWL_USER_TYPE(uint16_t), multileaf_lists[i + start].size(), nullptr);
                            leaf_qsmax_buffers[i] = owlDeviceBufferCreate(context, OWL_USER_TYPE(uint16_t), multileaf_lists[i + start].size(), nullptr);
                            // auto leaf_qsmin_buffer_ptr = (uint16_t*)owlBufferGetPointer(leaf_qsmin_buffers[i], 0);
                            // auto leaf_qsmax_buffer_ptr = (uint16_t*)owlBufferGetPointer(leaf_qsmax_buffers[i], 0);
                            // auto leaf_qbbmin_buffer_ptr = (glm::u16vec4*)owlBufferGetPointer(leaf_qbbmin_buffers[i], 0);
                            // auto leaf_qbbmax_buffer_ptr = (glm::u16vec4*)owlBufferGetPointer(leaf_qbbmax_buffers[i], 0);
                            std::vector<glm::u16vec4> leaf_qbbmin(multileaf_lists[i + start].size());
                            std::vector<glm::u16vec4> leaf_qbbmax(multileaf_lists[i + start].size());
                            std::vector<uint16_t> leaf_qsmin(multileaf_lists[i + start].size());
                            std::vector<uint16_t> leaf_qsmax(multileaf_lists[i + start].size());
                            for (int j = 0; j < num_prims; ++j) {
                                leaf_qbbmin[j] = multileaf_lists[i + start][j].quantBoundsMin;
                                leaf_qbbmax[j] = multileaf_lists[i + start][j].quantBoundsMax;
                                leaf_qsmin[j] = multileaf_lists[i + start][j].quantBoundsMin.w;
                                leaf_qsmax[j] = multileaf_lists[i + start][j].quantBoundsMax.w;
                            }
                            // memcpy(leaf_qsmin_buffer_ptr, leaf_qsmin.data(), leaf_qsmin.size() * sizeof(uint16_t));
                            // memcpy(leaf_qsmax_buffer_ptr, leaf_qsmax.data(), leaf_qsmax.size() * sizeof(uint16_t));
                            // memcpy(leaf_qbbmin_buffer_ptr, leaf_qbbmin.data(), leaf_qbbmin.size() * sizeof(glm::u16vec4));
                            // memcpy(leaf_qbbmax_buffer_ptr, leaf_qbbmax.data(), leaf_qbbmax.size() * sizeof(glm::u16vec4));
                            owlBufferUpload(leaf_qsmin_buffers[i], leaf_qsmin.data());
                            owlBufferUpload(leaf_qsmax_buffers[i], leaf_qsmax.data());
                            owlBufferUpload(leaf_qbbmin_buffers[i], leaf_qbbmin.data());
                            owlBufferUpload(leaf_qbbmax_buffers[i], leaf_qbbmax.data());
                            owlGeomSetBuffer(multiLeavesAccelGeom[i], "leaf_qbbmin", leaf_qbbmin_buffers[i]);
                            owlGeomSetBuffer(multiLeavesAccelGeom[i], "leaf_qbbmax", leaf_qbbmax_buffers[i]);
                            owlGeomSetBuffer(multiLeavesAccelGeom[i], "leaf_qsmin", leaf_qsmin_buffers[i]);
                            owlGeomSetBuffer(multiLeavesAccelGeom[i], "leaf_qsmax", leaf_qsmax_buffers[i]);
                        }
                        else if (!bumesh->elements_loaded()) {                            
                            /* Upload data needed for multileaves and leaves, but not for elements */
                            auto leaf_buffer = owlDeviceBufferCreate(context, OWL_USER_TYPE(MultiLeaf), multileaf_lists[i + start].size(), nullptr);
                            owlBufferUpload(leaf_buffer, multileaf_lists[i + start].data());
                            // auto leaf_buffer_ptr = (MultiLeaf*) owlBufferGetPointer(leaf_buffer, 0);
                            // memcpy(leaf_buffer_ptr, multileaf_lists[i + start].data(), multileaf_lists[i + start].size() * sizeof(MultiLeaf));
                            owlGeomSetBuffer(multiLeavesAccelGeom[i], "leaf_buffer", leaf_buffer);
                        } else {
                            /* Upload data needed for elements */
                            auto leaf_buffer = owlDeviceBufferCreate(context, OWL_USER_TYPE(MultiLeaf), multileaf_lists[i + start].size(), nullptr);
                            owlBufferUpload(leaf_buffer, multileaf_lists[i + start].data());
                            // auto leaf_buffer_ptr = (MultiLeaf*) owlBufferGetPointer(leaf_buffer, 0);
                            // memcpy(leaf_buffer_ptr, multileaf_lists[i + start].data(), multileaf_lists[i + start].size() * sizeof(MultiLeaf));
                            owlGeomSetBuffer(multiLeavesAccelGeom[i], "leaf_buffer", leaf_buffer);

                            auto index_buffer = owlManagedMemoryBufferCreate(context, OWL_USER_TYPE(glm::u16vec4), index_lists[i + start].size(), nullptr);
                            auto vertex_buffer = owlManagedMemoryBufferCreate(context, OWL_USER_TYPE(glm::vec4), vertex_lists[i + start].size(), nullptr);
                            auto index_buffer_ptr = (glm::u16vec4*) owlBufferGetPointer(index_buffer, 0);
                            auto vertex_buffer_ptr = (glm::vec4*) owlBufferGetPointer(vertex_buffer, 0);
                            
                            memcpy(index_buffer_ptr, index_lists[i + start].data(), index_lists[i + start].size() * sizeof(glm::u16vec4));
                            memcpy(vertex_buffer_ptr, vertex_lists[i + start].data(), vertex_lists[i + start].size() * sizeof(glm::vec4));
                            // owlBufferUpload(index_buffer, index_lists[i + start].data());
                            // owlBufferUpload(vertex_buffer, vertex_lists[i + start].data());
                            owlGeomSetBuffer(multiLeavesAccelGeom[i], "index_buffer",  index_buffer);
                            owlGeomSetBuffer(multiLeavesAccelGeom[i], "vertex_buffer", vertex_buffer);
                        }

                        uint32_t part_num = i + start;
                        owlGeomSetRaw(multiLeavesAccelGeom[i], "part_num", &part_num);
                        owlGeomSetRaw(multiLeavesAccelGeom[i], "parent_bbmin", &parts_list[i + start].bounds_min);
                        owlGeomSetRaw(multiLeavesAccelGeom[i], "parent_bbmax", &parts_list[i + start].bounds_max);
                    }

                    /* Build blas */
                    // std::cout<<"Building " << bID << std::endl;
                    multiLeafGroups[bID] = owlUserGeomGroupCreate(context, multiLeavesAccelGeom.size(), multiLeavesAccelGeom.data());
                    owlGroupBuildAccel(multiLeafGroups[bID]);
                    owlInstanceGroupSetChild(multiLeavesAccel, bID, multiLeafGroups[bID]);
                    
                    if (mirror) {
                        owlInstanceGroupSetChild(multiLeavesAccel, bID + num_presplit_groups, multiLeafGroups[bID]);
                        owl4x3f tfm;
                        tfm.t = owl3f{0.f, 2555890.f, 0.f};
                        tfm.vx = owl3f{1.f, 0.f, 0.f};
                        tfm.vy = owl3f{0.f,-1.f, 0.f};
                        tfm.vz = owl3f{0.f, 0.f, 1.f};
                        owlInstanceGroupSetTransform(multiLeavesAccel, bID + num_presplit_groups, &tfm);
                    }

                    /* Free any unneeded data */
                    if (!bumesh->leaves_loaded()) {
                        for (int i = 0; i < num_parts_in_blas; ++i) {
                            owlBufferRelease(leaf_qbbmin_buffers[i]);
                            owlBufferRelease(leaf_qbbmax_buffers[i]);                       
                        }
                    }
                }
                
                /* Now build TLAS */
                owlGroupBuildAccel(multiLeavesAccel);
                owlLaunchParamsSetGroup(launchParams, "multiLeavesAccel", multiLeavesAccel);
            }

            if (bumesh->triangle_mesh_loaded()) {
                auto &mesh_indices = bumesh->get_triangle_mesh_indices();
                auto &mesh_vertices = bumesh->get_triangle_mesh_vertices();
                auto &mesh_colors = bumesh->get_triangle_mesh_colors();
                for (int i = 0 ; i < mesh_vertices.size(); ++i) {
                    LP.tri_mesh_bbmin = (i == 0) ? glm::vec4(mesh_vertices[i], 0.f) : glm::min(glm::vec4(mesh_vertices[i], 0.f), LP.tri_mesh_bbmin);
                    LP.tri_mesh_bbmax = (i == 0) ? glm::vec4(mesh_vertices[i], 0.f) : glm::max(glm::vec4(mesh_vertices[i], 0.f), LP.tri_mesh_bbmax);
                }

                std::cout << "tri mesh min max " << glm::to_string(LP.tri_mesh_bbmin)
                        << " " << glm::to_string(LP.tri_mesh_bbmax) << std::endl;


                // LP.tri_mesh_bbmin = glm::vec4(1232128.f, 1259072.f, 1238336.f, 0.f);
                // LP.tri_mesh_bbmax = glm::vec4(1270848.f, 1277952.f, 1255296.f, 0.f);

                OWLBuffer vertexBuffer = owlDeviceBufferCreate(context,OWL_FLOAT3, mesh_vertices.size(), mesh_vertices.data());
                OWLBuffer indexBuffer = owlDeviceBufferCreate(context,OWL_INT3, mesh_indices.size(), mesh_indices.data());
                OWLBuffer colorsBuffer; if (mesh_colors.size() > 0) colorsBuffer = owlDeviceBufferCreate(context,OWL_FLOAT3, mesh_colors.size(), mesh_colors.data());

                OWLGeom trianglesGeom = owlGeomCreate(context,trianglesGeomType);

                owlTrianglesSetVertices(trianglesGeom,vertexBuffer,
                                    mesh_vertices.size(),sizeof(glm::vec3),0);
                owlTrianglesSetIndices(trianglesGeom,indexBuffer,
                                    mesh_indices.size(),sizeof(glm::ivec3),0);

                owlGeomSetBuffer(trianglesGeom,"vertex",vertexBuffer);
                owlGeomSetBuffer(trianglesGeom,"index",indexBuffer);
                owlGeomSetBuffer(trianglesGeom,"colors",(mesh_colors.size() > 0) ? colorsBuffer : nullptr);
                // owlGeomSet3f(trianglesGeom,"color",owl3f{0,1,0});

                // ------------------------------------------------------------------
                // the group/accel for that mesh
                // ------------------------------------------------------------------
                OWLGroup trianglesGroup = owlTrianglesGeomGroupCreate(context,1,&trianglesGeom);
                owlGroupBuildAccel(trianglesGroup);
                // mirror 
                OWLGroup world = owlInstanceGroupCreate(context, (mirror) ? 2 : 1);
                owlInstanceGroupSetChild(world, 0, trianglesGroup);
                if (mirror) {
                    owlInstanceGroupSetChild(world, 1, trianglesGroup);
                    owl4x3f tfm;
                    tfm.t = owl3f{0.f, 2555890.f, 0.f};
                    tfm.vx = owl3f{1.f, 0.f, 0.f};
                    tfm.vy = owl3f{0.f,-1.f, 0.f};
                    tfm.vz = owl3f{0.f, 0.f, 1.f};
                    owlInstanceGroupSetTransform(world, 1, &tfm);
                }

                owlGroupBuildAccel(world);        
                owlLaunchParamsSetGroup(launchParams, "world", world);
            } else {
                LP.tri_mesh_bbmin = LP.tri_mesh_bbmax  = vec4(0.f);
            }

            // Some other useful data
            LP.bumesh_bbmin = global_bbmin;
            LP.bumesh_bbmax = global_bbmax;
            owlLaunchParamsSetRaw(launchParams, "bumesh_bbmin", &global_bbmin);
            owlLaunchParamsSetRaw(launchParams, "bumesh_bbmax", &global_bbmax);
            owlLaunchParamsSetRaw(launchParams, "tri_mesh_bbmin", &LP.tri_mesh_bbmin);
            owlLaunchParamsSetRaw(launchParams, "tri_mesh_bbmax", &LP.tri_mesh_bbmax);
            owlBuildSBT(context);

            // Initialize space skipping / adaptive sampling stuff
            glm::vec4 temp = glm::vec4(0.f);
            partMinColorBuffer = owlManagedMemoryBufferCreate(context, OWL_USER_TYPE(glm::vec4), num_parts, nullptr);
            partMaxColorBuffer = owlManagedMemoryBufferCreate(context, OWL_USER_TYPE(glm::vec4), num_parts, nullptr);
            partColorVarBuffer = owlManagedMemoryBufferCreate(context, OWL_USER_TYPE(glm::vec4), num_parts, nullptr);
            owlLaunchParamsSetBuffer(launchParams, "part_min_color", partMinColorBuffer);
            owlLaunchParamsSetBuffer(launchParams, "part_max_color", partMaxColorBuffer);
            owlLaunchParamsSetBuffer(launchParams, "part_var_color", partColorVarBuffer);
            owlLaunchParamsSetRaw(launchParams, "max_color", &temp);
            LP.part_min_color = (glm::vec4*)owlBufferGetPointer(partMinColorBuffer, 0);
            LP.part_max_color = (glm::vec4*)owlBufferGetPointer(partMaxColorBuffer, 0);
            LP.part_var_color = (glm::vec4*)owlBufferGetPointer(partColorVarBuffer, 0);
            update_adaptive_sampling_metadata();

            // Resize volume to fit on screen 
            // glm::vec4 lengths = global_bbmax - global_bbmin;
            // float max_side_length = std::max(lengths.x, std::max(lengths.y, lengths.z));
            // bumesh_transform->set_scale(2.f / max_side_length);

            LP.min_step_size = .01;
            LP.max_step_size = .05;
            std::cout << "min/max: " << global_bbmin.w << " " << global_bbmax.w << std::endl;
        };

        /* May be called asynchronously by python. Always run above code on main thread */
        if (render_thread_id == std::this_thread::get_id()) 
            setBUMesh();
        else {
            auto future = enqueueCommand(setBUMesh);
            future.wait();
        }
    }

    std::mutex as_mutex;
    void RenderSystem::update_adaptive_sampling_metadata()
    {
        if (!bumesh) return;

        auto &parts_list = bumesh->get_part_list();
        LP.part_min_color = (glm::vec4*)owlBufferGetPointer(partMinColorBuffer, 0);
        LP.part_max_color = (glm::vec4*)owlBufferGetPointer(partMaxColorBuffer, 0);
        LP.part_var_color = (glm::vec4*)owlBufferGetPointer(partColorVarBuffer, 0);

        glm::vec4 max_color_variance = glm::vec4(0.f);
        glm::vec4 max_color = glm::vec4(0.f);
        parallel_for(parts_list.size(),[&](int partID) {
            float data_min = parts_list[partID].bounds_min.w;
            float data_max = parts_list[partID].bounds_max.w;

            data_min /= LP.bumesh_bbmax.w;
            data_max /= LP.bumesh_bbmax.w;

            int lowaddr, highaddr;

            /* First map out where the low and high data values go in the tfr function */ 
            int lowMinAddr = -1, highMinAddr = -1;
            float minAlpha = -1.f;
            lookupTableAddrs(
                data_min, 
                LP.transferFunction, 
                LP.transferFunctionWidth,
                LP.transferFunctionMin,
                LP.transferFunctionMax,
                lowMinAddr, highMinAddr, minAlpha
            );
            lowaddr = lowMinAddr;

            int lowMaxAddr = -1, highMaxAddr = -1;
            float maxAlpha = -1.f;
            lookupTableAddrs(
                data_max, 
                LP.transferFunction, 
                LP.transferFunctionWidth,
                LP.transferFunctionMin,
                LP.transferFunctionMax,
                lowMaxAddr, highMaxAddr, maxAlpha
            );
            highaddr = highMaxAddr;

            if ((lowMinAddr == -1) || (highMinAddr == -1)) return; // bad transfer function...

            /* Depending on the lower/upper bounds of the transfer function transform, these may need to swap */
            if (highaddr < lowaddr) std::swap(highaddr, lowaddr);

            /* Now, sweep through the transfer function, computing the max lookup value through the domain */
            glm::vec4 color_low, color_high, color_variance;
            int n = 0; glm::vec4 sum = glm::vec4(0.f); glm::vec4 sumsq = glm::vec4(0.f);
            for (int i = lowaddr; i <= highaddr; ++i) {
                n = n + 1;
                glm::vec4 color;
                if (i == lowaddr) {
                    color = glm::mix(LP.transferFunction[lowMinAddr], LP.transferFunction[highMinAddr], minAlpha);
                } else if (i == highaddr) {
                    color = glm::mix(LP.transferFunction[lowMaxAddr], LP.transferFunction[highMaxAddr], maxAlpha);
                    
                } else {
                    color = LP.transferFunction[i];
                }
                sum = sum + color;
                sumsq = sumsq + color * color;
                color_variance = (sumsq - (sum * sum) / float(n)) / float(n-1);
                color_low  = (i == lowaddr) ? color : glm::min(color_low , color);
                color_high = (i == lowaddr) ? color : glm::max(color_high, color);
            }

            /* Store the min/max colors on the part */
            LP.part_max_color[partID] = color_high;
            LP.part_min_color[partID] = color_low;
            LP.part_var_color[partID] = color_variance;
            
            {
                std::lock_guard<std::mutex> lock(as_mutex);
                max_color_variance = glm::max(max_color_variance, color_variance);
                max_color = glm::max(color_high, max_color);
            }
		});

        // normalize variance
        parallel_for(parts_list.size(),[&](int partID) {
            LP.part_var_color[partID] /= glm::vec4(max_color_variance);
        });

        owlLaunchParamsSetRaw(launchParams,  "max_color", &max_color);
        reset_accumulation();
    }

    bool RenderSystem::cleanup() {
        if (!initialized) return false;
        initialized = false;
        owlModuleRelease(module);
        owlContextDestroy(context);
        // cudaFree(LP.entities);
        // cudaFree(LP.transforms);
        return true;
    }

    void RenderSystem::process_command_queue()
    {
        std::lock_guard<std::mutex> lock(qMutex);
        while (!commandQueue.empty()) {
            auto item = commandQueue.front();
            item.function();
            try {
                item.promise->set_value();
            }
            catch (std::future_error& e) {
                if (e.code() == std::make_error_condition(std::future_errc::promise_already_satisfied))
                    std::cout << "RenderSystem: [promise already satisfied]\n";
                else
                    std::cout << "RenderSystem: [unknown exception]\n";
            }
            commandQueue.pop();
        }
    }

    void RenderSystem::update_launch_params()
    {
        glfwGetFramebufferSize(window, &curr_frame_size.x, &curr_frame_size.y);
        // const vec3f lookFrom(-4.f,-3.f,-2.f);
        // const vec3f lookAt(0.f,0.f,0.f);
        // const vec3f lookUp(0.f,1.f,0.f);
        // const float cosFovy = 0.66f;
        LP.view = camera_controls.transform();//glm::lookAt(glm::vec3(-4.f, -3.f, -2.f), glm::vec3(0.f), glm::vec3(0.f, 1.f, 0.f));
        LP.proj = glm::perspective(glm::radians(45.f), float(curr_frame_size.x) / float(curr_frame_size.y), 1.0f, 1000.0f);
        LP.viewinv = glm::inverse(LP.view);
        LP.projinv = glm::inverse(LP.proj);
        // owlBufferUpload(frameStateBuffer, &LP);

        auto cam = camera->get_struct();
        auto cam_transform = camera_transform->get_struct();
        owlLaunchParamsSetRaw(launchParams,"camera_entity",&cam);
        owlLaunchParamsSetRaw(launchParams,"camera_transform",&cam_transform);
        // owlLaunchParamsSetBuffer(launchParams,"entities",entitiesBuffer);
        // owlLaunchParamsSetBuffer(launchParams,"transforms",transformsBuffer);

        owlLaunchParamsSetRaw(launchParams,"view",&LP.view);
        owlLaunchParamsSetRaw(launchParams,"proj",&LP.proj);
        owlLaunchParamsSetRaw(launchParams,"viewinv",&LP.viewinv);
        owlLaunchParamsSetRaw(launchParams,"projinv",&LP.projinv);
        owlLaunchParamsSetRaw(launchParams,"frame_size",&LP.frame_size);
        owlLaunchParamsSetRaw(launchParams,"frame",&LP.frame); 
        owlLaunchParamsSetRaw(launchParams,"startFrame",&LP.startFrame); 
        owlLaunchParamsSetRaw(launchParams,"reset",&LP.reset); 
        owlLaunchParamsSetRaw(launchParams,"enable_pathtracer",&LP.enable_pathtracer); 
        owlLaunchParamsSetRaw(launchParams,"enable_space_skipping",&LP.enable_space_skipping); 
        owlLaunchParamsSetRaw(launchParams,"enable_adaptive_sampling",&LP.enable_adaptive_sampling); 
        owlLaunchParamsSetRaw(launchParams,"enable_id_colors",&LP.enable_id_colors); 
        owlLaunchParamsSetRaw(launchParams,"mirror",&LP.mirror); 
        owlLaunchParamsSetRaw(launchParams,"zoom",&LP.zoom); 
        owlLaunchParamsSetRaw(launchParams,"min_step_size",&LP.min_step_size); 
        owlLaunchParamsSetRaw(launchParams,"max_step_size",&LP.max_step_size); 
        owlLaunchParamsSetRaw(launchParams,"adaptive_power",&LP.adaptive_power); 
        owlLaunchParamsSetRaw(launchParams,"attenuation",&LP.attenuation); 
        owlLaunchParamsSetRaw(launchParams,"opacity",&LP.opacity); 
        owlLaunchParamsSetRaw(launchParams,"volume_type",&LP.volume_type); 
        owlLaunchParamsSetRaw(launchParams,"show_time_heatmap",&LP.show_time_heatmap); 
        owlLaunchParamsSetRaw(launchParams,"show_samples_heatmap",&LP.show_samples_heatmap); 
        owlLaunchParamsSetRaw(launchParams,"empty_threshold",&LP.empty_threshold);        
        owlLaunchParamsSetRaw(launchParams,"time_min",&LP.time_min);        
        owlLaunchParamsSetRaw(launchParams,"time_max",&LP.time_max);        
        owlLaunchParamsSetRaw(launchParams,"tri_mesh_color",&LP.tri_mesh_color);        
        owlLaunchParamsSetRaw(launchParams,"background_color",&LP.background_color);    
        owlLaunchParamsSetRaw(launchParams, "transferFunctionMin", &LP.transferFunctionMin);
        owlLaunchParamsSetRaw(launchParams, "transferFunctionMax", &LP.transferFunctionMax);
        owlLaunchParamsSetRaw(launchParams, "transferFunctionWidth", &LP.transferFunctionWidth);

        auto bumesh_transform_struct = bumesh_transform->get_struct();
        owlLaunchParamsSetRaw(launchParams,"bumesh_transform",&bumesh_transform_struct);

        auto tri_mesh_transform_struct = tri_mesh_transform->get_struct();
        owlLaunchParamsSetRaw(launchParams,"tri_mesh_transform",&tri_mesh_transform_struct);
    }

    void RenderSystem::update_components() {
        Entity::UpdateComponents();
        Transform::UpdateComponents();

        // EntityStruct* device_entity_structs = (EntityStruct*) owlBufferGetPointer(entitiesBuffer, 0);
        // TransformStruct* device_transform_structs = (TransformStruct*) owlBufferGetPointer(transformsBuffer, 0);
        
        // Entity* host_entities = Entity::GetFront();
        // EntityStruct* host_entity_structs = Entity::GetFrontStruct();
        // Transform* host_transforms = Transform::GetFront();
        // TransformStruct* host_transform_structs = Transform::GetFrontStruct();
        
        // /* Update dirty components */
        // for (uint32_t i = 0; i < Entity::GetCount(); ++i) {
        //     if (host_entities[i].is_dirty()) {
        //         device_entity_structs[i] = host_entity_structs[i];
        //         host_entities[i].mark_clean();
        //     }
        // }
        // for (uint32_t i = 0; i < Transform::GetCount(); ++i) {
        //     if (host_transforms[i].is_dirty()) {
        //         device_transform_structs[i] = host_transform_structs[i];
        //         host_transforms[i].mark_clean();
        //     }
        // }
    }

    void RenderSystem::render_optix() {
        glfwGetFramebufferSize(window, &curr_frame_size.x, &curr_frame_size.y);

        if ((curr_frame_size.x != last_frame_size.x) || (curr_frame_size.y != last_frame_size.y))  {
            last_frame_size.x = curr_frame_size.x; last_frame_size.y = curr_frame_size.y;
            LP.frame_size = curr_frame_size;
            RenderSystem::initialize_framebuffer(curr_frame_size.x, curr_frame_size.y);
            owlBufferResize(frameBuffer, curr_frame_size.x*curr_frame_size.y);
            owlBufferResize(accumBuffer, curr_frame_size.x*curr_frame_size.y);
        }
        
        static int count = 0;
        static double start=0;
        static double stop=0;

        /* Trace Rays */
        start = glfwGetTime();
        owlParamsLaunch2D(rayGen, curr_frame_size.x, curr_frame_size.y, launchParams);
        cudaGraphicsMapResources(1, &cudaResourceTex);
        const void* fbdevptr = owlBufferGetPointer(frameBuffer,0);
        cudaArray_t array;
        cudaGraphicsSubResourceGetMappedArray(&array, cudaResourceTex, 0, 0);
        cudaMemcpyToArray(array, 0, 0, fbdevptr, curr_frame_size.x * curr_frame_size.y  * sizeof(glm::vec4), cudaMemcpyDeviceToDevice);
        cudaGraphicsUnmapResources(1, &cudaResourceTex);

        for (int i = 0; i < owlGetDeviceCount(context); i++) {
            cudaSetDevice(i);
            cudaDeviceSynchronize();
            cudaError_t err = cudaPeekAtLastError();
            if (err != 0) {
                std::cout<< "ERROR: " << cudaGetErrorString(err)<<std::endl;
                throw std::runtime_error("ERROR");
            }
        }
        stop = glfwGetTime();
        glfwSetWindowTitle(window, std::to_string(1.f / (stop - start)).c_str());

        // Draw pixels from optix frame buffer
        glViewport(0, 0, curr_frame_size.x, curr_frame_size.y);
       
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
            
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0.0, 1.0, 0.0, 1.0, 0.0, 1.0);
                
        glDisable(GL_DEPTH_TEST);
        
        glBindTexture(GL_TEXTURE_2D, imageTexID);

        // This is incredibly slow, but does not require interop
        // glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, windowSize.x, windowSize.y, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
        
        // Draw texture to screen via immediate mode
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, imageTexID);

        glBegin(GL_QUADS);
        glTexCoord2f( 0.0f, 0.0f );
        glVertex2f  ( 0.0f, 0.0f );

        glTexCoord2f( 1.0f, 0.0f );
        glVertex2f  ( 1.0f, 0.0f );

        glTexCoord2f( 1.0f, 1.0f );
        glVertex2f  ( 1.0f, 1.0f );

        glTexCoord2f( 0.0f, 1.0f );
        glVertex2f  ( 0.0f, 1.0f );
        glEnd();

        glDisable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

        LP.frame = LP.frame % 1000;
        LP.frame++;
        LP.startFrame++;
    }

    void RenderSystem::initialize_framebuffer(int fb_width, int fb_height) {
        if (imageTexID != -1) {
            cudaGraphicsUnregisterResource(cudaResourceTex);
        }
        
        // Enable Texturing
        glEnable(GL_TEXTURE_2D);
        // Generate a Texture ID for the framebuffer
        glGenTextures(1, &imageTexID);
        // Make this teh current texture
        glBindTexture(GL_TEXTURE_2D, imageTexID);
        // Allocate the texture memory. The last parameter is NULL since we only 
        // want to allocate memory, not initialize it.
        glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, fb_width, fb_height);
        
        // Must set the filter mode, GL_LINEAR enables interpolation when scaling
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        //Registration with CUDA
        cudaGraphicsGLRegisterImage(&cudaResourceTex, imageTexID, GL_TEXTURE_2D, cudaGraphicsRegisterFlagsNone);
    }

    //-----------------------------------------------------------------------------
    // [SECTION] Example App: Debug Console / ShowExampleAppConsole()
    //-----------------------------------------------------------------------------

    // Demonstrate creating a simple console window, with scrolling, filtering, completion and history.
    // For the console example, here we are using a more C++ like approach of declaring a class to hold the data and the functions.
    struct ExampleAppConsole
    {
        char                              InputBuf[256];
        ImVector<char*>                   Items;
        ImVector<const char*>             Commands;
        ImVector<char*>                   History;
        int                               HistoryPos;    // -1: new line, 0..History.Size-1 browsing history.
        ImGuiTextFilter                   Filter;
        bool                              AutoScroll;
        bool                              ScrollToBottom;
        std::deque<PythonResult>          CommandResults;

        ExampleAppConsole()
        {
            ClearLog();
            memset(InputBuf, 0, sizeof(InputBuf));
            HistoryPos = -1;
            Commands.push_back("HELP");
            Commands.push_back("HISTORY");
            Commands.push_back("CLEAR");
            Commands.push_back("CLASSIFY");  // "classify" is only here to provide an example of "C"+[tab] completing to "CL" and displaying matches.
            AutoScroll = true;
            ScrollToBottom = false;


            if (Options::IsBUMeshSet()) {
                ExecCommand("import glm");
                ExecCommand("import RTXBigUMesh");
                ExecCommand("from RTXBigUMesh import RenderSystem");
                ExecCommand("from RTXBigUMesh import Entity");
                ExecCommand("from RTXBigUMesh import Transform");
                ExecCommand("from RTXBigUMesh import BUMesh");
                ExecCommand("rs = RenderSystem.Get()");
                ExecCommand("camera = Entity.Get(\"camera\")");
                ExecCommand("bumesh_transform = Transform.Get(\"bumesh_transform\")");
                ExecCommand("tri_mesh_transform = Transform.Get(\"tri_mesh_transform\")");
                std::string command = "bumesh = BUMesh.CreateFromDir(\"mesh\",\"";
                command += Options::GetBUMeshPath();
                command += "\", read_triangle_mesh=True, read_multileaves=True, read_leaves=True, read_elements=False, max_parts=1000)";
                ExecCommand(command.c_str());
                ExecCommand("rs.set_bumesh(bumesh)");
            } else if (Options::IsMainModuleSet()) {
                /**/
            }
            else {
                ExecCommand("import glm");
                ExecCommand("import RTXBigUMesh");
                ExecCommand("from RTXBigUMesh import RenderSystem");
                ExecCommand("from RTXBigUMesh import Entity");
                ExecCommand("from RTXBigUMesh import Transform");
                ExecCommand("from RTXBigUMesh import BUMesh");
                ExecCommand("rs = RenderSystem.Get()");
                ExecCommand("camera = Entity.Get(\"camera\")");
                ExecCommand("bumesh_transform = Transform.Get(\"bumesh_transform\")");
                ExecCommand("tri_mesh_transform = Transform.Get(\"tri_mesh_transform\")");
                // ExecCommand("bumesh = BUMesh.CreateFromDir(\"mesh\",\"/space/bigmesh/fun3d/fun3d-optix\", read_triangle_mesh=True, read_multileaves=True, read_leaves=True, read_elements=False)");
                // ExecCommand("bumesh = BUMesh.CreateFromDir(\"mesh\",\"/space/bigmesh/fun3d_turb/\", read_triangle_mesh=True, read_multileaves=True, read_leaves=True, read_elements=True)");
                ExecCommand("bumesh = BUMesh.CreateFromDir(\"mesh\",\"/space/bigmesh/exajet/\", read_triangle_mesh=True, read_multileaves=True, read_leaves=True, read_elements=True)");
                // ExecCommand("bumesh = BUMesh.CreateFromDir(\"mesh\",\"/space/bigmesh/full_ocean/\", read_triangle_mesh=True, read_multileaves=True, read_leaves=True, read_elements=True)");
                // ExecCommand("bumesh = BUMesh.CreateFromDir(\"mesh\",\"./test_data/example_planar_wedge/\", read_triangle_mesh=False, read_multileaves=True, read_leaves=True, read_elements=True)");
                // ExecCommand("bumesh = BUMesh.CreateFromDir(\"mesh\",\"./test_data/agulhas/\", read_triangle_mesh=True, read_multileaves=True, read_leaves=True, read_elements=True)");
                // ExecCommand("bumesh = BUMesh.CreateFromDir(\"mesh\",\"./test_data/fun3d/fun3d-optix\", read_triangle_mesh=True, read_multileaves=True, read_leaves=True, read_elements=False, max_parts=1000)");
                ExecCommand("rs.set_bumesh(bumesh)");
            }

 

            // AddLog("Welcome to Dear ImGui!");
        }
        ~ExampleAppConsole()
        {
            ClearLog();
            for (int i = 0; i < History.Size; i++)
                free(History[i]);
        }

        // Portable helpers
        static int   Stricmp(const char* str1, const char* str2)         { int d; while ((d = toupper(*str2) - toupper(*str1)) == 0 && *str1) { str1++; str2++; } return d; }
        static int   Strnicmp(const char* str1, const char* str2, int n) { int d = 0; while (n > 0 && (d = toupper(*str2) - toupper(*str1)) == 0 && *str1) { str1++; str2++; n--; } return d; }
        static char* Strdup(const char *str)                             { size_t len = strlen(str) + 1; void* buf = malloc(len); IM_ASSERT(buf); return (char*)memcpy(buf, (const void*)str, len); }
        static void  Strtrim(char* str)                                  { char* str_end = str + strlen(str); while (str_end > str && str_end[-1] == ' ') str_end--; *str_end = 0; }

        void    ClearLog()
        {
            for (int i = 0; i < Items.Size; i++)
                free(Items[i]);
            Items.clear();
        }

        void    Interupt()
        {
            auto ps = PythonSystem::Get(); ps->interupt();
            // AddLog("Traceback: Kernel Interupted");
        }

        void    AddLog(const char* fmt, ...) IM_FMTARGS(2)
        {
            // FIXME-OPT
            char buf[1024];
            va_list args;
            va_start(args, fmt);
            vsnprintf(buf, IM_ARRAYSIZE(buf), fmt, args);
            buf[IM_ARRAYSIZE(buf)-1] = 0;
            va_end(args);
            Items.push_back(Strdup(buf));
        }

        void    Draw(const char* title, bool* p_open)
        {          
            if (ImGui::CollapsingHeader("Console")) { 
                // ImGui::SetNextWindowSize(ImVec2(520,600), ImGuiCond_FirstUseEver);
                // if (!ImGui::Begin(title, p_open))
                // {
                //     ImGui::End();
                //     return;
                // }

                // As a specific feature guaranteed by the library, after calling Begin() the last Item represent the title bar. So e.g. IsItemHovered() will return true when hovering the title bar.
                // Here we create a context menu only available from the title bar.
                if (ImGui::BeginPopupContextItem())
                {
                    if (ImGui::MenuItem("Close Console"))
                        *p_open = false;
                    ImGui::EndPopup();
                }

                // ImGui::TextWrapped("This example implements a console with basic coloring, completion and history. A more elaborate implementation may want to store entries along with extra data such as timestamp, emitter, etc.");
                // ImGui::TextWrapped("Enter 'HELP' for help, press TAB to use text completion.");

                // TODO: display items starting from the bottom

                // if (ImGui::SmallButton("Add Dummy Text"))  { AddLog("%d some text", Items.Size); AddLog("some more text"); AddLog("display very important message here!"); } ImGui::SameLine();
                // if (ImGui::SmallButton("Add Dummy Error")) { AddLog("[error] something went wrong"); } ImGui::SameLine();
                if (ImGui::SmallButton("Clear")) { ClearLog(); } ImGui::SameLine();
                if (ImGui::SmallButton("Interupt Kernel")) { Interupt(); } ImGui::SameLine();
                bool copy_to_clipboard = ImGui::SmallButton("Copy");
                //static float t = 0.0f; if (ImGui::GetTime() - t > 0.02f) { t = ImGui::GetTime(); AddLog("Spam %f", t); }

                ImGui::Separator();

                // Options menu
                if (ImGui::BeginPopup("Options"))
                {
                    ImGui::Checkbox("Auto-scroll", &AutoScroll);
                    ImGui::EndPopup();
                }

                // Options, Filter
                if (ImGui::Button("Options"))
                    ImGui::OpenPopup("Options");
                ImGui::SameLine();
                Filter.Draw("Filter (\"incl,-excl\") (\"error\")", 180);
                ImGui::Separator();

                const float footer_height_to_reserve = ImGui::GetStyle().ItemSpacing.y + ImGui::GetFrameHeightWithSpacing(); // 1 separator, 1 input text
                ImGui::BeginChild("ScrollingRegion", ImVec2(0, -footer_height_to_reserve), false, ImGuiWindowFlags_HorizontalScrollbar); // Leave room for 1 separator + 1 InputText
                if (ImGui::BeginPopupContextWindow())
                {
                    if (ImGui::Selectable("Clear")) ClearLog();
                    ImGui::EndPopup();
                }

                // Display every line as a separate entry so we can change their color or add custom widgets. If you only want raw text you can use ImGui::TextUnformatted(log.begin(), log.end());
                // NB- if you have thousands of entries this approach may be too inefficient and may require user-side clipping to only process visible items.
                // You can seek and display only the lines that are visible using the ImGuiListClipper helper, if your elements are evenly spaced and you have cheap random access to the elements.
                // To use the clipper we could replace the 'for (int i = 0; i < Items.Size; i++)' loop with:
                //     ImGuiListClipper clipper(Items.Size);
                //     while (clipper.Step())
                //         for (int i = clipper.DisplayStart; i < clipper.DisplayEnd; i++)
                // However, note that you can not use this code as is if a filter is active because it breaks the 'cheap random-access' property. We would need random-access on the post-filtered list.
                // A typical application wanting coarse clipping and filtering may want to pre-compute an array of indices that passed the filtering test, recomputing this array when user changes the filter,
                // and appending newly elements as they are inserted. This is left as a task to the user until we can manage to improve this example code!
                // If your items are of variable size you may want to implement code similar to what ImGuiListClipper does. Or split your data into fixed height items to allow random-seeking into your list.
                ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4,1)); // Tighten spacing
                if (copy_to_clipboard)
                    ImGui::LogToClipboard();
                for (int i = 0; i < Items.Size; i++)
                {
                    const char* item = Items[i];
                    if (!Filter.PassFilter(item))
                        continue;

                    // Normally you would store more information in your item (e.g. make Items[] an array of structure, store color/type etc.)
                    bool pop_color = false;
                    if (strstr(item, "[error]"))            { ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0f, 0.4f, 0.4f, 1.0f)); pop_color = true; }
                    else if (strstr(item, "Traceback"))            { ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0f, 0.4f, 0.4f, 1.0f)); pop_color = true; }
                    else if (strncmp(item, ">>> ", 2) == 0)   { ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0f, 0.8f, 0.6f, 1.0f)); pop_color = true; }
                    else if (strncmp(item, "... ", 2) == 0)   { ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0f, 0.8f, 0.6f, 1.0f)); pop_color = true; }
                    ImGui::TextUnformatted(item);
                    if (pop_color)
                        ImGui::PopStyleColor();
                }
                if (copy_to_clipboard)
                    ImGui::LogFinish();

                if (ScrollToBottom || (AutoScroll && ImGui::GetScrollY() >= ImGui::GetScrollMaxY()))
                    ImGui::SetScrollHereY(1.0f);
                ScrollToBottom = false;

                ImGui::PopStyleVar();
                ImGui::EndChild();
                ImGui::Separator();

                bool reclaim_focus = false;
                if (ImGui::InputText("Input", InputBuf, IM_ARRAYSIZE(InputBuf), 
                    // ImGuiInputTextFlags_EnterReturnsTrue|ImGuiInputTextFlags_CallbackCompletion|ImGuiInputTextFlags_CallbackHistory, 
                    ImGuiInputTextFlags_AllowTabInput
                    |ImGuiInputTextFlags_EnterReturnsTrue
                    |ImGuiInputTextFlags_CallbackHistory
                    | ((CommandResults.size() != 0) ? ImGuiInputTextFlags_ReadOnly : 0), // disable if commands still being processed
                    &TextEditCallbackStub, (void*)this)
                )
                {
                    char* s = InputBuf;
                    // Strtrim(s);
                    // if (s[0])
                    ExecCommand(s);
                    strcpy(s, "");
                    reclaim_focus = true;
                }


                // Auto-focus on window apparition
                ImGui::SetItemDefaultFocus();
                if (reclaim_focus)
                    ImGui::SetKeyboardFocusHere(-1); // Auto focus previous widget
            }
            // Process the queue of asynchronous python commands, logging results as they finish execution
            ProcessQueue();
            // ImGui::End();
        }

        void    ExecCommand(const char* command_line)
        {
            // Insert into history. First find match and delete it so it can be pushed to the back. This isn't trying to be smart or optimal.
            // ignore empty commands
            if (command_line[0]) {    
                HistoryPos = -1;
                for (int i = History.Size-1; i >= 0; i--)
                    if (Stricmp(History[i], command_line) == 0)
                    {
                        free(History[i]);
                        History.erase(History.begin() + i);
                        break;
                    }
                History.push_back(Strdup(command_line));
            }

            // Process command
            auto ps = PythonSystem::Get();
            std::string output = "";
            std::string error = "";
            PythonResult result;
            CommandResults.push_back(result);
            ps->push(command_line, CommandResults.back());
        }

        void ProcessQueue()
        {
            if (
                (CommandResults.size() > 0) && 
                (CommandResults.front().future.wait_for(std::chrono::seconds(0)) == std::future_status::ready)
            ) {
                PythonResult result = CommandResults.front();
                CommandResults.pop_front();

                if (result.complete) {
                    AddLog(">>> %s\n", result.command.c_str());
                } else {
                    AddLog("... %s\n", result.command.c_str());
                }

                if (result.output.length() > 0) {
                    AddLog(result.output.c_str());
                }
                else if (result.error.length() > 0) {
                    AddLog(result.error.c_str());
                }

                // On commad input, we scroll to bottom even if AutoScroll==false
                ScrollToBottom = true;
            }

            

            

            // if (Stricmp(command_line, "CLEAR") == 0)
            // {
            //     ClearLog();
            // }
            // else if (Stricmp(command_line, "HELP") == 0)
            // {
            //     AddLog("Commands:");
            //     for (int i = 0; i < Commands.Size; i++)
            //         AddLog("- %s", Commands[i]);
            // }
            // else if (Stricmp(command_line, "HISTORY") == 0)
            // {
            //     int first = History.Size - 10;
            //     for (int i = first > 0 ? first : 0; i < History.Size; i++)
            //         AddLog("%3d: %s\n", i, History[i]);
            // }
            // else
            // {
            //     AddLog("Unknown command: '%s'\n", command_line);
            // }

            
            
        }

        static int TextEditCallbackStub(ImGuiInputTextCallbackData* data) // In C++11 you are better off using lambdas for this sort of forwarding callbacks
        {
            ExampleAppConsole* console = (ExampleAppConsole*)data->UserData;
            return console->TextEditCallback(data);
        }

        int     TextEditCallback(ImGuiInputTextCallbackData* data)
        {
            //AddLog("cursor: %d, selection: %d-%d", data->CursorPos, data->SelectionStart, data->SelectionEnd);
            switch (data->EventFlag)
            {
            case ImGuiInputTextFlags_CallbackCompletion:
                {
                    // Example of TEXT COMPLETION

                    // Locate beginning of current word
                    const char* word_end = data->Buf + data->CursorPos;
                    const char* word_start = word_end;
                    while (word_start > data->Buf)
                    {
                        const char c = word_start[-1];
                        if (c == ' ' || c == '\t' || c == ',' || c == ';')
                            break;
                        word_start--;
                    }

                    // Build a list of candidates
                    ImVector<const char*> candidates;
                    for (int i = 0; i < Commands.Size; i++)
                        if (Strnicmp(Commands[i], word_start, (int)(word_end-word_start)) == 0)
                            candidates.push_back(Commands[i]);

                    if (candidates.Size == 0)
                    {
                        // No match
                        AddLog("No match for \"%.*s\"!\n", (int)(word_end-word_start), word_start);
                    }
                    else if (candidates.Size == 1)
                    {
                        // Single match. Delete the beginning of the word and replace it entirely so we've got nice casing
                        data->DeleteChars((int)(word_start-data->Buf), (int)(word_end-word_start));
                        data->InsertChars(data->CursorPos, candidates[0]);
                        data->InsertChars(data->CursorPos, " ");
                    }
                    else
                    {
                        // Multiple matches. Complete as much as we can, so inputing "C" will complete to "CL" and display "CLEAR" and "CLASSIFY"
                        int match_len = (int)(word_end - word_start);
                        for (;;)
                        {
                            int c = 0;
                            bool all_candidates_matches = true;
                            for (int i = 0; i < candidates.Size && all_candidates_matches; i++)
                                if (i == 0)
                                    c = toupper(candidates[i][match_len]);
                                else if (c == 0 || c != toupper(candidates[i][match_len]))
                                    all_candidates_matches = false;
                            if (!all_candidates_matches)
                                break;
                            match_len++;
                        }

                        if (match_len > 0)
                        {
                            data->DeleteChars((int)(word_start - data->Buf), (int)(word_end-word_start));
                            data->InsertChars(data->CursorPos, candidates[0], candidates[0] + match_len);
                        }

                        // List matches
                        AddLog("Possible matches:\n");
                        for (int i = 0; i < candidates.Size; i++)
                            AddLog("- %s\n", candidates[i]);
                    }

                    break;
                }
            case ImGuiInputTextFlags_CallbackHistory:
                {
                    // Example of HISTORY
                    const int prev_history_pos = HistoryPos;
                    if (data->EventKey == ImGuiKey_UpArrow)
                    {
                        if (HistoryPos == -1)
                            HistoryPos = History.Size - 1;
                        else if (HistoryPos > 0)
                            HistoryPos--;
                    }
                    else if (data->EventKey == ImGuiKey_DownArrow)
                    {
                        if (HistoryPos != -1)
                            if (++HistoryPos >= History.Size)
                                HistoryPos = -1;
                    }

                    // A better implementation would preserve the data on the current input line along with cursor position.
                    if (prev_history_pos != HistoryPos)
                    {
                        const char* history_str = (HistoryPos >= 0) ? History[HistoryPos] : "";
                        data->DeleteChars(0, data->BufTextLen);
                        data->InsertChars(0, history_str);
                    }
                }
            }
            return 0;
        }
    };


    void EditTransform(glm::mat4 cameraView, glm::mat4 cameraProjection, glm::mat4 &matrix)
    {
        /* Improves numerical stability */
        glm::mat4 viewInv = glm::inverse(cameraView);
        // glm::vec3 scale, translation, skew; glm::vec4 perspective; glm::quat orientation;
        // glm::decompose(cameraView, scale, orientation, translation, skew, perspective);
        
        glm::vec3 camera_pos = glm::vec3(glm::column(viewInv, 3));
        viewInv[3] = glm::vec4(0.f, 0.f, 0.f, 1.f);
        cameraView = glm::inverse(viewInv);
        // cameraView = glm::translate(cameraView, -camera_pos);
        matrix = glm::translate(glm::mat4(1.f), -camera_pos) * matrix;
        
        static ImGuizmo::OPERATION mCurrentGizmoOperation(ImGuizmo::TRANSLATE);
        static ImGuizmo::MODE mCurrentGizmoMode(ImGuizmo::LOCAL);
        static bool useSnap = false;
        static float snap[3] = { 1.f, 1.f, 1.f };
        static float bounds[] = { -0.5f, -0.5f, -0.5f, 0.5f, 0.5f, 0.5f };
        static float boundsSnap[] = { 0.1f, 0.1f, 0.1f };
        static bool boundSizing = false;
        static bool boundSizingSnap = false;

        if (ImGui::IsKeyPressed(90))
            mCurrentGizmoOperation = ImGuizmo::TRANSLATE;
        if (ImGui::IsKeyPressed(69))
            mCurrentGizmoOperation = ImGuizmo::ROTATE;
        if (ImGui::IsKeyPressed(82)) // r Key
            mCurrentGizmoOperation = ImGuizmo::SCALE;

        if (ImGui::RadioButton("Translate", mCurrentGizmoOperation == ImGuizmo::TRANSLATE))
            mCurrentGizmoOperation = ImGuizmo::TRANSLATE;
        ImGui::SameLine();
        if (ImGui::RadioButton("Rotate", mCurrentGizmoOperation == ImGuizmo::ROTATE))
            mCurrentGizmoOperation = ImGuizmo::ROTATE;
        ImGui::SameLine();
        if (ImGui::RadioButton("Scale", mCurrentGizmoOperation == ImGuizmo::SCALE))
            mCurrentGizmoOperation = ImGuizmo::SCALE;

        float matrixTranslation[3], matrixRotation[3], matrixScale[3];
        ImGuizmo::DecomposeMatrixToComponents(glm::value_ptr(matrix), matrixTranslation, matrixRotation, matrixScale);
        ImGui::InputFloat3("Tr", matrixTranslation, 32);
        ImGui::InputFloat3("Rt", matrixRotation, 32);
        ImGui::InputFloat3("Sc", matrixScale, 32);
        matrixScale[0] = std::max(matrixScale[0], 1e-8f);
        matrixScale[1] = std::max(matrixScale[1], 1e-8f);
        matrixScale[2] = std::max(matrixScale[2], 1e-8f);
        ImGuizmo::RecomposeMatrixFromComponents(matrixTranslation, matrixRotation, matrixScale, glm::value_ptr(matrix));

        if (mCurrentGizmoOperation != ImGuizmo::SCALE)
        {
            if (ImGui::RadioButton("Local", mCurrentGizmoMode == ImGuizmo::LOCAL))
                mCurrentGizmoMode = ImGuizmo::LOCAL;
            ImGui::SameLine();
            if (ImGui::RadioButton("World", mCurrentGizmoMode == ImGuizmo::WORLD))
                mCurrentGizmoMode = ImGuizmo::WORLD;
        }
        if (ImGui::IsKeyPressed(83))
            useSnap = !useSnap;
        ImGui::Checkbox("", &useSnap);
        ImGui::SameLine();

        switch (mCurrentGizmoOperation)
        {
        case ImGuizmo::TRANSLATE:
            ImGui::InputFloat3("Snap", &snap[0], "%.32f");
            break;
        case ImGuizmo::ROTATE:
            ImGui::InputFloat("Angle Snap", &snap[0], 0.f, 0.f, "%.32f");
            break;
        case ImGuizmo::SCALE:
            ImGui::InputFloat("Scale Snap", &snap[0], 0.f, 0.f, "%.32f");
            break;
        }
        ImGui::Checkbox("Bound Sizing", &boundSizing);
        if (boundSizing)
        {
            ImGui::PushID(3);
            ImGui::Checkbox("", &boundSizingSnap);
            ImGui::SameLine();
            ImGui::InputFloat3("Snap", boundsSnap);
            ImGui::PopID();
        }

        ImGuiIO& io = ImGui::GetIO();
        ImGuizmo::SetRect(0, 0, io.DisplaySize.x, io.DisplaySize.y);
        ImGuizmo::Manipulate(glm::value_ptr(cameraView), glm::value_ptr(cameraProjection), mCurrentGizmoOperation, mCurrentGizmoMode, 
            glm::value_ptr(matrix), NULL, useSnap ? &snap[0] : NULL, boundSizing?bounds:NULL, boundSizingSnap?boundsSnap:NULL);

        matrix = glm::translate(glm::mat4(1.f), camera_pos) * matrix;
    }

    void RenderSystem::reset_camera(float zoom) {
        glm::vec3 center = glm::mix(glm::vec3(LP.bumesh_bbmin), glm::vec3(LP.bumesh_bbmax), .5);
        glm::vec3 eye = center + glm::normalize(glm::vec3(LP.bumesh_bbmax) - glm::vec3(LP.bumesh_bbmin)) * zoom;
        glm::vec3 up = glm::vec3(0.f, 1.f, 0.f);
        camera_controls.initialize(eye, center, up);
    }

    void RenderSystem::render_imgui() {
        // // Load any extra colormaps the user wants to see in the demo
        // for (int i = 1; i < argc; ++i) {
        //     int w, h, n;
        //     uint8_t *img_data = stbi_load(argv[i], &w, &h, &n, 4);
        //     auto img = std::vector<uint8_t>(img_data, img_data + w * 1 * 4);
        //     stbi_image_free(img_data);
        //     // Input images are assumed to be sRGB color-space
        //     tfn_widget.add_colormap(Colormap(argv[i], img, SRGB));
        // }

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        ImGui::Text(("Bounds min: " + glm::to_string(LP.bumesh_bbmin)).c_str());
        ImGui::Text(("Bounds max: " + glm::to_string(LP.bumesh_bbmax)).c_str());

        static float distance = 100.f;
        ImGui::InputFloat("Distance: ", &distance, .5f, .1f);

        static float zoom = 1.f;
        if (ImGui::SliderFloat("Zoom: ", &zoom, 0.0f, 100.0f, "%.4f", 10.f))
        {
            LP.zoom = zoom;
            reset_accumulation();
        }

        // static bool options_open = true;
        // if (ImGui::CollapsingHeader("Options", &options_open))
        {
            if (ImGui::Button("Reset Camera", ImVec2(ImGui::GetWindowSize().x, 0.0f))) {
                reset_camera(distance);
                reset_accumulation();
            }

            if (ImGui::Button("Save Camera", ImVec2(ImGui::GetWindowSize().x, 0.0f)))
                ImGui::OpenPopup("Save Camera");
            if (ImGui::BeginPopupModal("Save Camera", NULL, ImGuiWindowFlags_AlwaysAutoResize))
            {
                static char path[1024] = "./camera.bin";
                ImGui::InputText("Output path", path, IM_ARRAYSIZE(path));
                if (ImGui::Button("Save", ImVec2(120, 0))) { 
                    save_camera(std::string(path));
                    ImGui::CloseCurrentPopup(); 
                }
                ImGui::SetItemDefaultFocus();
                ImGui::SameLine();
                if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
                ImGui::EndPopup();
            }

            if (ImGui::Button("Load Camera", ImVec2(ImGui::GetWindowSize().x, 0.0f)))
                ImGui::OpenPopup("Load Camera");
            if (ImGui::BeginPopupModal("Load Camera", NULL, ImGuiWindowFlags_AlwaysAutoResize))
            {
                static char path[1024] = "./camera.bin";
                ImGui::InputText("Input path", path, IM_ARRAYSIZE(path));
                if (ImGui::Button("Load", ImVec2(120, 0))) { 
                    load_camera(std::string(path));
                    ImGui::CloseCurrentPopup(); 
                }
                ImGui::SetItemDefaultFocus();
                ImGui::SameLine();
                if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
                ImGui::EndPopup();
            }

            ImGuizmo::BeginFrame();
            ImGuizmo::SetOrthographic(false);

            bool show_console = true;
            static ExampleAppConsole console;
            if (show_console)
                console.Draw("Example: Console", &show_console);
            
            // ImGuizmo::ViewManipulate(glm::value_ptr(LP.view), /*camDistance*/10.f, 
            //     ImVec2(LP.frame_size.x - 128, 0), ImVec2(128, 128), 0x10101010);

            // Doesn't seem to work for me...
            // ImGuizmo::DrawGrid(glm::value_ptr(LP.view), glm::value_ptr(LP.proj), glm::value_ptr(glm::mat4(1.f)), 10.f);
            if (ImGui::CollapsingHeader("Gizmo Properties")) {
                static int e = 0;
                if (ImGui::RadioButton("Transform Volume", &e, 0)) { reset_accumulation(); } ImGui::SameLine();
                if (ImGui::RadioButton("Transform Mesh", &e, 1)) { reset_accumulation(); }

                glm::vec3 midpoint = (e == 0) ? 
                    (glm::vec3(LP.bumesh_bbmax - LP.bumesh_bbmin) * .5f) + glm::vec3(LP.bumesh_bbmin)
                    : (glm::vec3(LP.tri_mesh_bbmax - LP.tri_mesh_bbmin) * .5f) + glm::vec3(LP.tri_mesh_bbmin);

                auto &tfm = (e == 0) ? bumesh_transform : tri_mesh_transform;
                glm::mat4 transform = tfm->get_local_to_world_matrix();
                transform = glm::translate(transform, midpoint);
                EditTransform(LP.view, LP.proj, transform);
                transform = glm::translate(transform, -midpoint);
                tfm->set_transform(transform);
                // if (e != 0) tfm->set_scale(2,1,1); // TEMPORARY

                auto glfw = Libraries::GLFW::Get();
                int left = glfw->get_button_action("DefaultWindow", 0);
                if (left) 
                    reset_accumulation();
            }

            if (ImGui::CollapsingHeader("Render Settings")) {
                static int e = 0;
                if (ImGui::RadioButton("Show Volume", &e, 0)) { reset_accumulation(); } ImGui::SameLine();
                if (ImGui::RadioButton("Show Time", &e, 1)) { reset_accumulation(); } ImGui::SameLine();
                if (ImGui::RadioButton("Show Samples", &e, 2)) reset_accumulation();
                
                static int m = 1;
                if (ImGui::RadioButton("Enable ray marching", &m, 0)) reset_accumulation(); ImGui::SameLine();
                if (ImGui::RadioButton("Enable path tracing", &m, 1)) reset_accumulation(); 
                if (m == 0) LP.enable_pathtracer = false;
                else LP.enable_pathtracer = true;

                if (ImGui::SliderFloat("Time Min", (float*)&LP.time_min, 0.000f, 100000000.f, "%.4f", 8.)) reset_accumulation();
                if (ImGui::SliderFloat("Time Max", (float*)&LP.time_max, 0.000f, 100000000.f, "%.4f", 8.)) reset_accumulation();
                if (e == 0) {
                    LP.show_samples_heatmap = LP.show_time_heatmap = false;
                } else if (e == 1) {
                    LP.show_samples_heatmap = false; LP.show_time_heatmap = true;
                } else {
                    LP.show_samples_heatmap = true; LP.show_time_heatmap = false;
                }

                ImGui::ColorPicker4("Background color", &LP.background_color.x); //ImGui::SameLine();
                // ImGui::ColorPicker4("Trimesh color", &LP.tri_mesh_color.x);

                if (ImGui::Button("Render High Quality Image", ImVec2(ImGui::GetWindowSize().x, 0.0f)))
                    ImGui::OpenPopup("Render High Quality Image");
                if (ImGui::BeginPopupModal("Render High Quality Image", NULL, ImGuiWindowFlags_AlwaysAutoResize))
                {
                    static char path[1024] = "./image.png";
                    ImGui::InputText("Output path", path, IM_ARRAYSIZE(path));
                    static int samples = 8;
                    ImGui::InputInt("Samples per pixel", &samples);
                    if (ImGui::Button("Render", ImVec2(120, 0))) { 
                        ImGui::CloseCurrentPopup(); 
                        render_image(samples, std::string(path));
                    }
                    ImGui::SetItemDefaultFocus();
                    ImGui::SameLine();
                    if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
                    ImGui::EndPopup();
                }
            }

            if (tfn_widget.changed() || (LP.startFrame == 0)) 
            {
                auto colormap = tfn_widget.get_colormapf();
                owlBufferResize(transferFunctionBuffer, colormap.size() / 4);
                glm::vec4 *ptr = (glm::vec4*)owlBufferGetPointer(transferFunctionBuffer, 0);
                for (int i = 0; i < colormap.size() / 4; ++i) {
                    glm::vec4 c = glm::vec4(
                        colormap[i * 4 + 0],
                        colormap[i * 4 + 1],
                        colormap[i * 4 + 2],
                        colormap[i * 4 + 3]
                    );
                    if (c.a < .005) c.a = 0.f;
                    ptr[i] = c;
                }
            
                reset_accumulation();
                LP.transferFunction = ptr;
                LP.transferFunctionWidth = colormap.size() / 4;
                
                // Update space skipping / adaptive sampling data
                update_adaptive_sampling_metadata();
            }

            if (ImGui::CollapsingHeader("Raycasting Properties")) {
                int max_lod = 0;
                if (bumesh && bumesh->multileaves_loaded()) max_lod = 1;
                if (bumesh && bumesh->leaves_loaded()) max_lod = 2;
                if (bumesh && bumesh->elements_loaded()) max_lod = 3;
                if (ImGui::SliderInt("Volume LOD", (int*)&LP.volume_type, 0, max_lod)) reset_accumulation();
                if (ImGui::SliderFloat("Attenuation", (float*)&LP.attenuation, 0.001f, 100.f, "%.4f", 8.)) {
                    update_adaptive_sampling_metadata();
                    reset_accumulation();
                }
                if (ImGui::SliderFloat("Opacity", (float*)&LP.opacity, 0.0, 20.f, "%.4f", 1.)) {
                    update_adaptive_sampling_metadata();
                    reset_accumulation();
                }

                // ImGui::SliderFloat("Adaptive Power", (float*)&LP.adaptive_power, 1.f, 20.f, "%.4f", 8.);
                // ImGui::SliderFloat("Empty threshold", (float*)&LP.empty_threshold, 0.000f, 1.f, "%.4f", 8.);
                
                // ImGui::SliderFloat("Max Step Size", (float*)&LP.max_step_size, 0.001f, 100.f, "%.4f", 8.);
                // float step_size[2] = {LP.min_step_size, LP.max_step_size};
                
                // static float vec4f[4] = { 0.10f, 0.20f, 0.30f, 0.44f };
                // ImGui::DragFloat2("drag float2", vec4f, 0.01f, 0.0f, 1.0f);
                
                // ImGui::SliderFloat2("Step Size", step_size, 0.001f, 100.f, "%.4f", 8.);
                // LP.min_step_size = step_size[0]; LP.max_step_size = step_size[1];
                // static float v1 = 0.f;
                // static float v2 = 1.f;
                // ImGui::RangeSliderFloat("TEST", &v1, &v2, 0.f, 1.f);

                float window_width = ImGui::GetWindowWidth();
                float initOffset = window_width * .05f;
                float doffset = ((window_width-initOffset) / 4.f);
                ImGui::NewLine();
                int offset = initOffset;

                ImGui::SameLine(offset); if (ImGui::KnobFloat(" Adapt. Pow ", doffset * .4, .002f, &LP.adaptive_power, 1.f, 20.f)) reset_accumulation();
                offset += doffset;

                ImGui::SameLine(offset); if (ImGui::KnobFloat("Empty-Thrsh ", doffset * .4, .002f, &LP.empty_threshold, .0f, 1.f)) reset_accumulation();
                offset += doffset;

                ImGui::SameLine(offset); if (ImGui::KnobFloat("  Min Step  ", doffset * .4, .002f, &LP.min_step_size, 0.001f, 1.f)) reset_accumulation();
                if (LP.min_step_size > LP.max_step_size) LP.max_step_size = LP.min_step_size;
                offset += doffset;
                
                ImGui::SameLine(offset); if (ImGui::KnobFloat("  Max Step  ", doffset * .4, .002f, &LP.max_step_size, 0.001f, 1.f)) reset_accumulation();
                if (LP.max_step_size < LP.min_step_size) LP.min_step_size = LP.max_step_size;
            }
        }
        if (ImGui::CollapsingHeader("Transfer Function")) {
            if (ImGui::Button("Save", ImVec2(ImGui::GetWindowSize().x, 0.0f)))
                ImGui::OpenPopup("Save");
            if (ImGui::BeginPopupModal("Save", NULL, ImGuiWindowFlags_AlwaysAutoResize))
            {
                static char path[1024] = "./colormap.png";
                ImGui::InputText("Output path", path, IM_ARRAYSIZE(path));
                if (ImGui::Button("Save", ImVec2(120, 0))) { 
                    save_color_map(tfn_widget.get_colormap() , std::string(path));
                    ImGui::CloseCurrentPopup(); 
                }
                ImGui::SetItemDefaultFocus();
                ImGui::SameLine();
                if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
                ImGui::EndPopup();
            }

            if (ImGui::Button("Load", ImVec2(ImGui::GetWindowSize().x, 0.0f)))
                ImGui::OpenPopup("Load");
            if (ImGui::BeginPopupModal("Load", NULL, ImGuiWindowFlags_AlwaysAutoResize))
            {
                static char path[1024] = "./colormap.png";
                ImGui::InputText("Input path", path, IM_ARRAYSIZE(path));
                if (ImGui::Button("Load", ImVec2(120, 0))) { 
                    load_color_map(std::string(path));
                    ImGui::CloseCurrentPopup(); 
                }
                ImGui::SetItemDefaultFocus();
                ImGui::SameLine();
                if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
                ImGui::EndPopup();
            }

            float range[2] = {LP.transferFunctionMin, LP.transferFunctionMax};
            if (ImGui::SliderFloat("Tfn Min", &LP.transferFunctionMin, -1.f, 1.f)) {
                update_adaptive_sampling_metadata();
                reset_accumulation();
            }

            if (ImGui::SliderFloat("Tfn Max", &LP.transferFunctionMax, 0.f, 2.f)) {
                update_adaptive_sampling_metadata();
                reset_accumulation();
            }

            // if (ImGui::SliderFloat2("Tfn Range", range, -1.f, 1.f, "%.4f")) {
            //     update_adaptive_sampling_metadata();
            //     reset_accumulation();
            // }
            // LP.transferFunctionMin = range[0]; LP.transferFunctionMax = range[1];
            tfn_widget.draw_ui();
        }

        ImGui::Render();        
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    }

    void RenderSystem::reset_accumulation() {
        LP.reset = true;
        LP.startFrame = 0;
    }

    bool RenderSystem::start() {
        if (!initialized) return false;
        if (running) return false;

        running = true;
        close = false;

        auto lastTime = glfwGetTime();

        auto glfw = Libraries::GLFW::Get();
        // window = glfw->create_window("DefaultWindow", 854, 480, false, true, true);
        window = glfw->create_window("DefaultWindow", 1024, 1024, false, true, true);
        glfw->make_context_current("DefaultWindow");
        
        glViewport(0, 0, 854*2, 480*2);
        glClearColor(1, 1, 1, 1);
        
        ImGui::CreateContext();
        auto &io  = ImGui::GetIO();

        ImGui::StyleColorsDark();
        ImGui_ImplGlfw_InitForOpenGL(window, true);
        const char* glsl_version = "#version 130";
        ImGui_ImplOpenGL3_Init(glsl_version);

        /* Setup Optix Launch Params */
        OWLVarDecl launchParamVars[] = {
            { "frame",             OWL_USER_TYPE(uint64_t),           OWL_OFFSETOF(LaunchParams, frame)},
            { "startFrame",        OWL_USER_TYPE(uint64_t),           OWL_OFFSETOF(LaunchParams, startFrame)},
            { "reset",             OWL_USER_TYPE(bool),               OWL_OFFSETOF(LaunchParams, reset)},
            { "enable_pathtracer", OWL_USER_TYPE(bool),               OWL_OFFSETOF(LaunchParams, enable_pathtracer)},
            { "enable_adaptive_sampling", OWL_USER_TYPE(bool),               OWL_OFFSETOF(LaunchParams, enable_adaptive_sampling)},
            { "enable_space_skipping", OWL_USER_TYPE(bool),               OWL_OFFSETOF(LaunchParams, enable_space_skipping)},
            { "enable_id_colors", OWL_USER_TYPE(bool),               OWL_OFFSETOF(LaunchParams, enable_id_colors)},
            { "mirror", OWL_USER_TYPE(bool),               OWL_OFFSETOF(LaunchParams, mirror)},
            { "zoom", OWL_USER_TYPE(float),               OWL_OFFSETOF(LaunchParams, zoom)},
            { "min_step_size",     OWL_USER_TYPE(float),              OWL_OFFSETOF(LaunchParams, min_step_size)},
            { "max_step_size",     OWL_USER_TYPE(float),              OWL_OFFSETOF(LaunchParams, max_step_size)},
            { "adaptive_power",    OWL_USER_TYPE(float),              OWL_OFFSETOF(LaunchParams, adaptive_power)},
            { "attenuation",       OWL_USER_TYPE(float),              OWL_OFFSETOF(LaunchParams, attenuation)},
            { "opacity",           OWL_USER_TYPE(float),              OWL_OFFSETOF(LaunchParams, opacity)},
            { "volume_type",       OWL_USER_TYPE(uint32_t),           OWL_OFFSETOF(LaunchParams, volume_type)},
            { "camera_entity",     OWL_USER_TYPE(EntityStruct),       OWL_OFFSETOF(LaunchParams, camera_entity)},
            { "camera_transform",  OWL_USER_TYPE(TransformStruct),    OWL_OFFSETOF(LaunchParams, camera_transform)},
            // { "entities",          OWL_BUFPTR,                        OWL_OFFSETOF(LaunchParams, entities)},
            // { "transforms",        OWL_BUFPTR,                        OWL_OFFSETOF(LaunchParams, transforms)},
            { "view",              OWL_USER_TYPE(glm::mat4),          OWL_OFFSETOF(LaunchParams, view)},
            { "viewinv",           OWL_USER_TYPE(glm::mat4),          OWL_OFFSETOF(LaunchParams, viewinv)},
            { "proj",              OWL_USER_TYPE(glm::mat4),          OWL_OFFSETOF(LaunchParams, proj)},
            { "projinv",           OWL_USER_TYPE(glm::mat4),          OWL_OFFSETOF(LaunchParams, projinv)},
            { "frame_size",        OWL_USER_TYPE(glm::ivec2),         OWL_OFFSETOF(LaunchParams, frame_size)},
            { "fbPtr",             OWL_BUFPTR,                        OWL_OFFSETOF(LaunchParams, fbPtr)},
            { "accumPtr",          OWL_BUFPTR,                        OWL_OFFSETOF(LaunchParams, accumPtr)},
            { "world",             OWL_GROUP,                         OWL_OFFSETOF(LaunchParams, world)},
            { "transferFunction",  OWL_BUFPTR,                        OWL_OFFSETOF(LaunchParams, transferFunction)},
            { "transferFunctionWidth",  OWL_USER_TYPE(glm::ivec2),    OWL_OFFSETOF(LaunchParams, transferFunctionWidth)},            
            { "transferFunctionMin",  OWL_USER_TYPE(float),           OWL_OFFSETOF(LaunchParams, transferFunctionMin)},            
            { "transferFunctionMax",  OWL_USER_TYPE(float),           OWL_OFFSETOF(LaunchParams, transferFunctionMax)}, 
            { "part_max_color",    OWL_BUFPTR,                        OWL_OFFSETOF(LaunchParams, part_max_color)}, 
            { "part_min_color",    OWL_BUFPTR,                        OWL_OFFSETOF(LaunchParams, part_min_color)}, 
            { "part_var_color",    OWL_BUFPTR,                        OWL_OFFSETOF(LaunchParams, part_var_color)},             
            { "max_color",         OWL_USER_TYPE(glm::vec4),          OWL_OFFSETOF(LaunchParams, max_color)},             
            { "multiLeavesAccel",  OWL_GROUP,                         OWL_OFFSETOF(LaunchParams, multiLeavesAccel)},
            { "partsAccel",        OWL_GROUP,                         OWL_OFFSETOF(LaunchParams, partsAccel)},
            { "bumesh_bbmin",      OWL_USER_TYPE(glm::vec4),          OWL_OFFSETOF(LaunchParams, bumesh_bbmin)},
            { "bumesh_bbmax",      OWL_USER_TYPE(glm::vec4),          OWL_OFFSETOF(LaunchParams, bumesh_bbmax)},            
            { "bumesh_transform",  OWL_USER_TYPE(TransformStruct),    OWL_OFFSETOF(LaunchParams, bumesh_transform)},            
            { "show_time_heatmap",  OWL_USER_TYPE(bool),    OWL_OFFSETOF(LaunchParams, show_time_heatmap)},            
            { "show_samples_heatmap",  OWL_USER_TYPE(bool),    OWL_OFFSETOF(LaunchParams, show_samples_heatmap)},            
            { "empty_threshold",  OWL_USER_TYPE(float),    OWL_OFFSETOF(LaunchParams, empty_threshold)},            
            { "time_min",  OWL_USER_TYPE(float),    OWL_OFFSETOF(LaunchParams, time_min)},            
            { "time_max",  OWL_USER_TYPE(float),    OWL_OFFSETOF(LaunchParams, time_max)},
            { "tri_mesh_color",  OWL_USER_TYPE(glm::vec4),    OWL_OFFSETOF(LaunchParams, tri_mesh_color)},
            { "background_color",  OWL_USER_TYPE(glm::vec4),    OWL_OFFSETOF(LaunchParams, background_color)},
            { "tri_mesh_transform",  OWL_USER_TYPE(TransformStruct),    OWL_OFFSETOF(LaunchParams, tri_mesh_transform)},
            { "tri_mesh_bbmin",      OWL_USER_TYPE(glm::vec4),          OWL_OFFSETOF(LaunchParams, tri_mesh_bbmin)},
            { "tri_mesh_bbmax",      OWL_USER_TYPE(glm::vec4),          OWL_OFFSETOF(LaunchParams, tri_mesh_bbmax)},            
            { /* sentinel to mark end of list */ }
        };
        
        launchParams = owlLaunchParamsCreate(context, sizeof(LaunchParams), 
                                            launchParamVars, -1);
        


        /* Temporary test code */
        const int NUM_VERTICES = 8;
        vec3f vertices[NUM_VERTICES] =
        {
            { -1.f,-1.f,-.1f },
            { +1.f,-1.f,-.1f },
            { -1.f,+1.f,-.1f },
            { +1.f,+1.f,-.1f },
            { -1.f,-1.f,+.1f },
            { +1.f,-1.f,+.1f },
            { -1.f,+1.f,+.1f },
            { +1.f,+1.f,+.1f }
        };

        const int NUM_INDICES = 12;
        vec3i indices[NUM_INDICES] =
        {
            { 0,1,3 }, { 2,3,0 },
            { 5,7,6 }, { 5,6,4 },
            { 0,4,5 }, { 0,5,1 },
            { 2,3,7 }, { 2,7,6 },
            { 1,5,7 }, { 1,7,3 },
            { 4,0,2 }, { 4,2,6 }
        };

        // const vec2i fbSize(800,600);
        // const vec3f lookFrom(-4.f,-3.f,-2.f);
        // const vec3f lookAt(0.f,0.f,0.f);
        // const vec3f lookUp(0.f,1.f,0.f);
        // const float cosFovy = 0.66f;


        initialize_framebuffer(854*2, 480*2);
        frameBuffer = owlManagedMemoryBufferCreate(context,OWL_USER_TYPE(glm::vec4),854*2*480*2, nullptr);
        accumBuffer = owlDeviceBufferCreate(context,OWL_INT,854*2*480*2, nullptr);
        owlLaunchParamsSetBuffer(launchParams, "fbPtr", frameBuffer);
        owlLaunchParamsSetBuffer(launchParams, "accumPtr", accumBuffer);

        // transfer function
        uint16_t width = 12;
        transferFunctionBuffer = owlManagedMemoryBufferCreate(context,OWL_USER_TYPE(glm::vec4),width, nullptr);
        owlLaunchParamsSetBuffer(launchParams, "transferFunction", transferFunctionBuffer);
        owlLaunchParamsSetRaw(launchParams, "transferFunctionWidth", &width);
        glm::vec4 *tf = (glm::vec4*)owlBufferGetPointer(transferFunctionBuffer,0);
        
        tf[0]  = glm::vec4(59.f/255.f,76.f/255.f,192.f/255.f, 0.083333333);
        tf[1]  = glm::vec4(83.f/255.f,111.f/255.f,220.f/255.f, 0.166666667);
        tf[2]  = glm::vec4(111.f/255.f,145.f/255.f,242.f/255.f, 0.25);
        tf[3]  = glm::vec4(140.f/255.f,174.f/255.f,252.f/255.f, 0.333333333);
        tf[4]  = glm::vec4(169.f/255.f,197.f/255.f,252.f/255.f, 0.416666667);
        tf[5]  = glm::vec4(197.f/255.f,213.f/255.f,241.f/255.f, 0.5);
        tf[6]  = glm::vec4(221.f/255.f,221.f/255.f,221.f/255.f, 0.583333333);
        tf[7]  = glm::vec4(238.f/255.f,205.f/255.f,188.f/255.f, 0.666666667);
        tf[8]  = glm::vec4(246.f/255.f,182.f/255.f,155.f/255.f, 0.75);
        tf[9]  = glm::vec4(243.f/255.f,152.f/255.f,122.f/255.f, 0.833333333);
        tf[10] = glm::vec4(230.f/255.f,116.f/255.f,91.f/255.f, 0.916666667);
        tf[11] = glm::vec4(180.f/255.f,4.f/255.f,38.f/255.f, 1);


        // -------------------------------------------------------
        // declare geometry type
        // -------------------------------------------------------
        OWLVarDecl trianglesGeomVars[] = {
            { "index",  OWL_BUFPTR, OWL_OFFSETOF(TrianglesGeomData,index)},
            { "vertex", OWL_BUFPTR, OWL_OFFSETOF(TrianglesGeomData,vertex)},
            { "colors",  OWL_BUFPTR, OWL_OFFSETOF(TrianglesGeomData,colors)}
            // { "color",  OWL_FLOAT3, OWL_OFFSETOF(TrianglesGeomData,color)}
        };
        trianglesGeomType
            = owlGeomTypeCreate(context,
                                OWL_GEOM_TRIANGLES,
                                sizeof(TrianglesGeomData),
                                trianglesGeomVars,3);
        owlGeomTypeSetClosestHit(trianglesGeomType, /*ray type */ 0, module,"TriangleMesh");
        // owlGeomTypeSetClosestHit(trianglesGeomType, /*ray type */ 1, module,"TriangleMesh");

        // ##################################################################
        // set up all the *GEOMS* we want to run that code on
        // ##################################################################

        // ------------------------------------------------------------------
        // triangle mesh
        // ------------------------------------------------------------------
        OWLBuffer vertexBuffer = owlDeviceBufferCreate(context,OWL_FLOAT3,NUM_VERTICES,vertices);
        OWLBuffer indexBuffer = owlDeviceBufferCreate(context,OWL_INT3,NUM_INDICES,indices);
        
        OWLGeom trianglesGeom = owlGeomCreate(context,trianglesGeomType);

        owlTrianglesSetVertices(trianglesGeom,vertexBuffer,
                                NUM_VERTICES,sizeof(vec3f),0);
        owlTrianglesSetIndices(trianglesGeom,indexBuffer,
                                NUM_INDICES,sizeof(vec3i),0);

        owlGeomSetBuffer(trianglesGeom,"vertex",vertexBuffer);
        owlGeomSetBuffer(trianglesGeom,"index",indexBuffer);
        owlGeomSetBuffer(trianglesGeom,"colors",nullptr);
        // owlGeomSet3f(trianglesGeom,"color",owl3f{0,1,0});
        LP.tri_mesh_bbmin = LP.tri_mesh_bbmax = vec4(0.f);
        owlLaunchParamsSetRaw(launchParams, "tri_mesh_bbmin", &LP.tri_mesh_bbmin);
        owlLaunchParamsSetRaw(launchParams, "tri_mesh_bbmax", &LP.tri_mesh_bbmax);

        // ------------------------------------------------------------------
        // the group/accel for that mesh
        // ------------------------------------------------------------------
        OWLGroup trianglesGroup = owlTrianglesGeomGroupCreate(context,1,&trianglesGeom);
        owlGroupBuildAccel(trianglesGroup);
        OWLGroup world = owlInstanceGroupCreate(context, 1);
        owlInstanceGroupSetChild(world, 0, trianglesGroup); 
        owlGroupBuildAccel(world);
        owlLaunchParamsSetGroup(launchParams, "world", world);

        // -------------------------------------------------------
        // set up miss prog 
        // -------------------------------------------------------
        OWLVarDecl missProgVars[] = {{ /* sentinel to mark end of list */ }};
        // just reuse the same miss prog for both ray types
        missProg = owlMissProgCreate(context,module,"miss",sizeof(MissProgData),missProgVars,-1);
        // missProg = owlMissProgCreate(context,module,"miss",sizeof(MissProgData),missProgVars,-1); 
        
        // -------------------------------------------------------
        // set up ray gen program
        // -------------------------------------------------------
        OWLVarDecl rayGenVars[] = {{ /* sentinel to mark end of list */ }};
        rayGen = owlRayGenCreate(context,module,"simpleRayGen", sizeof(RayGenData), rayGenVars,-1);


        // BUMesh Geometry Types
        
        // MultiLeaves type
        OWLVarDecl multiLeavesGeomVars[] = {
                { "leaf_qbbmin",               OWL_BUFPTR,                OWL_OFFSETOF(MultileavesGeomData,leaf_qbbmin)},
                { "leaf_qbbmax",               OWL_BUFPTR,                OWL_OFFSETOF(MultileavesGeomData,leaf_qbbmax)},
                { "leaf_qsmin",                OWL_BUFPTR,                OWL_OFFSETOF(MultileavesGeomData,leaf_qsmin)},
                { "leaf_qsmax",                OWL_BUFPTR,                OWL_OFFSETOF(MultileavesGeomData,leaf_qsmax)},
                { "leaf_buffer",               OWL_BUFPTR,                OWL_OFFSETOF(MultileavesGeomData,leaf_buffer)},
                { "vertex_buffer",             OWL_BUFPTR,                OWL_OFFSETOF(MultileavesGeomData,vertex_buffer)},
                { "index_buffer",              OWL_BUFPTR,                OWL_OFFSETOF(MultileavesGeomData,index_buffer)},
                { "part_num",                  OWL_USER_TYPE(uint32_t),   OWL_OFFSETOF(MultileavesGeomData,part_num)},
                { "parent_bbmin",              OWL_USER_TYPE(glm::vec4),  OWL_OFFSETOF(MultileavesGeomData,parent_bbmin)},
                { "parent_bbmax",              OWL_USER_TYPE(glm::vec4),  OWL_OFFSETOF(MultileavesGeomData,parent_bbmax)},
                
                /*sentinal marks end of var list*/{nullptr}};

        multiLeavesGeomType = owlGeomTypeCreate(context,
                                    OWL_GEOM_USER,
                                    sizeof(MultileavesGeomData),
                                    multiLeavesGeomVars,-1);

        owlGeomTypeSetBoundsProg(multiLeavesGeomType, module, "MultiLeafBounds");
        owlGeomTypeSetIntersectProg(multiLeavesGeomType, /* ray type */ 0, module, "MultiLeafINT");
        owlGeomTypeSetClosestHit(multiLeavesGeomType,/* ray type */ 0, module,"MultiLeafCH");   
        // owlGeomTypeSetIntersectProg(multiLeavesGeomType, /* ray type */ 1, module, "MultiLeafINT");
        // owlGeomTypeSetClosestHit(multiLeavesGeomType,/* ray type */ 1, module,"MultiLeafCH");    

        // Part type
        OWLVarDecl partsGeomVars[] = {
                { "parts_buffer",      OWL_BUFPTR,  OWL_OFFSETOF(PartsGeomData,parts_buffer)},
                /*sentinal marks end of var list*/{nullptr}};

        partsGeomType = owlGeomTypeCreate(context,
                                    OWL_GEOM_USER,
                                    sizeof(PartsGeomData),
                                    partsGeomVars,-1);
        owlGeomTypeSetBoundsProg(partsGeomType, module, "PartBounds");
        owlGeomTypeSetIntersectProg(partsGeomType, /* ray type */ 0, module, "PartINT");
        owlGeomTypeSetClosestHit(partsGeomType,/* ray type */ 0, module,"PartCH");
        // owlGeomTypeSetIntersectProg(partsGeomType, /* ray type */ 1, module, "PartSSINT");
        // owlGeomTypeSetClosestHit(partsGeomType,/* ray type */ 1, module,"PartSSCH");

        // ##################################################################
        // build *SBT* required to trace the groups
        // ##################################################################
        
        owlBuildPrograms(context);
        owlBuildPipeline(context);
        owlBuildSBT(context);

        /* Done setting up OptiX */

        glfw->poll_events();

        std::array<float, 2> cur_mouse, prev_mouse;
        cur_mouse = glfw->get_cursor_pos("DefaultWindow");


        while (!close)
        {

            /* Regulate the framerate. */
            auto currentTime = glfwGetTime();
            if ((currentTime - lastTime) < .008) continue;
            lastTime = currentTime;

            /* Poll events from the window */
            glfw->poll_events();

            if (!glfw->does_window_exist("DefaultWindow")) {
                close = true;
                break;
            }

            /* Clear screen, update viewport */
            int display_w, display_h;
            glfwGetFramebufferSize(window, &display_w, &display_h);
            glViewport(0, 0, display_w, display_h);
            glClearColor(r,g,b,1);
            glClear(GL_COLOR_BUFFER_BIT);

            LP.frame_size = glm::ivec2(display_w, display_h);

            {
                GLenum err;
                while((err = glGetError()) != GL_NO_ERROR)
                {
                    std::cout<<"GLERROR"<<std::endl;
                }
            }
            
            update_components();
            update_launch_params();
            render_optix();
            render_imgui();

            {
                GLenum err;
                while((err = glGetError()) != GL_NO_ERROR)
                {
                    std::cout<<"GLERROR"<<std::endl;
                }
            }

            glfw->swap_buffers("DefaultWindow");
            process_command_queue();
            
            


            prev_mouse = cur_mouse;
            cur_mouse = glfw->get_cursor_pos("DefaultWindow");
            cur_mouse[1] = (display_h - 1) - cur_mouse[1];

            /* reset history if we've clicked something */
            LP.reset = false;
            int left = glfw->get_button_action("DefaultWindow", 0);
            int right = glfw->get_button_action("DefaultWindow", 1);
            int middle = glfw->get_button_action("DefaultWindow", 2);
            auto scroll = glfw->get_scroll_offset("DefaultWindow");

            if ((!io.WantCaptureMouse) && ((left != 0)  || (right != 0) || (middle != 0) || (abs(scroll[1]) > 0.f))) {
                reset_accumulation();
            }

            if ((left != 0) && (!io.WantCaptureMouse)) {
                glm::vec2 prevmouse = glm::vec2(prev_mouse[0], prev_mouse[1]) / glm::vec2(curr_frame_size.x, curr_frame_size.y);
                glm::vec2 currmouse = glm::vec2(cur_mouse[0], cur_mouse[1]) / glm::vec2(curr_frame_size.x, curr_frame_size.y);
                prevmouse *= 2.f; prevmouse -= 1.f;
                currmouse *= 2.f; currmouse -= 1.f;
                camera_controls.rotate(prevmouse, currmouse);
            }

            if ((right != 0) && (!io.WantCaptureMouse)) {
                glm::vec2 prevmouse = glm::vec2(prev_mouse[0], prev_mouse[1]) / glm::vec2(curr_frame_size.x, curr_frame_size.y);
                glm::vec2 currmouse = glm::vec2(cur_mouse[0], cur_mouse[1]) / glm::vec2(curr_frame_size.x, curr_frame_size.y);
                prevmouse *= 2.f; prevmouse -= 1.f;
                currmouse *= 2.f; currmouse -= 1.f;
                camera_controls.pan((currmouse - prevmouse));
            }

            if ((middle != 0) && (!io.WantCaptureMouse)) {
                glm::vec2 prevmouse = glm::vec2(prev_mouse[0], prev_mouse[1]) / glm::vec2(curr_frame_size.x, curr_frame_size.y);
                glm::vec2 currmouse = glm::vec2(cur_mouse[0], cur_mouse[1]) / glm::vec2(curr_frame_size.x, curr_frame_size.y);
                prevmouse *= 2.f; prevmouse -= 1.f;
                currmouse *= 2.f; currmouse -= 1.f;
                float dist = glm::distance(prevmouse, currmouse);
                float sign = ((currmouse.y - prevmouse.y) > 0.f) ? 1.f : -1.f;
                camera_controls.zoom(dist * sign * 100.f);
            }

            if ((abs(scroll[1]) > 0.f) && (!io.WantCaptureMouse)) {
                camera_controls.zoom(scroll[1] * 4.f);
            }
        }

        ImGui::DestroyContext();
        return true;
    }

    std::future<void> RenderSystem::enqueueCommand(std::function<void()> function)
    {
        if (render_thread_id != std::this_thread::get_id()) 
            std::lock_guard<std::mutex> lock(qMutex);
        Command c;
        c.function = function;
        c.promise = std::make_shared<std::promise<void>>();
        auto new_future = c.promise->get_future();
        commandQueue.push(c);
        // cv.notify_one();
        return new_future;
    }

    /* These commands can be called from separate threads, but must be run on the event thread. */

    void RenderSystem::create_window(string key, uint32_t width, uint32_t height, bool floating, bool resizable, bool decorated) {
        using namespace Libraries;
        auto glfw = GLFW::Get();
        if (glfw->does_window_exist(key))
            throw std::runtime_error( std::string("Error: window already exists, cannot create window"));
        
        auto createWindow = [key, width, height, floating, resizable, decorated]() {
            using namespace Libraries;
            auto glfw = GLFW::Get();
            glfw->create_window(key, width, height, floating, resizable, decorated);
        };

        auto future = enqueueCommand(createWindow);
        future.wait();
    }

    void RenderSystem::destroy_window(string key) {
        using namespace Libraries;
        auto glfw = GLFW::Get();
        if (!glfw->does_window_exist(key))
            throw std::runtime_error( std::string("Error: window does not exist, cannot close window"));
        
        auto closeWindow = [key]() {
            using namespace Libraries;
            auto glfw = GLFW::Get();
            glfw->destroy_window(key);
        };

        auto future = enqueueCommand(closeWindow);
        future.wait();
    }

    void RenderSystem::swap_buffers() {
        auto swap_buffers = [] () {
            using namespace Libraries;
            auto glfw = GLFW::Get();
            glfw->swap_buffers("DefaultWindow");
        };

        auto future = enqueueCommand(swap_buffers);
        future.wait();
    }

    void RenderSystem::resize_window(string key, uint32_t width, uint32_t height) {
        using namespace Libraries;
        auto glfw = GLFW::Get();
        if (!glfw->does_window_exist(key)){
            throw std::runtime_error("Error: Window does not exist");
        }

        auto resizeWindow = [key, width, height] () {
            using namespace Libraries;
            auto glfw = GLFW::Get();
            glfw->resize_window(key, width, height);
        };

        auto future = enqueueCommand(resizeWindow);
        future.wait();
    }

    void RenderSystem::set_window_pos(string key, uint32_t x, uint32_t y) {
        using namespace Libraries;
        auto glfw = GLFW::Get();
        if (!glfw->does_window_exist(key)){
            throw std::runtime_error("Error: Window does not exist");
        }

        auto setWindowPos = [key, x, y] () {
            using namespace Libraries;
            auto glfw = GLFW::Get();
            glfw->set_window_pos(key, x, y);
        };

        auto future = enqueueCommand(setWindowPos);
        future.wait();
    }

    void RenderSystem::set_window_visibility(string key, bool visible) {
        using namespace Libraries;
        auto glfw = GLFW::Get();
        if (!glfw->does_window_exist(key)){
            throw std::runtime_error("Error: Window does not exist");
        }

        auto setWindowVisibility = [key, visible] () {
            using namespace Libraries;
            auto glfw = GLFW::Get();
            glfw->set_window_visibility(key, visible);
        };

        auto future = enqueueCommand(setWindowVisibility);
        future.wait();
    }

    void RenderSystem::set_background_color(glm::vec4 color)
    {
        LP.background_color = color;
        reset_accumulation();
    }

    void RenderSystem::set_tri_mesh_color(glm::vec4 color)
    {
        LP.tri_mesh_color = color;
        reset_accumulation();
    }

    void RenderSystem::save_camera(std::string path)
    {
        std::ofstream outfile (path,std::ofstream::binary);
        // TransformStruct cam_struct = camera_transform->get_struct();
        glm::mat4 cam_transform = camera_controls.inv_transform();
        glm::vec3 cam_center = camera_controls.center();
        // io::writeElement(outfile, cam_struct);
        io::writeElement(outfile, cam_transform);
        io::writeElement(outfile, cam_center);
        outfile.close();
    }
    
    void RenderSystem::load_camera(std::string path)
    {
        std::ifstream infile(path,std::ofstream::binary);
        // TransformStruct cam_struct;
        glm::mat4 cam_transform;
        glm::vec3 cam_center;
        // io::readElement(infile, cam_struct);
        io::readElement(infile, cam_transform);
        io::readElement(infile, cam_center);

        camera_controls.initialize(
            glm::column(cam_transform, 3), 
            cam_center, 
            glm::column(cam_transform, 1)
        );

        reset_accumulation();
        
        infile.close();
    }

    void RenderSystem::save_color_map(std::vector<uint8_t> color_map, std::string path)
    {
        int width = color_map.size() / 4;
        stbi_write_png(path.c_str(), /* width */width, /* height */ 1, 
                        /* num channels*/ 4, color_map.data(), /* stride in bytes */ width * 4);
    }

    void RenderSystem::load_color_map(std::string path)
    {
        int x, y, comp;
        uint8_t *image = stbi_load(path.c_str(), &x, &y, &comp, STBI_rgb_alpha);   
        std::vector<uint8_t> colors(4 * x);
        memcpy(colors.data(), image, colors.size() * sizeof(uint8_t));
        stbi_image_free(image);

        Colormap colormap(path, colors, ColorSpace::LINEAR);
        colormap.change_alpha_control_pts = true;
        tfn_widget.add_colormap(colormap);
        // return colors;
    }

    void RenderSystem::set_attenuation(float attenuation)
    {
        LP.attenuation = attenuation;
    }

    void RenderSystem::set_opacity(float opacity)
    {
        LP.opacity = opacity;
    }

    void RenderSystem::set_lod(int lod) {
        auto func = [this, lod]() {
            LP.volume_type = lod;
        };
        if (render_thread_id == std::this_thread::get_id()) 
            func();
        else
            enqueueCommand(func);
    }
    
    void RenderSystem::set_color_map_range(float minimum, float maximum)
    {
        LP.transferFunctionMin = minimum;
        LP.transferFunctionMax = maximum;
    }

    void RenderSystem::set_zoom(float zoom)
    {
        LP.zoom = zoom;
    }

    void RenderSystem::enable_id_colors()
    {
        LP.enable_id_colors = true;
    }

    void RenderSystem::disable_id_colors()
    {
        LP.enable_id_colors = false;
    }

    void RenderSystem::enable_mirror()
    {
        LP.mirror = true;
    }

    void RenderSystem::disable_mirror()
    {
        LP.mirror = false;
    }

    void RenderSystem::show_heatmap() {
        auto func = [this]() {
            LP.show_time_heatmap = true;
            LP.show_samples_heatmap = false;
        };
        if (render_thread_id == std::this_thread::get_id()) 
            func();
        else
            enqueueCommand(func);
    }

    void RenderSystem::show_volume() {
        auto func = [this]() {
            LP.show_time_heatmap = false;
            LP.show_samples_heatmap = false;
        };
        if (render_thread_id == std::this_thread::get_id()) 
            func();
        else
            enqueueCommand(func);
    }

    void RenderSystem::set_heatmap_range(float minimum, float maximum) {
        LP.time_min = minimum;
        LP.time_max = maximum;
    }

    void RenderSystem::enable_adaptive_sampling()
    {
        auto func = [this]() {
            LP.enable_adaptive_sampling = true;
        };
        if (render_thread_id == std::this_thread::get_id()) 
            func();
        else
            enqueueCommand(func);
    }

    void RenderSystem::disable_adaptive_sampling()
    {
        auto func = [this]() {
            LP.enable_adaptive_sampling = false;
        };
        if (render_thread_id == std::this_thread::get_id()) 
            func();
        else
            enqueueCommand(func);
    }

    void RenderSystem::enable_empty_space_skipping()
    {
        auto func = [this]() {
            LP.enable_space_skipping = true;
        };
        if (render_thread_id == std::this_thread::get_id()) 
            func();
        else
            enqueueCommand(func);
    }

    void RenderSystem::disable_empty_space_skipping()
    {
        auto func = [this]() {
            LP.enable_space_skipping = false;
        };
        if (render_thread_id == std::this_thread::get_id()) 
            func();
        else
            enqueueCommand(func);
    }

    void RenderSystem::quit() {
        auto func = [this]() {
            close = true;
        };
        if (render_thread_id == std::this_thread::get_id()) 
            func();
        else
            enqueueCommand(func);
    }

    double RenderSystem::render_image(int samples_per_pixel, std::string path, bool quit_on_finish) {
        double avg_duration = 0.f;
        auto renderimg = [this, samples_per_pixel, path, quit_on_finish, &avg_duration]() {
            auto glfw = Libraries::GLFW::Get();
            // if (!rendering) {
            //     rendering = true;
            // }

            reset_accumulation();
            update_adaptive_sampling_metadata();
            update_components();
            LP.reset = false;
            
            
            for (uint32_t i = 0; i < samples_per_pixel; ++i)
            {
                update_launch_params();

                auto start = glfwGetTime();
                glfw->poll_events();
                render_optix();
                auto stop = glfwGetTime();
                double scaling = 1. / (double)(i + 1);
                avg_duration = (1.f / (stop - start)) * scaling + avg_duration * (1.0f - scaling); 

                glfw->swap_buffers("DefaultWindow");
                glfwSetWindowTitle(window, std::to_string(avg_duration).c_str());
            }

            save_image(path);       

            if (quit_on_finish) {
                close = true;
            } 

        };

        /* May be called asynchronously by python. Always run above code on main thread */
        if (render_thread_id == std::this_thread::get_id()) 
            renderimg();
        else {
            auto future = enqueueCommand(renderimg);
            future.wait();
        }

        return avg_duration;
    }

    double RenderSystem::get_fps(int samples_per_pixel) {
        double avg_duration = 0.f;
        auto renderimg = [this, samples_per_pixel, &avg_duration]() {
            auto glfw = Libraries::GLFW::Get();

            reset_accumulation();
            update_adaptive_sampling_metadata();
            update_components();
            LP.reset = false;

            // int num_devices = owlGetDeviceCount(context);
            // for (int i = 0; i < num_devices; ++i) {
            //     cudaSetDevice(i);
            //     cudaDeviceSynchronize();
            // }
            // cudaSetDevice(0);
            
            for (uint32_t i = 0; i < samples_per_pixel; ++i)
            {
                update_launch_params();
                glfw->poll_events();

                auto start = glfwGetTime();
                render_optix();
                auto stop = glfwGetTime();
                double scaling = 1. / (double)(i + 1);
                avg_duration = (1.f / (stop - start)) * scaling + avg_duration * (1.0f - scaling); 

                glfw->swap_buffers("DefaultWindow");
                glfwSetWindowTitle(window, std::to_string(avg_duration).c_str());
            }
        };

        /* May be called asynchronously by python. Always run above code on main thread */
        if (render_thread_id == std::this_thread::get_id()) 
            renderimg();
        else {
            auto future = enqueueCommand(renderimg);
            future.wait();
        }

        return avg_duration;
    }

    void RenderSystem::save_image(std::string path)
    {
        glfwGetFramebufferSize(window, &curr_frame_size.x, &curr_frame_size.y);

        const glm::vec4 *fb = (const glm::vec4*)owlBufferGetPointer(frameBuffer,0);

        // #error todo, convert fb to 256 image
        std::vector<uint8_t> colors(4 * curr_frame_size.x * curr_frame_size.y);
        for (size_t i = 0; i < (curr_frame_size.x * curr_frame_size.y); ++i) {
            colors[i * 4 + 0] = uint8_t(fb[i].r * 255);
            colors[i * 4 + 1] = uint8_t(fb[i].g * 255);
            colors[i * 4 + 2] = uint8_t(fb[i].b * 255);
            colors[i * 4 + 3] = uint8_t(fb[i].a * 255);
        }

        stbi_flip_vertically_on_write(true);
        stbi_write_png(path.c_str(), curr_frame_size.x, curr_frame_size.y, /* num channels*/ 4, colors.data(), /* stride in bytes */ curr_frame_size.x * 4);
    }

    void RenderSystem::toggle_window_fullscreen(string key)
    {
        using namespace Libraries;
        auto glfw = GLFW::Get();
        if (!glfw->does_window_exist(key)){
            throw std::runtime_error("Error: Window does not exist");
        }
        
        auto setWindowFullscreen = [key] () {
            using namespace Libraries;
            auto glfw = GLFW::Get();
            glfw->toggle_fullscreen(key);
        };

        auto future = enqueueCommand(setWindowFullscreen);
        future.wait();
    }

    bool RenderSystem::stop() {
        using namespace Libraries;
        if (!initialized) return false;
        if (!running) return false;

        close = true;
        running = false;       
        return true;
    }

    bool RenderSystem::should_close() {
        using namespace Libraries;
        using namespace Systems;
        return close;
    }

    RenderSystem::RenderSystem() {}
    RenderSystem::~RenderSystem() {}    
}
