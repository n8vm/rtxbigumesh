#pragma once

#include <owl/owl.h>
#include <owl/common/math/vec.h>

#ifdef __CUDACC__
#ifndef CUDA_DECORATOR
#define CUDA_DECORATOR __both__
#endif
#else
#ifndef CUDA_DECORATOR
#define CUDA_DECORATOR
#endif
#endif

#include "LaunchParams.hxx"

using namespace owl;

/* variables for the triangle mesh geometry */
struct TrianglesGeomData
{
  /*! base color we use for the entire mesh */
  vec3 *colors = nullptr;
  /*! array/buffer of vertex indices */
  ivec3 *index = nullptr;
  /*! array/buffer of vertex positions */
  vec3 *vertex = nullptr;
};

struct PartsGeomData
{
  PartInfo* parts_buffer;
};

struct MultileavesGeomData
{
  /* Just the bounds data, in case the other buffers aren't uploaded.
  Note, these are nullptr if the child elements are loaded in. */
  glm::u16vec4 *leaf_qbbmin = nullptr;
  glm::u16vec4 *leaf_qbbmax = nullptr;
  uint16_t *leaf_qsmin = nullptr;
  uint16_t *leaf_qsmax = nullptr;

  MultiLeaf    *leaf_buffer = nullptr;
  glm::vec4    *vertex_buffer = nullptr;
  glm::u16vec4 *index_buffer = nullptr;
  uint32_t      part_num;
  glm::vec4 parent_bbmin;
  glm::vec4 parent_bbmax;
};

/* variables for the ray generation program */
struct RayGenData
{int placeholder;};

/* variables for the miss program */
struct MissProgData
{int placeholder;};

// Useful for adaptive sampling min/max/variance computation 
inline CUDA_DECORATOR
void lookupTableAddrs(float s, glm::vec4 *tfn, int tfn_width, float lowerBound, float upperBound, int &low, int &high, float &alpha) {
  if (lowerBound == upperBound) return;
  s = (s - lowerBound) / (upperBound - lowerBound);
  float faddress = glm::clamp(s, 0.f, 1.f) * float(tfn_width - 1);
  low = glm::floor(faddress);
  high = glm::ceil(faddress);
  alpha = faddress - float(low);
}

inline CUDA_DECORATOR
vec4 lookupTable(float s, glm::vec4 *tfn, int tfn_width, float lowerBound, float upperBound) {
  if (lowerBound == upperBound) return vec4(1.f, 0.f, 1.f, 1.f);
  s = (s - lowerBound) / (upperBound - lowerBound);
  float faddress = glm::clamp(s, 0.f, 1.f) * float(tfn_width - 1);
  int low = glm::floor(faddress), high = glm::ceil(faddress);
  float alpha = faddress - float(low);
  return glm::mix(tfn[low], tfn[high], alpha);
}

inline CUDA_DECORATOR 
glm::vec3 to_vec3(owl::vec3f other) {
  return glm::vec3(other.x, other.y, other.z);
}

inline CUDA_DECORATOR 
bool clip_ray_to_box(
  glm::vec3 origin, glm::vec3 direction, float tmin, float tmax, 
  glm::vec3 bbmin, glm::vec3 bbmax, 
  float &new_tmin, float &new_tmax) 
{
  // Clip ray to volume bounding box
  glm::vec3 t_lo = (bbmin - origin) / direction;
  glm::vec3 t_hi = (bbmax - origin) / direction;
  
  glm::vec3 t_nr = glm::min(t_lo,t_hi);
  glm::vec3 t_fr = glm::max(t_lo,t_hi);
  
  new_tmin = glm::max(tmin, glm::max(float(t_nr.x), glm::max(float(t_nr.y), float(t_nr.z))));
  new_tmax = glm::min(tmax, glm::min(float(t_fr.x), glm::min(float(t_fr.y), float(t_fr.z))));

  return (new_tmin < new_tmax);
}

inline CUDA_DECORATOR 
glm::vec3 toLinear(glm::vec3 sRGB)
{
    glm::bvec3 cutoff = glm::lessThan(sRGB, vec3(0.04045));
    glm::vec3 higher = glm::pow((sRGB + glm::vec3(0.055)) / glm::vec3(1.055), glm::vec3(2.4));
    glm::vec3 lower = sRGB/glm::vec3(12.92);

    return glm::mix(higher, lower, cutoff);
}

/*! helper function that creates a semi-random color from an ID */
inline CUDA_DECORATOR
glm::vec3 random_color(int i)
{
  int r = unsigned(i)*13*17 + 0x234235;
  int g = unsigned(i)*7*3*5 + 0x773477;
  int b = unsigned(i)*11*19 + 0x223766;
  return glm::vec3((r&255)/255.f,
                  (g&255)/255.f,
                  (b&255)/255.f);
}

inline CUDA_DECORATOR
glm::mat4 mirror(glm::vec3 v, glm::vec3 p)
{
  // mat[col][row]
  // row 0
  glm::mat4 mat = glm::mat4(1.f);
  mat[0][0] = 1 - 2 * pow(v.x, 2);
  mat[1][0] = -2 * v.x * v.y;
  mat[2][0] = -2 * v.x * v.z;
  mat[3][0] = 2 * glm::dot(p,v) * v.x;
  // row 1
  mat[0][1] = -2*v.x*v.y;
  mat[1][1] = 1-2*pow(v.y,2);
  mat[2][1] = -2*v.y*v.z;
  mat[3][1] = 2*glm::dot(p,v)*v.y;

  // row 2
  mat[0][2] = -2*v.x*v.z;
  mat[1][2] = -2*v.y*v.z;
  mat[2][2] = 1-2*pow(v.z,2);
  mat[3][2] = 2*glm::dot(p,v)*v.z;

  // row 3
  mat[0][3] = 0;
  mat[1][3] = 0;
  mat[2][3] = 0;
  mat[3][3] = 1;
  return mat;
}