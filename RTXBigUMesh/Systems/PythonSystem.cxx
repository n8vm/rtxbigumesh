#include "./PythonSystem.hxx"
#include <errcode.h>
#include "RTXBigUMesh/Tools/Options.hxx"
#include <iostream>
#include <cstdlib>
#include <csignal>

#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <stdio.h>

#include <vector>
#include <sstream>
#include <iostream>
#include <iomanip>

#include "RTXBigUMesh/Systems/RenderSystem.hxx"
#include "RTXBigUMesh/Tools/WhereAmI.hxx"

#include <stdio.h>

#include "RTXBigUMesh/Tools/IO.hxx"

#ifdef MS_WINDOWS
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif /* MS_WINDOWS */

namespace Systems
{

std::mutex PythonSystem::qMutex;
std::queue<PythonSystem::Command> PythonSystem::commandQueue = {};

#include <Python-ast.h>

PythonSystem::PythonSystem()
{
}
PythonSystem::~PythonSystem()
{
}

PythonSystem *PythonSystem::Get()
{
    static PythonSystem instance;
    return &instance;
}

void PythonSystem::push(std::string command_line, PythonResult &result)
{
    result.command = command_line;
    result.complete = false;

    auto push_command = [command_line, &result]() {
        try {
            std::stringstream ss;
            // std::string command;
            // if (show_ps1) {
            //     std::cout<< ">>>";
            // } else {
            //     std::cout<< "...";
            // }
            
            // std::getline(cin, command);

            ss << std::quoted(command_line);
            std::string command = ss.str();

            // std::cin >> command;
            // std::cout<<command<<std::endl;
            // PyRun_InteractiveOneObjectEx(command.c_str());
            PyRun_SimpleString("o = io.StringIO()");
            PyRun_SimpleString("e = io.StringIO()");
            PyRun_SimpleString((std::string("with redirect_stdout(o), redirect_stderr(e): ") + std::string("complete = interpreter.push(" + command + ")")).c_str());
            PyRun_SimpleString("out = o.getvalue()");
            PyRun_SimpleString("err = e.getvalue()");
                        
            PyObject *mainModule = PyImport_AddModule("__main__");
            PyObject *complete = PyObject_GetAttrString(mainModule, "complete");
            PyObject *out = PyObject_GetAttrString(mainModule, "out");
            PyObject *err = PyObject_GetAttrString(mainModule, "err");
            Py_ssize_t out_size, err_size;
            
            int val = PyLong_AsLong(complete);
            const char* out_cstr = PyUnicode_AsUTF8AndSize(out, &out_size);
            const char* err_cstr = PyUnicode_AsUTF8AndSize(err, &err_size);
            std::string out_str = std::string(out_cstr);
            std::string err_str = std::string(err_cstr);


            // show_ps1 = (val == 0);
            result.output = out_str;
            result.error = err_str;
            result.complete = (val == 0);
        } catch (const std::exception& ex) {
            std::cout<<ex.what()<<std::endl;
        }
    };
    result.future = enqueueCommand(push_command);
}

void PythonSystem::interupt()
{
    PyErr_SetInterrupt();
}

std::future<void> PythonSystem::enqueueCommand(std::function<void()> function)
{
    std::lock_guard<std::mutex> lock(qMutex);
    Command c;
    c.function = function;
    c.promise = std::make_shared<std::promise<void>>();
    auto new_future = c.promise->get_future();
    commandQueue.push(c);
    // cv.notify_one();
    return new_future;
}

void PythonSystem::process_command_queue()
{
    std::lock_guard<std::mutex> lock(qMutex);
    while (!commandQueue.empty()) {
        auto item = commandQueue.front();
        item.function();
        try {
            item.promise->set_value();
        }
        catch (std::future_error& e) {
            if (e.code() == std::make_error_condition(std::future_errc::promise_already_satisfied))
                std::cout << "PythonSystem: [promise already satisfied]\n";
            else
                std::cout << "PythonSystem: [unknown exception]\n";
        }
        commandQueue.pop();
    }
}

bool PythonSystem::initialize()
{
    initialized = true;
    return true;
}

wchar_t *GetWC(const char *c)
{
    const size_t cSize = strlen(c) + 1;
    wchar_t *wc = new wchar_t[cSize];
    mbstowcs(wc, c, cSize);

    return wc;
}

bool PythonSystem::start()
{
    if (!initialized)
        return false;
    if (running)
        return false;

    auto loop = [this]() {

        // auto func = [this]() {
        // Don't write __pycache__ directories everywhere...
        Py_DontWriteBytecodeFlag = 1;

        // Initialize the python interpreter
        const wchar_t *name = L"RTXBigUMesh";
        Py_SetProgramName(const_cast<wchar_t *>(name));
        Py_Initialize();

        Py_InspectFlag = 1;
        Py_InteractiveFlag = 1;

        std::string executablePath = Options::GetExecutablePath();

        PyRun_SimpleString("import sys");
        PyRun_SimpleString("import os");

        // Insert executable path into system path. 
        // Allows python interpreter to import RTXBigMesh.so
        PyRun_SimpleString(std::string(std::string("sys.path.insert(0, r\"") + executablePath + std::string("\")")).c_str());
        PyRun_SimpleString(std::string(std::string("sys.path.insert(0, os.path.join(r\"") + executablePath + std::string("\", \"..\" ))")).c_str());

        PyRun_SimpleString("import code");
        PyRun_SimpleString("from contextlib import redirect_stdout");
        PyRun_SimpleString("from contextlib import redirect_stderr");
        PyRun_SimpleString("import io");
        

        // if (Options::IsMainModuleSet())
        // {
        //     auto fp = fopen(Options::GetMainModule().c_str(), "r");
        //     PyRun_AnyFile(fp, Options::GetMainModule().c_str() );
        // }

        PyRun_SimpleString("interpreter = code.InteractiveConsole()");


        if (Options::IsMainModuleSet()) {
            std::ifstream file(Options::GetMainModule());
            std::string str;
            while (std::getline(file, str)) {
                PythonResult result;
                push(str.c_str(), result);
                process_command_queue();
                // ExecCommand(str.c_str());
                if (result.output.length() > 0)
                    std::cout<<result.output<<std::endl;
                if (result.error.length() > 0)
                    std::cout<<"error " << result.error<<std::endl;
            }
        }

        bool show_ps1 = true;
        while (running) {
            process_command_queue();
        }

        Py_Finalize();

        // If we quit the python interpreter, close all windows
        auto rs = Systems::RenderSystem::Get();
        rs->stop();
    };

    running = true;
    eventThread = thread(loop);

    return true;
}

bool PythonSystem::stop()
{
    if (!initialized)
        return false;
    if (!running)
        return false;
    running = false;
    eventThread.join();
    return true;
}

bool PythonSystem::should_close()
{
    return running;
}
} // namespace Systems
