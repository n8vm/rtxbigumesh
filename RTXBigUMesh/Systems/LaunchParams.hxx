#pragma once

#include <owl/owl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_access.hpp>

#include "RTXBigUMesh/Components/EntityStruct.hxx"
#include "RTXBigUMesh/Components/TransformStruct.hxx"
#include "RTXBigUMesh/Components/BUMeshStruct.hxx"

struct LaunchParams {
    uint64_t frame = 0;
    uint64_t startFrame = 0;
    bool reset = false;
    bool enable_adaptive_sampling = true;
    bool enable_space_skipping = true;
    bool enable_id_colors = false;
    bool mirror = true;
    float zoom = 1.f;

    bool enable_pathtracer = true;
    bool show_time_heatmap = false;
    bool show_samples_heatmap = false;
    float time_min = 0.f;
    float time_max = 100.f;
    uint32_t volume_type = 0;

    EntityStruct camera_entity;
    TransformStruct camera_transform;
    glm::mat4 view;
    glm::mat4 viewinv;
    glm::mat4 proj;
    glm::mat4 projinv;

    // EntityStruct* entities;
    // TransformStruct* transforms;


    glm::ivec2 frame_size;
    glm::vec4 *fbPtr;
    uint32_t *accumPtr;
    OptixTraversableHandle world;

    glm::vec4 *transferFunction;
    uint16_t transferFunctionWidth;
    float transferFunctionMin = 0.f;
    float transferFunctionMax = 1.f;
    glm::vec4 *part_max_color;
    glm::vec4 *part_min_color;
    glm::vec4 *part_var_color;
    glm::vec4 max_color;
    float attenuation = 1.f; 
    float opacity = 1.f; 
    float min_step_size = .01f; 
    float max_step_size = .01f; 
    float adaptive_power = 3.f; 
    float empty_threshold = .0f; 
    
    
    OptixTraversableHandle multiLeavesAccel;
    OptixTraversableHandle partsAccel;
    glm::vec4 bumesh_bbmin = glm::vec4(0.f);
    glm::vec4 bumesh_bbmax = glm::vec4(0.f);
    TransformStruct bumesh_transform;

    glm::vec4 tri_mesh_bbmin = glm::vec4(0.f);
    glm::vec4 tri_mesh_bbmax = glm::vec4(0.f);
    glm::vec4 tri_mesh_color = glm::vec4(1.f);
    glm::vec4 background_color = glm::vec4(1.f);
    TransformStruct tri_mesh_transform;
};
