
#pragma once

#include "owl/llowl.h"
#include "RTXBigUMesh/Tools/System.hxx"
#include "RTXBigUMesh/Tools/arcball_camera.hxx"
#include "RTXBigUMesh/Libraries/GLFW.hxx"

#include "RTXBigUMesh/Components/Entity.hxx"
#include "RTXBigUMesh/Components/Transform.hxx"
#include "RTXBigUMesh/Components/BUMesh.hxx"
#include "transfer_function_widget.h"


#include "LaunchParams.hxx"
#include <queue>

namespace Systems 
{
    class RenderSystem : public System {
        public:
            static RenderSystem* Get();
            bool initialize();
            bool cleanup();
            bool start();
            bool stop();
            bool should_close();
            
            void resize_window(string key, uint32_t width, uint32_t height);
            void set_window_pos(string key, uint32_t x, uint32_t y);
            void set_window_visibility(string key, bool visible);
            void toggle_window_fullscreen(string key);

            void set_background_color(glm::vec4 color);
            void set_tri_mesh_color(glm::vec4 color);

            void swap_buffers();
            
            void set_bumesh(BUMesh *mesh, bool mirror = false);
            void set_bumesh_transform(Transform *transform);
            void set_tri_mesh_transform(Transform *transform);
            
            void save_camera(std::string path);
            void load_camera(std::string path);
            void save_color_map(std::vector<uint8_t> color_map, std::string path);
            void load_color_map(std::string path);
            void set_attenuation(float attenuation);
            void set_opacity(float opacity);
            void set_lod(int lod);
            void set_color_map_range(float minimum, float maximum);
            void show_heatmap();
            void show_volume();
            void set_heatmap_range(float minimum, float maximum);
            void enable_adaptive_sampling();
            void disable_adaptive_sampling();
            void enable_empty_space_skipping();
            void disable_empty_space_skipping();
            void quit();
            double render_image(int samples_per_pixel, std::string path, bool quit_on_finish = false);
            double get_fps(int samples_per_pixel);
            void save_image(std::string path);
            void reset_camera(float zoom);
            void reset_accumulation();
            void set_zoom(float zoom);
            void enable_id_colors();
            void disable_id_colors();
            void enable_mirror();
            void disable_mirror();
    
        private:
            bool rendering = false; 
            TransferFunctionWidget tfn_widget;
            std::thread::id render_thread_id;

            GLFWwindow* window = nullptr;

            OWLContext context;
            OWLModule module;
            GLuint imageTexID = -1;
            cudaGraphicsResource_t cudaResourceTex;
            OWLBuffer frameBuffer;
            OWLBuffer accumBuffer;
            OWLBuffer transferFunctionBuffer;
            
            // for space skipping and adaptive sampling
            OWLBuffer partMinColorBuffer;
            OWLBuffer partMaxColorBuffer;
            OWLBuffer partColorVarBuffer;

            OWLGeomType trianglesGeomType; // several multileaves per multileaf geom
            OWLGeomType multiLeavesGeomType; // several multileaves per multileaf geom
            OWLGeomType partsGeomType; // several parts per part geom
            
            OWLLaunchParams launchParams;
            // OWLGroup partsAccel;

            LaunchParams LP;

            // OWLBuffer entitiesBuffer;
            // OWLBuffer transformsBuffer;
            Entity *camera = nullptr;
            Transform *camera_transform = nullptr;
            
            BUMesh *bumesh = nullptr;
            Transform *bumesh_transform = nullptr;
            // OWLBuffer bumesh_part_info_buffer;

            Transform *tri_mesh_transform = nullptr;
        
            ArcballCamera camera_controls = ArcballCamera(
                glm::vec3(0.f, 0.f, -5.f), 
                glm::vec3(0.f), 
                glm::vec3(0.f, 1.f, 0.f)
            );

            glm::ivec2 curr_frame_size = glm::ivec2(0);
            glm::ivec2 last_frame_size = glm::ivec2(0);

            OWLRayGen rayGen;
            OWLMissProg missProg;


            float r = 1, g = 1, b = 1;
            // Making these private. Simplifying things a bit by only allowing one window.
            void create_window(string key, uint32_t width = 512, uint32_t height = 512, bool floating = true, bool resizable = true, bool decorated = true);
            void destroy_window(string key);
            void process_command_queue();
            
            void update_launch_params();
            void update_components();
            void render_optix();
            void render_imgui();
            void initialize_framebuffer(int fb_width, int fb_height);
            
            void update_adaptive_sampling_metadata();

            bool close = true;
            RenderSystem();
            ~RenderSystem();

            struct Command {
                std::function<void()> function;
                std::shared_ptr<std::promise<void>> promise;
            };

            static std::condition_variable cv;
            static std::mutex qMutex;
            static std::queue<Command> commandQueue;

            std::future<void> enqueueCommand(std::function<void()> function);
    };   
}
