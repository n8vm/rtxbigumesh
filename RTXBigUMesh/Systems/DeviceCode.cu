#include "DeviceCode.hxx"
#include <optix_device.h>
#include "cuda_fp16.h"
#include <owl/common/math/random.h>

#define PI 3.141592654f
#define TAU ( PI * 2.0f )

using namespace glm;
typedef owl::common::LCG<4> Random;

/*! launch parameters in constant memory, filled in by optix upon
      optixLaunch (this gets filled in from the buffer we pass to
      optixLaunch) */
extern "C" __constant__ LaunchParams optixLaunchParams;

// /* Ray Types */
// typedef owl::RayT<0,2> QueryRay;
// typedef owl::RayT<1,2> SpaceSkippingRay;

// BUMesh Part Bounding Box    
#define NUM_BINS 32
struct PartPayload {
  int type = 0;
  int count = 0;
  // int ids[NUM_BINS];
  // float bounds_min[NUM_BINS];
  // float bounds_max[NUM_BINS];
  // float occupancy_bins[NUM_BINS] = {0.f}; // REMOVE THIS!!!
  float bins[NUM_BINS] = {-1.f};
  float datavalue = -1.f;
  int id = -1;

  __device__
  PartPayload() {
    for (int i = 0; i < NUM_BINS; ++i) bins[i] = -1;
  }
};

OPTIX_BOUNDS_PROGRAM(PartBounds)(const void  *geomData,
  box3f       &primBounds,
  const int    primID)
{
  const PartsGeomData &self = *(const PartsGeomData*)geomData;
  primBounds = box3f();
  primBounds.lower.x = self.parts_buffer[primID].bounds_min.x;
  primBounds.lower.y = self.parts_buffer[primID].bounds_min.y;
  primBounds.lower.z = self.parts_buffer[primID].bounds_min.z;
  primBounds.upper.x = self.parts_buffer[primID].bounds_max.x;
  primBounds.upper.y = self.parts_buffer[primID].bounds_max.y;
  primBounds.upper.z = self.parts_buffer[primID].bounds_max.z;
}

OPTIX_CLOSEST_HIT_PROGRAM(PartCH)()
{
  const PartsGeomData &self = owl::getProgramData<PartsGeomData>();
  PartPayload &prd = owl::getPRD<PartPayload>();
  const int   primID = optixGetPrimitiveIndex();

  vec4 bbmin = self.parts_buffer[primID].bounds_min;
  vec4 bbmax = self.parts_buffer[primID].bounds_max;
  
  float lmin_val = bbmin[3];
  float lmax_val = bbmax[3];

  float val = lmax_val;//float(lmin_val + lmax_val) * .5;
  prd.datavalue = val;//(val - gminval) / (gmaxval - gminval);
}

OPTIX_INTERSECT_PROGRAM(PartINT)()
{
  PartPayload &prd = owl::getPRD<PartPayload>();
  const vec3f org  = optixGetObjectRayOrigin();
  const vec3f dir  = optixGetObjectRayDirection();  
  float tmax      = optixGetRayTmax();
  const float tmin = optixGetRayTmin();
  int primID = optixGetPrimitiveIndex();

  const PartsGeomData &self = owl::getProgramData<PartsGeomData>();
  float hit_tmin, hit_tmax;
  glm::vec3 origin = glm::vec3(org.x, org.y, org.z);
  glm::vec3 direction = glm::vec3(dir.x, dir.y, dir.z);
  vec3 bbmin = glm::vec3(self.parts_buffer[primID].bounds_min);
  vec3 bbmax = glm::vec3(self.parts_buffer[primID].bounds_max);
  bool hit = clip_ray_to_box(origin, direction, tmin, tmax, bbmin, bbmax, hit_tmin, hit_tmax);

  if (!hit) return;

  if (prd.type == 0) {
    if (tmax <= optixGetRayTmax()) {
      optixReportIntersection(tmax, 0);
      prd.id = primID;
    }
  } else {
    
    /* Map the hit min and max tmax to the occupancy bins */
    float fminhitaddr = (hit_tmin - tmin) / (tmax - tmin);
    float fmaxhitaddr = (hit_tmax - tmin) / (tmax - tmin);    
    int minhitaddr = floor(fminhitaddr * (NUM_BINS-1));
    int maxhitaddr = ceil(fmaxhitaddr * (NUM_BINS-1));

    /* Also lookup the transferred max color value */
    glm::vec4 max_color = optixLaunchParams.part_max_color[primID];
    glm::vec4 norm_variance = optixLaunchParams.part_var_color[primID];
    float max_alpha = max_color[3];
    float max_variance = max(max(norm_variance.x, norm_variance.y), norm_variance.z);

    /* Skip "splatting" empty bounds */
    if (max_alpha <= optixLaunchParams.empty_threshold) return;

    /* Splat that max value onto the bins */
    for (int i = minhitaddr; i <= maxhitaddr; ++i) {
      // prd.occupancy_bins[i] = max(prd.occupancy_bins[i], max_alpha);
      if (optixLaunchParams.enable_pathtracer) {
        prd.bins[i] = max(prd.bins[i], max_alpha);
      } else {
        prd.bins[i] = max(prd.bins[i], max_variance);
      }
    }


    // // printf("TEST\n");
    // if (prd.count < 20) {
    //   prd.ids[prd.count] = optixGetPrimitiveIndex();
    //   prd.bounds_min[prd.count] = new_tmin;
    //   prd.bounds_max[prd.count] = new_tmax;
    // }

    prd.count += 1;
  }
}

// BUMesh Multileaf Bounding Box    
struct MultiLeafPayload {
  uint8_t type = 0;
  float datavalue = -1.f;
  int id = -1;
};

OPTIX_BOUNDS_PROGRAM(MultiLeafBounds)(const void  *geomData,
  box3f       &primBounds,
  const int    primID)
{
  const MultileavesGeomData &self = *(const MultileavesGeomData*)geomData;
  glm::u16vec4 qbbmin = (self.leaf_qbbmin == nullptr) ? self.leaf_buffer[primID].quantBoundsMin : self.leaf_qbbmin[primID];
  glm::u16vec4 qbbmax = (self.leaf_qbbmax == nullptr) ? self.leaf_buffer[primID].quantBoundsMax : self.leaf_qbbmax[primID];
  vec4 bbmin, bbmax;
  decode_multileaf_bounds(
    qbbmin, qbbmax, 
    self.parent_bbmin, self.parent_bbmax,
    bbmin, bbmax);

  primBounds = box3f();
  primBounds.lower.x = bbmin.x;
  primBounds.lower.y = bbmin.y;
  primBounds.lower.z = bbmin.z;
  primBounds.upper.x = bbmax.x;
  primBounds.upper.y = bbmax.y;
  primBounds.upper.z = bbmax.z;
}

OPTIX_CLOSEST_HIT_PROGRAM(MultiLeafCH)()
{
  MultiLeafPayload &prd = owl::getPRD<MultiLeafPayload>();
  // const int   primID = optixGetPrimitiveIndex();
  // const MultileavesGeomData &self = owl::getProgramData<MultileavesGeomData>();

  // vec4 bbmin, bbmax;
  // decode_multileaf_bounds(
  //   self.leaf_buffer[primID].quantBoundsMin, self.leaf_buffer[primID].quantBoundsMax, 
  //   self.parent_bbmin, self.parent_bbmax,
  //   bbmin, bbmax);

  // float lmin_val = bbmin[3];
  // float lmax_val = bbmax[3];

  // float val = float(lmin_val + lmax_val) * .5;// / 5.f;
  // float gminval = optixLaunchParams.bumesh_bbmin.w;
  // float gmaxval = optixLaunchParams.bumesh_bbmax.w;
  // prd.datavalue = (val - gminval) / (gmaxval - gminval);

  // if (prd.type == 1) {
  // }
  prd.datavalue = __int_as_float(optixGetAttribute_0());
  prd.id = optixGetAttribute_1();
}

OPTIX_INTERSECT_PROGRAM(MultiLeafINT)()
{
  /* Reject all multileaves outside beyond tmax. */
  float hit_t      = optixGetRayTmax();
  if (hit_t > optixGetRayTmax()) return;

  // Decode multileaf
  int primID = optixGetPrimitiveIndex();
  MultiLeafPayload &prd = owl::getPRD<MultiLeafPayload>();
  const MultileavesGeomData &self = owl::getProgramData<MultileavesGeomData>();
  

  float gminval = optixLaunchParams.bumesh_bbmin.w;
  float gmaxval = optixLaunchParams.bumesh_bbmax.w;

  // IF INTERSECTING MULTILEAF BOUNDS
  if (prd.type == 0) {
    // glm::u16vec4 qbbmin = (self.leaf_qsmin == nullptr) ? self.leaf_buffer[primID].quantBoundsMin : self.leaf_qbbmin[primID];
    // glm::u16vec4 qbbmax = (self.leaf_qsmax == nullptr) ? self.leaf_buffer[primID].quantBoundsMax : self.leaf_qbbmax[primID];
    // vec4 ml_bbmin, ml_bbmax;
    // decode_multileaf_bounds(
    //   qbbmin, qbbmax, 
    //   self.parent_bbmin, self.parent_bbmax,
    //   ml_bbmin, ml_bbmax);
    // float ml_smin = ml_bbmin.w, ml_smax = ml_bbmax.w;

    uint16_t qsmin = (self.leaf_qsmin == nullptr) ? self.leaf_buffer[primID].quantBoundsMin.w : self.leaf_qsmin[primID];
    uint16_t qsmax = (self.leaf_qsmax == nullptr) ? self.leaf_buffer[primID].quantBoundsMax.w : self.leaf_qsmax[primID];
    float ml_smin, ml_smax;
    decode_multileaf_scalar_bounds(
      qsmin, qsmax, 
      self.parent_bbmin.w, self.parent_bbmax.w,
      ml_smin, ml_smax);

    float val = ml_smax;//(ml_smax + ml_smin) * .5f;
    float datavalue = val;//(val - gminval) / (gmaxval - gminval);
    optixReportIntersection(hit_t, 0, __float_as_int(datavalue), primID);
    return;
  }
  // IF INTERSECTING MULTILEAF CHILD BOUNDS OR ELEMENTS 
  else 
  {
    glm::u16vec4 qbbmin = self.leaf_buffer[primID].quantBoundsMin;
    glm::u16vec4 qbbmax = self.leaf_buffer[primID].quantBoundsMax;
    vec4 ml_bbmin, ml_bbmax;
    decode_multileaf_bounds(
      qbbmin, qbbmax, 
      self.parent_bbmin, self.parent_bbmax,
      ml_bbmin, ml_bbmax);

    const MultiLeaf &multiLeaf = self.leaf_buffer[primID];
    const vec3f org  = optixGetObjectRayOrigin();
    const vec3f dir  = optixGetObjectRayDirection();  
    float hit_t      = optixGetRayTmax();
    const float tmin = optixGetRayTmin();

    const MultiLeafChildren &children = multiLeaf.children;
    vec3 point = vec3(org[0], org[1], org[2]);

    /* Loop through all child nodes belonging to this multileaf,
       testing if the ray origin intersect any... */
    for (int cID = 0; cID < MultiLeaf::WIDTH; ++cID) {
      vec4 c_bbmin, c_bbmax;
      
      // Check if node is invalid, skip those that are not...
      if (children.dim[0].child[cID].bounds_lo == 0xf &&
        children.dim[0].child[cID].bounds_hi == 0xf) continue;
      
      // Decode child bounds
      decode_leaf_bounds(
        children, cID,
        ml_bbmin, ml_bbmax,
        c_bbmin, c_bbmax);

      // Skip children that the ray origin does not intersect
      if (!(glm::all(glm::lessThan(vec3(c_bbmin), point)) 
            && glm::all(glm::greaterThan(vec3(c_bbmax), point)))) continue;

      // IF INTERSECTING MULTILEAF CHILD BOUNDS
      if (prd.type == 1) {
        float val = c_bbmax.w;//(c_bbmin.w + c_bbmax.w) * .5f;
        float datavalue = val;//(val - gminval) / (gmaxval - gminval);
        optixReportIntersection(hit_t, 0, __float_as_int(datavalue), primID * (cID + 1));
        return;
      } 
      // Intersect actual unstructured primitive
      else {
        // Compute offset to first index
        uint32_t indexOffset = multiLeaf.leafChildrenOffset;
        for (int i=0;i<cID;i++) {
          int tot  = children.primCount[i].numTotal;
          int tets = children.primCount[i].numTets;
          indexOffset += (2*tot-tets);
        }
        int numTets  = children.primCount[cID].numTets;
        int numTotal = children.primCount[cID].numTotal;

        /* First check to see if we intersect any of the tetrahedra */
        for (int i = 0; i < numTotal; ++i) {
          glm::u16vec4 idx0 = self.index_buffer[indexOffset++];
          glm::u16vec4 idx1 = (i < numTets) ? glm::u16vec4(-1) : self.index_buffer[indexOffset++];
          uint16_t element_indices[8] = {idx0[0],idx0[1],idx0[2],idx0[3],idx1[0],idx1[1],idx1[2],idx1[3]};
          float val = -1.f;
          bool hit = interpolateElement(element_indices, self.vertex_buffer, point, val);
          // Skip elements that the ray origin does not intersect
          if (!hit) continue;
          // printf("INTERSECTION\n");
          float datavalue = val;// (val - gminval) / (gmaxval - gminval);
          optixReportIntersection(hit_t, 0, __float_as_int(datavalue), (primID * (cID + 1)) * (i + 1));
          return;
        }
      }
    }
  }
}

// Avoid using too many registers here. 
struct TriMeshPayload {
  float r = -1.f, g = -1.f, b = -1.f, tmax = -1.f;
};


OPTIX_CLOSEST_HIT_PROGRAM(TriangleMesh)()
{
  TriMeshPayload &prd = owl::getPRD<TriMeshPayload>();

  const TrianglesGeomData &self = owl::getProgramData<TrianglesGeomData>();
  float2 bc = optixGetTriangleBarycentrics();

  // compute normal:
  const int   primID = optixGetPrimitiveIndex();
  const ivec3 index  = self.index[primID];
  const vec3 &A     = self.vertex[index.x];
  const vec3 &B     = self.vertex[index.y];
  const vec3 &C     = self.vertex[index.z];
  const vec3 &ACol = (self.colors == nullptr) ? vec3(optixLaunchParams.tri_mesh_color) : self.colors[index.x];
  const vec3 &BCol = (self.colors == nullptr) ? vec3(optixLaunchParams.tri_mesh_color) : self.colors[index.y];
  const vec3 &CCol = (self.colors == nullptr) ? vec3(optixLaunchParams.tri_mesh_color) : self.colors[index.z];
  const vec3 Ng     = normalize(cross(B-A,C-A));

  const vec3f rayDir = optixGetWorldRayDirection();
  vec3 dir = vec3(rayDir.x, rayDir.y, rayDir.z);

  vec3 vcol = ACol * (1.f - (bc.x + bc.y)) + BCol * bc.x + CCol * bc.y;

  vec3 color = (.2f + .8f*fabs(dot(dir,Ng)))*vcol;
  prd.r = color.x;
  prd.g = color.y;
  prd.b = color.z;
  prd.tmax = optixGetRayTmax();
}

inline __device__ 
vec3 get_miss_color()
{
  return vec3(optixLaunchParams.background_color);
}

OPTIX_MISS_PROGRAM(miss)()
{
}

const vec2i pixelID = owl::getLaunchIndex();

// const MissProgData &self = owl::getProgramData<MissProgData>();

inline __device__ 
float sample_free_path(float majorant, float rnd)
{
    return -log(rnd + 1e-4f) / majorant;  // log(0) -> -INF
}

inline __device__ 
bool is_null_collision(float sigma_extinction, float majorant_extinction, float rnd)
{
    return rnd > sigma_extinction / majorant_extinction;
}

inline __device__ 
float null_ratio(float sigma_extinction, float majorant_extinction)
{
    return 1.0f - sigma_extinction / majorant_extinction;
}

inline __device__ 
float sample_volume_extinction(vec3 p, Random &random, int &element_id) {
  int volume_type = optixLaunchParams.volume_type;
  // if (volume_type == 0) {
  //     vec3 pos = p + vec3(0.5f, 0.5f, 0.5f);
  //     int steps = 3;
  //     for (int i = 0; i < steps; ++i) {
  //         pos *= 3.0f;
  //         int s = (int(pos.x) & 1) + (int(pos.y) & 1) + (int(pos.z) & 1);
  //         if (s >= 2)
  //             return 0.0f;
  //     }
  //     if ((int(pos.x) & 1)) return .0;
  //     if ((int(pos.y) & 1)) return .25;
  //     if ((int(pos.z) & 1)) return .5;
  // } else if (volume_type == 1) {
  //     float r = 0.5f * (0.5f - abs(p.y));
  //     float a = float(M_PI * 8.0) * p.y;
  //     float dx = (cos(a) * r - p.x) * 2.0f;
  //     float dy = (sin(a) * r - p.z) * 2.0f;
  //     return pow(max((1.0f - dx * dx - dy * dy), 0.0f), 8.0f);
  // } else 
  if (volume_type == 0) {
    // bigmesh point query
    PartPayload pprd;
    pprd.datavalue = -1.f;
    owl::Ray ray;
    ray.origin = vec3f(p.x, p.y, p.z);
    ray.direction = vec3f(1.f,1.f,1.f);
    ray.tmin =  0.f;
    ray.tmax = .0001f;
    owl::traceRay(/*accel to trace against*/optixLaunchParams.partsAccel,
                  /*the ray to trace*/ray,
                  /*prd*/pprd);
    element_id = pprd.id;
    return pprd.datavalue;
  }
  else if (volume_type == 1) {
    // bigmesh point query
    MultiLeafPayload mlprd;
    mlprd.datavalue = -1.f;
    mlprd.type = 0;
    owl::Ray ray;
    ray.origin = vec3f(p.x, p.y, p.z);
    ray.direction = vec3f(1.f,1.f,1.f);//normalize(vec3f(random(), random(), random()) * 2.f - 1.f);
    ray.tmin =  0.f;
    ray.tmax = .0001f;
    owl::traceRay(/*accel to trace against*/optixLaunchParams.multiLeavesAccel,
                  /*the ray to trace*/ray,
                  /*prd*/mlprd);
    element_id = mlprd.id;
    return mlprd.datavalue;
  }
  else if (volume_type == 2) {
    // bigmesh point query
    MultiLeafPayload mlprd;
    mlprd.datavalue = -1.f;
    mlprd.type = 1;
    owl::Ray ray;
    ray.origin = vec3f(p.x, p.y, p.z);
    ray.direction = vec3f(1.f,1.f,1.f);//normalize(vec3f(random(), random(), random()) * 2.f - 1.f);
    ray.tmin =  0.f;
    ray.tmax = .0001f;
    owl::traceRay(/*accel to trace against*/optixLaunchParams.multiLeavesAccel,
                  /*the ray to trace*/ray,
                  /*prd*/mlprd);
    element_id = mlprd.id;
    return mlprd.datavalue;
  } else {
    // bigmesh point query
    MultiLeafPayload mlprd;
    mlprd.datavalue = -1.f;
    mlprd.type = 2;
    owl::Ray ray;
    ray.origin = vec3f(p.x, p.y, p.z);
    ray.direction = vec3f(1.f,1.f,1.f);//normalize(vec3f(random(), random(), random()) * 2.f - 1.f);
    ray.tmin =  0.f;
    ray.tmax = .0001f;
    owl::traceRay(/*accel to trace against*/optixLaunchParams.multiLeavesAccel,
                  /*the ray to trace*/ray,
                  /*prd*/mlprd);
    element_id = mlprd.id;
    return mlprd.datavalue;
  }
}

inline __device__ 
void raycast_volume(vec4 &color, vec3 origin, vec3 direction, float tmin, float tmax, vec3 bbmin, vec3 bbmax, Random &random, float base_step_size, float step_size, int &samples_taken) {
  int eid = -1;
  int max_samples_taken = 4000;
  samples_taken = 0;
  
  float offset = (random() - .5f) * step_size;
  // float offset = 0.f;
  for (float t = tmin + offset; t < tmax + offset; t += step_size) {
    // float max_dt = tmax - t;
    // float displacement = min(step_size, max_dt);
    // float t_offset = t + random() * max_dt;
    vec3 p = origin + direction * t;
    float s = sample_volume_extinction(p, random, eid);
    if (s >= 0.f) {
      
      samples_taken++;
      if (samples_taken > max_samples_taken) {
        color = vec4(1.f, 0.f, 1.f, 1.f); // Dont allow too many samples per pixel. Locks up the device.
        return;
      }

      vec4 sample = lookupTable(
        s, 
        optixLaunchParams.transferFunction, 
        optixLaunchParams.transferFunctionWidth, 
        optixLaunchParams.transferFunctionMin,
        optixLaunchParams.transferFunctionMax
      );
      sample.a = 1.f - pow(1.f - sample.a, (step_size / base_step_size) / optixLaunchParams.attenuation); // slight modification here, dividing by atteuation instead of base step size
      // sample.a = pow(sample.a, optixLaunchParams.attenuation);
      // sample.a *= optixLaunchParams.attenuation;
      // vec4 sample = vec4(s, s, s, s * .01f);
      
      color += (1.f-color.a) * sample.a * vec4(vec3(sample),1.f);
    }
    if (color.a > .98f) break;
  }
}

/* local and global majorant extinction should be between 0 and 1. */
inline __device__ 
void pathtrace_volume(vec4 &color, vec3 origin, vec3 direction, float &t, float tmin, float tmax, Random &random, float local_majorant_extinction, float global_majorant_extinction, int &samples_taken) 
{
  int eid = -1;
  local_majorant_extinction /= optixLaunchParams.attenuation;
  global_majorant_extinction /= optixLaunchParams.attenuation;
  local_majorant_extinction *= optixLaunchParams.opacity;
  global_majorant_extinction *= optixLaunchParams.opacity;
  local_majorant_extinction = glm::clamp(local_majorant_extinction, 0.f, global_majorant_extinction);
  
  t = tmin;

  vec3 pos;
  float gminval = optixLaunchParams.bumesh_bbmin.w;
  float gmaxval = optixLaunchParams.bumesh_bbmax.w;
  while (true)
  {   
    // Majorant_extinction is relative to some unknown inverse distance unit.
    // We can control opacity of the volume by scaling this unit 
    t = t - log(1.f - random()) / (local_majorant_extinction);
    pos = origin + direction * t;

    /* A boundary has been hit */
    if (double(t) > double(tmax)) {
        color = glm::vec4(0.f);
        return;
    }

    /* Sample the current scatter position, lookup alpha in colormap */
    samples_taken++;
    float s = sample_volume_extinction(pos, random, eid);
    
    /* Sample lands outside the volume, so extinction is 0. */
    if (s < 0.f) continue; 

    /* Fail safe to avoid locking up GPU */
    if (samples_taken > 4000) {
      color = glm::vec4(1.f, 0.f, 1.f, 1.f);
      return;
    }

    vec4 c = lookupTable(
      (s - gminval) / (gmaxval - gminval), 
      optixLaunchParams.transferFunction, 
      optixLaunchParams.transferFunctionWidth, 
      optixLaunchParams.transferFunctionMin,
      optixLaunchParams.transferFunctionMax
    );

    float extinction = c.a * optixLaunchParams.opacity;
    
    /* An absorption/emission collision has occurred */
    if (random() <= (extinction / local_majorant_extinction))
    {
      if (optixLaunchParams.enable_id_colors) {
        Random random; random.init(eid + 6, eid + 1);
        color = lookupTable(random(), 
          optixLaunchParams.transferFunction,
          optixLaunchParams.transferFunctionWidth, 
          0.f, 1.f
        );
        color.a = 1.f;
        // color = vec4(random_color(eid), 1.f);
      }
      else 
        color = vec4(c.r, c.g, c.b, 1.f);
      return;
    }
  }
}


OPTIX_RAYGEN_PROGRAM(simpleRayGen)()
{
  float start = clock();

  const RayGenData &self = owl::getProgramData<RayGenData>();
  const vec2i pixelID = owl::getLaunchIndex();
  glm::vec2 frame_size = optixLaunchParams.frame_size;
  vec3 final_color;



  // {
  //   glm::vec3 final_color(0.f, 0.f, 0.f);
  //   const int fbOfs = pixelID.x+frame_size.x* ((frame_size.y - 1) -  pixelID.y);
  //   // vec3f color = vec3f(tprd.r,tprd.g,tprd.b);
  //   float alpha;
  //   if ((optixLaunchParams.startFrame == 0) || optixLaunchParams.reset) {
  //     alpha = .0f; 
  //   } else {
  //     alpha = .97f;//1.f - (1.f / float(optixLaunchParams.startFrame));   
  //   }
  //   int iprev = optixLaunchParams.accumPtr[fbOfs];
  //   vec3 prev = vec3( ((iprev & (255 << 0 )) >> 0) / 255.f, 
  //                     ((iprev & (255 << 8 )) >> 8) / 255.f, 
  //                     ((iprev & (255 << 16)) >> 16) / 255.f
  //                   );
  //   vec3 new_color = final_color * (1.f - alpha) + prev * alpha;
  //   vec4f color = vec4f(42.f / 255.f, 42.f / 255.f, 42.f / 255.f, 42.f / 255.f);//vec4f(new_color.r,new_color.g,new_color.b, 1.f);

  //   if ((pixelID.x == 0) && (pixelID.y == 0)) {
  //     printf("SAVING COLOR %u %u   %f %f %f\n", optixLaunchParams.fbPtr[fbOfs], owl::make_rgba(color), color.x, color.y, color.z);
  //   }
  //   optixLaunchParams.fbPtr[fbOfs]    = owl::make_rgba(color);
  //   optixLaunchParams.accumPtr[fbOfs] = owl::make_rgba(color);
  //   return;
  // }
  
  EntityStruct camera = optixLaunchParams.camera_entity;
  TransformStruct camera_transform = optixLaunchParams.camera_transform;
  TransformStruct bumesh_transform = optixLaunchParams.bumesh_transform;
  TransformStruct tri_mesh_transform = optixLaunchParams.tri_mesh_transform;

  uint64_t offset = (optixLaunchParams.startFrame == 0) ? optixLaunchParams.frame : optixLaunchParams.startFrame;
  offset = (offset % 10000);//uint32_t(100 * (frame_size.x * frame_size.y));
  offset *= (frame_size.x * frame_size.y);
  // if ((pixelID.x == int(frame_size.x / 2)) && (pixelID.y == int(frame_size.y / 2))) {
  //   printf("startFrame: %d\n", optixLaunchParams.startFrame);
  //   printf("offset: %lld\n"    , offset);
    
  //   // , offset, optixLaunchParams.startFrame);
  // }
  Random random; random.init(pixelID.x + offset, pixelID.y + offset);
  
  
  glm::mat4 camWorldToLocal = camera_transform.worldToLocal;
  glm::mat4 projinv = optixLaunchParams.projinv;
  glm::mat4 viewinv = optixLaunchParams.viewinv * camWorldToLocal;
  glm::vec2 in_uv = glm::vec2(pixelID.x, pixelID.y) / frame_size;
  if (optixLaunchParams.zoom > 0.f) {
    in_uv /= optixLaunchParams.zoom;
    in_uv += (.5f - (.5f / optixLaunchParams.zoom));
  }
  glm::vec2 dir = in_uv * 2.f - 1.f; dir.y *= -1.f;
  glm::vec4 t = (projinv * glm::vec4(dir.x, dir.y, 1.f, 1.f));
  glm::vec3 target = glm::vec3(t) / float(t.w);
  glm::vec3 origin = glm::vec3(viewinv * glm::vec4(0.f,0.f,0.f,1.f));
  glm::vec3 direction = glm::vec3(viewinv * glm::vec4(target, 0.f));
  direction = glm::normalize(direction);

  owl::Ray ray;
  ray.tmin = .0f;
  ray.tmax = 1e38f;//10000.0f;
  ray.origin = vec3f(origin.x, origin.y, origin.z);
  ray.direction = vec3f(direction.x, direction.y, direction.z);

  glm::mat4 worldToLocal = bumesh_transform.worldToLocal;
  // if (optixLaunchParams.mirror) {
  //   glm::vec3 p = vec3(optixLaunchParams.bumesh_bbmax);
  //   glm::vec3 v = vec3(0.f, 1.f, 0.f);
  //   worldToLocal = worldToLocal * glm::transpose(mirror(p,v));
  // }
  vec3 o = to_vec3(ray.origin);
  vec3 d = to_vec3(ray.direction);
  o = vec3(worldToLocal * vec4(o, 1.0f));
  d = glm::normalize(vec3(worldToLocal * vec4(d, 0.0f)));

  // vec3 t_o = to_vec3(ray.origin);
  // vec3 t_d = to_vec3(ray.direction);
  // t_o = vec3(tri_mesh_transform.worldToLocal * vec4(o, 1.0f));
  // t_d = glm::normalize(vec3(tri_mesh_transform.worldToLocal * vec4(d, 0.0f)));


  // ray.origin = vec3f(t_o.x, t_o.y, t_o.z);
  // ray.direction = vec3f(t_d.x, t_d.y, t_d.z);
  // ray.origin = vec3f(o.x, o.y, o.z);
  // ray.direction = vec3f(d.x, d.y, d.z);
  ray.origin = vec3f(o.x, o.y, o.z);
  ray.direction = vec3f(d.x, d.y, d.z);

  owl::Ray mirrorRay;
  
  // ------------------------------------------------------------------
  // first, trace against the surfaces
  // ------------------------------------------------------------------
  TriMeshPayload tprd;
  TriMeshPayload mtprd;
  owl::traceRay(/*accel to trace against*/optixLaunchParams.world,
                /*the ray to trace*/ray,
                /*prd*/tprd);  

  // if (optixLaunchParams.mirror) {
  //   glm::mat4 reflection = glm::scale(glm::mat4(1.0f), glm::vec3(1.f, 1.f, -1.f));
  //   glm::vec3 mo = o;//glm::vec3(reflection * glm::vec4(o, 1.f));
  //   glm::vec3 md = glm::vec3(reflection * glm::vec4(d, 0.f));
  //   mirrorRay = ray;
  //   mirrorRay.origin = vec3f(mo.x, mo.y, mo.z);
  //   mirrorRay.direction = vec3f(md.x, md.y, md.z);
  //   owl::traceRay(/*accel to trace against*/optixLaunchParams.world,
  //                 /*the ray to trace*/mirrorRay,
  //                 /*prd*/mtprd);
  // }

  // Shade the surface position, shorten the ray to surface (if we hit one)
  if (float(tprd.tmax) >= 0.f) {
    ray.tmax = tprd.tmax;
  }

  // // Shade the surface position, shorten the ray to surface (if we hit one)
  // if ((float(mtprd.tmax) >= 0.f) && (mtprd.tmax < tprd.tmax)) {
  //   ray.tmax = mtprd.tmax;
  //   tprd = mtprd;
  // }
  
  // ------------------------------------------------------------------
  // Now perform ray casting
  // ------------------------------------------------------------------
  PartPayload pprd;

  vec4 result = vec4(0.0);
  int samples_taken = 0; 

  {
    vec3 bbmin = vec3(optixLaunchParams.bumesh_bbmin);
    vec3 bbmax = vec3(optixLaunchParams.bumesh_bbmax);
    
    float oldtmin = ray.tmin;
    float oldtmax = ray.tmax;
    bool intersected_volume = clip_ray_to_box(
        o, d, oldtmin, oldtmax, bbmin, bbmax, 
        /* new tmin/max */ ray.tmin, ray.tmax  
    );

    // if ((pixelID.x == (frame_size.x / 2)) && (pixelID.y == (frame_size.y / 2))) {
      // printf("TMIN %f, TMAX %f BB %f %f %f %f %f %f %f %f NTMIN %f NTMAX %f\n",
      // old_tmin, old_tmax, bbmin.x, bbmin.y, bbmin.z, bbmax.x, bbmax.y, bbmax.z, ray.tmin, ray.tmax
      // );
      // result = vec4(0.f,0.f,0.f,1.f);
    // }

    if (intersected_volume) 
    {
      float min_step_size = optixLaunchParams.min_step_size;
      float max_step_size = optixLaunchParams.max_step_size;
      glm::vec3 lengths = bbmax - bbmin;
      float max_side_length = glm::max(lengths.x, glm::max(lengths.y, lengths.z));
      min_step_size *= max_side_length;
      max_step_size *= max_side_length;

      owl::Ray ssray = ray;      
      
      pprd.type = 1;
      pprd.count = 0;
      ssray.tmin = ray.tmin;
      ssray.tmax = ray.tmax;
      ssray.origin = vec3f(o.x, o.y, o.z);
      ssray.direction = vec3f(d.x, d.y, d.z);
      owl::traceRay(/*accel to trace against*/optixLaunchParams.partsAccel,
                    /*the ray to trace*/ ssray,//ssray,
                    /*prd*/pprd);

      
      

      // if not skipping space
      // result = raycast_volume(o, d, ray.tmin, ray.tmax, bbmin, bbmax, random, step_size, samples_taken);
      result = vec4(0.f);
      float t = ray.tmin;
      for (int i = 0; i < NUM_BINS; ++i) {
        float fminhitaddr = float(i) / float(NUM_BINS);
        float fmaxhitaddr = float(i+1) / float(NUM_BINS);
        
        float tmin = glm::mix(ray.tmin, ray.tmax, fminhitaddr);
        float tmax = glm::mix(ray.tmin, ray.tmax, fmaxhitaddr); 
        
        /* Negative values indicate empty buckets that should be skipped */
        if ((!optixLaunchParams.enable_space_skipping) || pprd.bins[i] >= 0.f)
        {
          if (optixLaunchParams.enable_pathtracer) {
            float global_majorant_extinction = optixLaunchParams.max_color.w;// * optixLaunchParams.bumesh_bbmax.w;
            float local_majorant_extinction = (optixLaunchParams.enable_adaptive_sampling) ? pprd.bins[i] : optixLaunchParams.max_color.w;
            pathtrace_volume(result, o, d, t, tmin, tmax, random, local_majorant_extinction, global_majorant_extinction, samples_taken);
          }
          else 
          {
            // From space skipping paper... 
            float variance = pprd.bins[i];
            float step_size = (optixLaunchParams.enable_adaptive_sampling) ? 
              max(min_step_size + (max_step_size - min_step_size) * pow(abs(min(variance, 1.f) - 1.f), optixLaunchParams.adaptive_power), min_step_size) 
              : min_step_size;
            raycast_volume(result, o, d, tmin, tmax, bbmin, bbmax, random, min_step_size, step_size, samples_taken);
          }
        } 
        if (result.a > .98) break;
      }
    } 
  }

  float stop = clock();
  
  if (optixLaunchParams.show_time_heatmap) {
    final_color = glm::vec3(lookupTable(
      (stop - start), 
      optixLaunchParams.transferFunction, 
      optixLaunchParams.transferFunctionWidth, 
      optixLaunchParams.time_min,
      optixLaunchParams.time_max
    ));
  } 
  else if (optixLaunchParams.show_samples_heatmap) {
    final_color = glm::vec3(lookupTable(
      (samples_taken / float(4000)), 
      optixLaunchParams.transferFunction, 
      optixLaunchParams.transferFunctionWidth, 
      optixLaunchParams.time_min,
      optixLaunchParams.time_max
    ));
  }  
  else {    
    final_color = (vec3(result) * result.a) + 
                       (((float(tprd.tmax) < 0) ? get_miss_color() : vec3(tprd.r,tprd.g,tprd.b)) * (1.f - result.a));
    // final_color = (vec3(result) * result.a) + (get_miss_color() * (1.f - result.a));
  }

 
  // color.x = in_uv.x;
  // color.y = in_uv.y;
  // color.z = 1.f;
  // color = vec3f(origin.x, origin.y, origin.z);
  
  const int fbOfs = pixelID.x+frame_size.x* ((frame_size.y - 1) -  pixelID.y);
  float alpha;
  if ((optixLaunchParams.startFrame == 0) || optixLaunchParams.reset) {
    alpha = .0f; 
  } else {
    alpha = 1.f - (1.f / float(optixLaunchParams.startFrame));   
  }
  // int iprev = optixLaunchParams.accumPtr[fbOfs];
  vec3 prev = vec3(optixLaunchParams.fbPtr[fbOfs]);
  // vec3( ((iprev & (255 << 0 )) >> 0) / 255.f, 
  //                   ((iprev & (255 << 8 )) >> 8) / 255.f, 
  //                   ((iprev & (255 << 16)) >> 16) / 255.f
  //                 );
  vec3 new_color = final_color * (1.f - alpha) + prev * alpha;
  new_color = glm::min(new_color, glm::vec3(1.f));

  // vec3f color = vec3f(test//vec3f(new_color.r,new_color.g,new_color.b);
  
  optixLaunchParams.fbPtr[fbOfs]    = glm::vec4(new_color, 1.f);
  // optixLaunchParams.accumPtr[fbOfs] = glm::vec4(new_color, 1.f);

  // if (fbOfs == 0) {
  //   printf("random: %f\n", random());
  // }
}
