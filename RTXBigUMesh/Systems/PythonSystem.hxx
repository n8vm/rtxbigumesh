#pragma once
#include <Python.h>
#include <queue>

#include "RTXBigUMesh/Tools/System.hxx"

struct PythonResult {
    std::string command;
    std::string output;
    std::string error;
    bool complete;

    // Above not ready until future notifies.
    std::shared_future<void> future;
};

namespace Systems {
    class PythonSystem : public System {
        public:
            static PythonSystem* Get();
            bool initialize();
            bool start();
            bool stop();
            bool should_close();
            void push(std::string command_line, PythonResult &result);
            void interupt();
        private:
            struct Command {
                std::function<void()> function;
                std::shared_ptr<std::promise<void>> promise;
            };
            static std::queue<Command> commandQueue;
            static std::mutex qMutex;
            void process_command_queue();
            std::future<void> enqueueCommand(std::function<void()> function);

            PythonSystem();
            ~PythonSystem();
    };
}
