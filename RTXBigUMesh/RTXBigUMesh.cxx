#include "RTXBigUMesh.hxx"

#include "Systems/PythonSystem.hxx"
#include "Systems/RenderSystem.hxx"

#include "Components/Transform.hxx"
#include "Components/Entity.hxx"
#include "Components/BUMesh.hxx"

#include <iostream>

bool Initialized = false;

namespace RTXBigUMesh {
    /* Starts any syncronous and asynchronous systems. */
    void StartSystems() {
        std::cout<<"Starting systems" << std::endl;
        auto python_system = Systems::PythonSystem::Get();
        auto render_system = Systems::RenderSystem::Get();
        python_system->initialize();
        render_system->initialize();
        python_system->start();
        render_system->start();
    }

    /* Create any default scene components here. */
    void Initialize() {
        std::cout<<"Initializing..." << std::endl;
        Entity::Initialize();
        Transform::Initialize();
        BUMesh::Initialize();
    }

    /* Stops any syncronous and asynchronous systems that may currently be running*/
    void StopSystems() {
        auto python_system = Systems::PythonSystem::Get();
        auto render_system = Systems::RenderSystem::Get();
        
        python_system->stop();
        render_system->stop();

        RTXBigUMesh::CleanUp();        
        std::cout<<"Stopping systems" << std::endl;
    }

    /* Cleanup any allocated scene components here... */
    void CleanUp() {
        Initialized = false;

        std::cout<<"Cleaning Up..." << std::endl;
        Entity::CleanUp();
        Transform::CleanUp();
        BUMesh::CleanUp();
    }
};