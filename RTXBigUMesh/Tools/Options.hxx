#pragma once

#include <string>
#include <vector>
#include <set>
#include <map>

namespace Options
{
    bool ProcessArg(int &i, char **argv);
    int ProcessArgs(int argc, char **argv);

    std::string GetExecutablePath();
    std::string GetBUMeshPath();

    bool IsBUMeshSet();

    // For running python scripts from commandline.
    // Handled by the python system.
    bool IsMainModuleSet();
    std::string GetMainModule();
}; // namespace Options