#pragma once

#include <fstream>
#include <ostream>

namespace io {

template<typename T>
void readElement(std::ifstream &in, T &t)
{
    assert(in.good());
    in.read((char*)&t,sizeof(t));
}

template<typename T>
void readArray(std::ifstream &in, T *t, size_t N)
{
    assert(in.good());
    for (size_t i=0;i<N;i++)
        readElement(in,t[i]);
}

template<typename T>
void readVector(std::ifstream &in, std::vector<T> &t, const std::string &description)
{
    size_t N;
    readElement(in,N);
    t.resize(N);
    for (size_t i=0;i<N;i++)
        readElement(in,t[i]);
}



template<typename T>
void writeElement(std::ofstream &out, const T &t)
{
    out.write((char*)&t,sizeof(t));
    assert(out.good());
}

template<typename T>
void writeData(std::ofstream &out, const T *t, size_t N)
{
    for (size_t i=0;i<N;i++)
    write(out,t[i]);
    assert(out.good());
}

template<typename T>
void writeVector(std::ofstream &out, const std::vector<T> &vt)
{
    size_t N = vt.size();
    writeElement(out,N);
    for (auto &v : vt)
    writeElement(out,v);
    assert(out.good());
}


struct Exception : public std::exception {
};

struct CouldNotOpenException : public io::Exception {
    CouldNotOpenException(const std::string &fileName)
    : fileName(fileName)
    {}

    const std::string fileName;
};

struct ReadErrorException : public io::Exception {
};

struct WriteErrorException : public io::Exception {
};

}
