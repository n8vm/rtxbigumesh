#include <cstdlib>
#include <cstring>
#include <fstream>
#include "./Options.hxx"
#include <iostream>
#include <sys/stat.h>
#include <exception>

#include "./WhereAmI.hxx"

namespace Options
{
std::string ExecutablePath = "";
std::string MainModule = "";
std::string BUMeshPath = "";

bool isMainModuleSet = false;
bool isBUMeshSet = false;

#define $(flag) (strcmp(argv[i], flag) == 0)
bool ProcessArg(int &i, char **argv)
{
    int orig_i = i;

    if $("-bumesh")
    {
        ++i;
        BUMeshPath = std::string(argv[i]);
        ++i;
        std::cout<<"BUMesh dir set to : " << BUMeshPath << std::endl;
        isBUMeshSet = true;
    }
    
    // if $("-exampleflag")
    // {
    //     ++i;
    //     file = std::string(argv[i]);
    //     std::cout<<"Activating option. File is "<< file <<std::endl;
    //     ++i;
    //     exampleFlagSet = true;
    // }

    return i != orig_i;
}

int ProcessArgs(int argc, char **argv)
{
    using namespace Options;
    
    /* Get current executable path (Can be useful...) */
    int dirname_length;
    int length = wai_getExecutablePath(NULL, 0, NULL);
    ExecutablePath = std::string(length, '\0');
    wai_getExecutablePath(ExecutablePath.data(), length, &dirname_length);
    ExecutablePath = ExecutablePath.substr(0, dirname_length);
    
    int i = 1;
    bool stop = false;
    while (i < argc && !stop) {
        stop = true;
        if (ProcessArg(i, argv)) {
            stop = false;
        }
    }

    /* Process the first filename at the end of the arguments as a module to run */
    for (; i < argc; ++i) {
        std::string filename(argv[i]);
        
        /* Make sure that file exists */
        struct stat st;
        if (stat(filename.c_str(), &st) != 0)
            throw std::runtime_error( std::string(filename + " does not exist!"));

        MainModule = filename;
        isMainModuleSet = true;
    }
    return 0;
}

std::string GetExecutablePath()
{
    return ExecutablePath;
}

std::string GetBUMeshPath()
{
    return BUMeshPath;
}

std::string GetMainModule() {
    return MainModule;
}

bool IsMainModuleSet() 
{
    return isMainModuleSet;
}

bool IsBUMeshSet()
{
    return isBUMeshSet;
}


} // namespace Options
