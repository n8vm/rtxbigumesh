#pragma once

// tbb
#include <tbb/parallel_for.h>
#include <tbb/task_arena.h>
#include <tbb/task_scheduler_init.h>

template<typename INDEX_T, typename TASK_T>
inline void parallel_for(INDEX_T nTasks, TASK_T&& taskFunction)
{
    if (nTasks == 0) return;
    if (nTasks == 1)
        taskFunction(size_t(0));
    else
        tbb::parallel_for(INDEX_T(0), nTasks, std::forward<TASK_T>(taskFunction));
}

template<typename INDEX_T, typename TASK_T>
inline void serial_for(INDEX_T nTasks, TASK_T&& taskFunction)
{
    for (INDEX_T taskIndex = 0; taskIndex < nTasks; ++taskIndex) {
        taskFunction(taskIndex);
    }
}

template<typename TASK_T>
void serial_for_blocked(size_t begin, size_t end, size_t blockSize,
                        TASK_T &&taskFunction)
{
    for (size_t block_begin=begin; block_begin < end; block_begin += blockSize)
        taskFunction(block_begin,std::min(block_begin+blockSize,end));
}

template<typename TASK_T>
void parallel_for_blocked(size_t begin, size_t end, size_t blockSize,
                        const TASK_T &taskFunction)
{
    #if 0
    serial_for_blocked(begin,end,blockSize,taskFunction);
    #else
    const size_t numTasks = end-begin;
    const size_t numBlocks = (numTasks+blockSize-1)/blockSize;
    parallel_for(numBlocks,[&](size_t blockID){
        size_t block_begin = begin+blockID*blockSize;
        taskFunction(block_begin,std::min(block_begin+blockSize,end));
        });
    #endif
}