#include <thread>
#include <iostream>

#include "Tools/Options.hxx"
#include "RTXBigUMesh.hxx"

using namespace std;

// Parses arguments, then starts various asynchronous systems, 
// eg python interpreter, renderer, event handling, etc...
int main(int argc, char** argv)
{
    Options::ProcessArgs(argc, argv);
    RTXBigUMesh::Initialize();
    RTXBigUMesh::StartSystems();
    RTXBigUMesh::StopSystems();
    RTXBigUMesh::CleanUp();
    return 0;
}