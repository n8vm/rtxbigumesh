#include "./BUMesh.hxx"
#include <fstream>

#include "RTXBigUMesh/Tools/PrettyNumber.hxx"
#include <chrono>
#include <mutex>
#include <atomic>
#include <iomanip>
#include <algorithm>

#include "RTXBigUMesh/Tools/ParallelFor.hxx"
#include "RTXBigUMesh/Tools/IO.hxx"

#define TINYOBJLOADER_IMPLEMENTATION // define this in only *one* .cc
#include "tiny_obj_loader.h"

BUMesh BUMesh::bumeshes[MAX_BUMESHES];
BUMeshStruct BUMesh::bumesh_structs[MAX_BUMESHES];
std::map<std::string, uint32_t> BUMesh::lookupTable;
std::shared_ptr<std::mutex> BUMesh::creation_mutex;
bool BUMesh::Initialized = false;
bool BUMesh::Dirty = true;

BUMesh::BUMesh() {
	this->initialized = false;
}

BUMesh::BUMesh(std::string name, uint32_t id) {
	this->initialized = true;
	this->name = name;
	this->id = id;
    bumesh_structs[id].data_per_vertex = false;
    bumesh_structs[id].num_attributes = 0;
}

std::string BUMesh::to_string()
{
	std::string output;
	output += "{\n";
	output += "\ttype: \"BUMesh\",\n";
	output += "\tname: \"" + name + "\",\n";
	output += "\tid: \"" + std::to_string(id) + "\",\n";
	output += "}";
	return output;
}

BUMeshStruct &BUMesh::get_struct() {
	return bumesh_structs[id];
}

std::vector<PartInfo> &BUMesh::get_part_list()
{
	return part_info;
}

std::vector<std::vector<MultiLeaf>> &BUMesh::get_multileaf_lists()
{
	return multileaves;
}

std::vector<std::vector<glm::vec4>> &BUMesh::get_vertex_lists()
{
	return vertices;
}

std::vector<std::vector<glm::u16vec4>> &BUMesh::get_index_lists()
{
	return indices;
}

std::vector<glm::vec3> &BUMesh::get_triangle_mesh_vertices()
{
	return triangle_mesh_vertices;
}

std::vector<glm::ivec3> &BUMesh::get_triangle_mesh_indices()
{
	return triangle_mesh_indices;
}

std::vector<glm::vec3> &BUMesh::get_triangle_mesh_colors()
{
	return triangle_mesh_colors;
}

static std::mutex print_mu;
static void thread_safe_print(std::string text) {
	std::lock_guard<std::mutex> lock(print_mu);
	std::cout<<text;
}

void BUMesh::load_from(std::string file_base, bool read_triangle_mesh, bool read_multileaves, bool read_leaves, bool read_elements, int max_parts) {
	part_info.clear();
	auto t_load_begin = std::chrono::high_resolution_clock::now();

	// -------------------------------------------------------
    // part INFOs
    // -------------------------------------------------------
    {
      const std::string infoFileName = file_base+"summary";
      std::ifstream in(infoFileName, std::ios::binary);
      if (!in.good())
        throw std::runtime_error("fatal: could not open info file "+infoFileName);
      size_t count;
      in.read((char*)&count,sizeof(count));
	  count = std::min(count,(size_t)(max_parts));
      std::cout << "going to read " << PrettyNumber(count) << " part infos ..." << std::endl;
      part_info.resize(count);
      in.read((char*)part_info.data(),
              part_info.size()*sizeof(part_info[0]));
      std::cout << "#bm: loaded " << PrettyNumber(part_info.size())
                << " part summaries" << std::endl;
    }

	// -------------------------------------------------------
    // Triangle Mesh
    // -------------------------------------------------------
	if (read_triangle_mesh) {
		auto tri_file = file_base + "tris";
		auto tri_obj_file = file_base + "tris.obj";
		std::ifstream in_tri(tri_file, std::ios::binary);
		// std::ifstream in_obj(tri_obj_file, std::ios::binary);

		/* First try to read custom tris binary file */
		if (in_tri.good()) {
			io::readVector(in_tri, triangle_mesh_vertices,"triangle vertices");
        	io::readVector(in_tri, triangle_mesh_indices,"triangle indices");
			if (triangle_mesh_vertices.size() > 0) {
				triangle_mesh_read = true;
			}
		} 

		/* Else try loading an obj w. vertex colors */
		else {
			tinyobj::attrib_t attrib;
			std::vector<tinyobj::shape_t> shapes;
			std::vector<tinyobj::material_t> materials;

			std::string warn;
			std::string err;
			bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, tri_obj_file.c_str());

			if (!warn.empty()) {
				std::cout << warn << std::endl;
			}

			if (!err.empty()) {
				std::cerr << err << std::endl;
			}
			else {
				triangle_mesh_vertices.resize(attrib.vertices.size()/3);
				triangle_mesh_colors.resize(triangle_mesh_vertices.size());
				
				/* Copy over per vertex data */
				for (size_t v = 0; v < triangle_mesh_vertices.size(); ++v) {
					triangle_mesh_vertices[v] = glm::vec3(
						attrib.vertices[v * 3 + 0],
						attrib.vertices[v * 3 + 1],
						attrib.vertices[v * 3 + 2]
					);
					triangle_mesh_colors[v] = glm::vec3(
						attrib.colors[v * 3 + 0],
						attrib.colors[v * 3 + 1],
						attrib.colors[v * 3 + 2]
					);
				}

				/* Copy over indices */

				// Loop over shapes
				for (size_t s = 0; s < shapes.size(); s++) {
					// Loop over faces(polygon)
					size_t index_offset = 0;
					for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
						int fv = shapes[s].mesh.num_face_vertices[f];
						assert(fv == 3);

						glm::ivec3 indices;
						indices[0] = shapes[s].mesh.indices[3 * f + 0].vertex_index;
						indices[1] = shapes[s].mesh.indices[3 * f + 1].vertex_index;
						indices[2] = shapes[s].mesh.indices[3 * f + 2].vertex_index;

						triangle_mesh_indices.push_back(indices);
					}
				}

				if (triangle_mesh_indices.size() > 0)
					triangle_mesh_read = true;
			}
		}

		/* Finally, if we still havent read a triangle mesh, warn the user. */
		if (!triangle_mesh_read) {
			std::cout << "Info: no triangle mesh read, skipping..." << std::endl;
		} 
		
	}

	if (read_multileaves) {
		// -------------------------------------------------------
		// part DATAs
		// -------------------------------------------------------
		std::cout << "#bm: loading " << PrettyNumber(part_info.size()) << " data blocks ..." << std::endl;
		multileaves.resize(part_info.size());

		if (read_elements) {
			vertices.resize(part_info.size());
			indices.resize(part_info.size());
		} else {
			std::cout << "#bm: Skipping elements..." << std::endl;
		}

		std::atomic_int32_t count = 0;

		// for (int partID=0;partID<partInfo.size();partID++) {
		parallel_for(part_info.size(),[&](int partID) {
			count += 1;
			// auto text = ;std::setprecision(5) <<
			thread_safe_print(std::string("\r#bm: Loading ") + std::to_string(float(count) / part_info.size()) + std::string("%"));
			// std::cout << ;// << std::endl;
			char partNumString[12];
			sprintf(partNumString,"%09i",partID);
			const std::string partFileName = file_base+"_part_"+partNumString+std::string(".bum");
			std::ifstream in(partFileName, std::ios::binary);
			if (!in.good())
				throw std::runtime_error("fatal: could not open part file "+partFileName);
			io::readVector(in,multileaves[partID], "multileaves");
			if (read_elements) {
				io::readVector(in,indices[partID],"indices");
				io::readVector(in,vertices[partID],"vertices");
			}
		});
	} else {
		std::cout << "#bm: Skipping multileaves...";
	}
	
	auto t_load_end = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::seconds>(t_load_end - t_load_begin);
    std::cout << "\n#bm, done loading data parts, took "
              <<  PrettyDuration(duration) << std::endl;
	multileaves_read = read_multileaves;
	leaves_read = read_leaves;
	elements_read = read_elements;
}

void BUMesh::Initialize()
{
	if (IsInitialized()) return;
    creation_mutex = std::make_shared<std::mutex>();
	Initialized = true;
}

bool BUMesh::IsInitialized()
{
	return Initialized;
}

void BUMesh::CleanUp()
{
	if (!IsInitialized()) return;

	for (auto &bumesh : bumeshes) {
		if (bumesh.initialized) {
			BUMesh::Delete(bumesh.id);
		}
	}
    Initialized = false;
}

/* Static Factory Implementations */
BUMesh* BUMesh::Create(std::string name)
{
	try {
	    auto bumesh =  StaticFactory::Create(creation_mutex, name, "BUMesh", lookupTable, bumeshes, MAX_BUMESHES);
		return bumesh;
	} catch (...) {
		StaticFactory::DeleteIfExists(creation_mutex, name, "BUMesh", lookupTable, bumeshes, MAX_BUMESHES);
		throw;
	}
}

BUMesh* BUMesh::CreateFromDir(std::string name, std::string filebase, bool read_triangle_mesh, bool read_multileaves, bool read_leaves, bool read_elements, int max_parts)
{
	try {
	    auto bumesh =  StaticFactory::Create(creation_mutex, name, "BUMesh", lookupTable, bumeshes, MAX_BUMESHES);
		bumesh->load_from(filebase, read_triangle_mesh, read_multileaves, read_leaves, read_elements, max_parts);
		return bumesh;
	} catch (...) {
		StaticFactory::DeleteIfExists(creation_mutex, name, "BUMesh", lookupTable, bumeshes, MAX_BUMESHES);
		throw;
	}
}

BUMesh* BUMesh::Get(std::string name) {
	return StaticFactory::Get(creation_mutex, name, "BUMesh", lookupTable, bumeshes, MAX_BUMESHES);
}

BUMesh* BUMesh::Get(uint32_t id) {
	return StaticFactory::Get(creation_mutex, id, "BUMesh", lookupTable, bumeshes, MAX_BUMESHES);
}

void BUMesh::Delete(std::string name) {
	StaticFactory::Delete(creation_mutex, name, "BUMesh", lookupTable, bumeshes, MAX_BUMESHES);
	Dirty = true;
}

void BUMesh::Delete(uint32_t id) {
	StaticFactory::Delete(creation_mutex, id, "BUMesh", lookupTable, bumeshes, MAX_BUMESHES);
	Dirty = true;
}

BUMeshStruct* BUMesh::GetFrontStruct() {
	return bumesh_structs;
}

BUMesh* BUMesh::GetFront() {
	return bumeshes;
}

uint32_t BUMesh::GetCount() {
	return MAX_BUMESHES;
}
