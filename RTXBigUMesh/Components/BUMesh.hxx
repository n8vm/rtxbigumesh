// ┌───────────────────────────────────────────────────────────────────────────┐
// │  BUMesh Component                                                         │
// │                                                                           │
// └───────────────────────────────────────────────────────────────────────────┘

#pragma once

#include "RTXBigUMesh/Tools/StaticFactory.hxx"
#include "./BUMeshStruct.hxx"

class BUMesh : public StaticFactory {
    friend class StaticFactory;
private:
    /* Static fields */
	static std::shared_ptr<std::mutex> creation_mutex;
	static bool Initialized;

    static BUMesh bumeshes[MAX_BUMESHES];
    static BUMeshStruct bumesh_structs[MAX_BUMESHES];
    static std::map<std::string, uint32_t> lookupTable;

    BUMesh();
    BUMesh(std::string name, uint32_t id);

    /* Indicates that one of the components has been edited */
    static bool Dirty;

    /* Indicates this component has been edited */
    bool dirty = true;

	void mark_dirty() {
		Dirty = true;
		dirty = true;
	};

    // Many parts in a big unstructured mesh
    std::vector<PartInfo> part_info;
    
    // Multiple of these per part
    std::vector<std::vector<MultiLeaf>> multileaves;
    std::vector<std::vector<glm::vec4>> vertices;
    std::vector<std::vector<glm::u16vec4>> indices;

    std::vector<glm::vec3>       triangle_mesh_vertices;
    std::vector<glm::vec3>       triangle_mesh_colors;
    std::vector<glm::ivec3>      triangle_mesh_indices;

    void load_from(std::string filebase, bool read_triangle_mesh, bool read_multileaves, bool read_leaves, bool read_elements, int max_parts = -1);

public:
    bool multileaves_read = false;
    bool leaves_read = false;
    bool elements_read = false;
    bool triangle_mesh_read = false;    

    static BUMesh* Create(std::string name);
    static BUMesh* CreateFromDir(std::string name, std::string filebase, bool read_triangle_mesh = true, bool read_multileaves = true, bool read_leaves = true, bool read_elements = true, int max_parts = -1);
    static BUMesh* Get(std::string name);
    static BUMesh* Get(uint32_t id);
    static BUMeshStruct* GetFrontStruct();
    static BUMesh* GetFront();
    static uint32_t GetCount();
    static void Delete(std::string name);
    static void Delete(uint32_t id);

    static void Initialize();
	static bool IsInitialized();
    static void CleanUp();	

    std::string to_string();
    BUMeshStruct &get_struct();
    std::vector<PartInfo> &get_part_list();
    std::vector<std::vector<MultiLeaf>> &get_multileaf_lists();
    std::vector<std::vector<glm::vec4>> &get_vertex_lists();
    std::vector<std::vector<glm::u16vec4>> &get_index_lists();

    std::vector<glm::vec3> &get_triangle_mesh_vertices();
    std::vector<glm::vec3> &get_triangle_mesh_colors();
    std::vector<glm::ivec3> &get_triangle_mesh_indices();

    bool multileaves_loaded() {
        return multileaves_read;
    }

    bool leaves_loaded() {
        return leaves_read;
    }

    bool elements_loaded() {
        return elements_read;
    }

    bool triangle_mesh_loaded() {
        return triangle_mesh_read;
    }
};