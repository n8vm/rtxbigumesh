/* File shared by both GLSL and C++ */
#pragma once


#include <stdint.h>
#include <glm/glm.hpp>
#include <owl/owl.h>
#include <cstring>

#ifdef __CUDACC__
#ifndef CUDA_DECORATOR
#define CUDA_DECORATOR __both__
#endif
#else
#ifndef CUDA_DECORATOR
#define CUDA_DECORATOR
#endif
#endif

#define MAX_BUMESHES 1

/* 
   Contains metadata about the BUMesh, 
   eg num attributes, per cell vs per vertex, and so on.
   Attributes, indices and vertices are stored separately.
*/
struct BUMeshStruct
{
	int32_t num_attributes;
	bool data_per_vertex;
};

/* 
   Metadata about a part, composed of many multinodes
*/
struct PartInfo
{
	/* Number of (8-wide) multi-leaf nodes */
	uint32_t node_count;

	/* Number of vec4i's = note that this is NOT the same as num prims,
      because some prims (hexes, pyramids, wedges, ...) may use 
      two vec4i
   */
	uint32_t index_count;

	/* Number of vec4f vertices */
	uint32_t vertex_count;

	/* World space bounds of all prim vertices (+ attribute in w) */
	glm::vec4 bounds_min;
	glm::vec4 bounds_max;
};

/* 
   Internal unstructured mesh nodes, designed to efficiently 
   represent unstructured element and/or node indices 
*/
#define MULTINODE_WIDTH 8
#define MULTINODE_NUM_BITS_TETS 4 // 3 is probably enough ...
#define MULTINODE_NUM_BITS_TOTAL 4

struct MultiLeafChild
{ // 1 bytes
	uint8_t bounds_lo : 4;
	uint8_t bounds_hi : 4;
};

struct MultiLeafDim
{
	MultiLeafChild child[MULTINODE_WIDTH]; // sum:8 bytes
	MultiLeafChild &get_child(int idx)
	{
		if (idx >= MULTINODE_WIDTH)
			throw std::runtime_error("index out of bounds");
		return child[idx];
	}
};

struct MultiLeafPrimCount
{ // 1 bytes
	uint8_t numTets : MULTINODE_NUM_BITS_TETS;
	uint8_t numTotal : MULTINODE_NUM_BITS_TOTAL;
};

struct MultiLeafChildren
{
	MultiLeafDim dim[4];						   // sum:32 bytes
	MultiLeafPrimCount primCount[MULTINODE_WIDTH]; // sum:8 bytes
	MultiLeafDim &get_dim(uint8_t idx)
	{
		if (idx >= 4)
			throw std::runtime_error("index out of bounds");
		return dim[idx];
	}
	MultiLeafPrimCount &get_prim_count(uint8_t idx)
	{
		if (idx >= MULTINODE_WIDTH)
			throw std::runtime_error("index out of bounds");
		return primCount[idx];
	}
};

struct MultiLeaf
{
	enum
	{
		WIDTH = MULTINODE_WIDTH
	};
	enum
	{
		NUM_BITS_TETS = MULTINODE_NUM_BITS_TETS
	};
	enum
	{
		NUM_BITS_TOTAL = MULTINODE_NUM_BITS_TOTAL
	};

	// 60 byte version:

	// bytes 0 .. 16 : quantization domains
	glm::u16vec4 quantBoundsMin; // 4 bytes
	glm::u16vec4 quantBoundsMax; // 4 bytes
	// box4us   quantBounds;

	// bytes 16 .. 20 : child offset pointers
	uint32_t leafChildrenOffset;

	// bytes 24 .. 64 : per-child data: 8x(4x8x1 bounds + 8x1 count = 40 total)
	MultiLeafChildren children; // sum: 32+8 = 40 bytes
};

inline CUDA_DECORATOR void decode_multileaf_bounds(
	// quantized box:
	const glm::u16vec4 &quantized_multileaf_bounds_min,
	const glm::u16vec4 &quantized_multileaf_bounds_max,
	// parent domain that it's relative to:
	const glm::vec4 &parent_part_bounds_min,
	const glm::vec4 &parent_part_bounds_max,
	// return value:
	glm::vec4 &decoded_bounds_min,
	glm::vec4 &decoded_bounds_max)
{
#pragma unroll
	for (int d = 0; d < 4; d++)
	{
		decoded_bounds_min[d] = parent_part_bounds_min[d];
		decoded_bounds_max[d] = parent_part_bounds_max[d];
		if (decoded_bounds_min[d] != decoded_bounds_max[d])
		{
			decoded_bounds_min[d] += quantized_multileaf_bounds_min[d] * (1.f / (1 << 16)) * (parent_part_bounds_max[d] - parent_part_bounds_min[d]);
			decoded_bounds_max[d] -= quantized_multileaf_bounds_max[d] * (1.f / (1 << 16)) * (parent_part_bounds_max[d] - parent_part_bounds_min[d]);
		}
	}
}

inline CUDA_DECORATOR void decode_multileaf_scalar_bounds(
	// quantized scalar bounds:
	const uint16_t &quantized_multileaf_scalar_min,
	const uint16_t &quantized_multileaf_scalar_max,
	// parent scalar domain that it's relative to:
	const float &parent_part_scalar_min,
	const float &parent_part_scalar_max,
	// return value:
	float &decoded_scalar_min,
	float &decoded_scalar_max)
{	
	decoded_scalar_min = parent_part_scalar_min;
	decoded_scalar_max = parent_part_scalar_max;
	if (decoded_scalar_min != decoded_scalar_max)
	{
		decoded_scalar_min += quantized_multileaf_scalar_min * (1.f / (1 << 16)) * (parent_part_scalar_max - parent_part_scalar_min);
		decoded_scalar_max -= quantized_multileaf_scalar_max * (1.f / (1 << 16)) * (parent_part_scalar_max - parent_part_scalar_min);
	}
}

inline CUDA_DECORATOR void decode_leaf_bounds(
	// quantized box:
	const MultiLeafChildren &children,
	const uint8_t cID,
	// parent domain that it's relative to:
	const glm::vec4 &parent_multileaf_bounds_min,
	const glm::vec4 &parent_multileaf_bounds_max,
	// return value:
	glm::vec4 &decoded_bounds_min,
	glm::vec4 &decoded_bounds_max)
{
#pragma unroll
	for (int d = 0; d < 4; d++)
	{
		decoded_bounds_min[d] = parent_multileaf_bounds_min[d];
		decoded_bounds_max[d] = parent_multileaf_bounds_max[d];
		if (decoded_bounds_min[d] != decoded_bounds_max[d])
		{
			decoded_bounds_min[d] += (parent_multileaf_bounds_max[d] - parent_multileaf_bounds_min[d]) * (1.f / (1 << 4)) * children.dim[d].child[cID].bounds_lo;
			decoded_bounds_max[d] -= (parent_multileaf_bounds_max[d] - parent_multileaf_bounds_min[d]) * (1.f / (1 << 4)) * children.dim[d].child[cID].bounds_hi;
		}
	}
}

// -------------------------------------------------------------------------------------------------------------------------------
// Unstructured element interpolation routines
// Each element type has a corresponding interpolate<ELEMENT_TYPE> routine.
// If the point lies within the the element, the interpolate routines return true, and false otherwise.
// Per-vertex interpolated value is returned by reference.
// -------------------------------------------------------------------------------------------------------------------------------
#define TYPE_TETRAHEDRA 0
#define TYPE_PYRAMID 1
#define TYPE_WEDGE 2
#define TYPE_HEXAHEDRA 3

inline CUDA_DECORATOR int
get_type(
  const uint16_t element_indices[8]
)
{
  int type;
  if ((element_indices[6] != 65535) && (element_indices[7] != 65535))
    type = TYPE_HEXAHEDRA;
  else if (element_indices[5] != 65535)
    type = TYPE_WEDGE;
  else if (element_indices[4] != 65535)
    type = TYPE_PYRAMID;
  else
    type = TYPE_TETRAHEDRA;
  return type;
}

// -------------------------------------------------------------------------------------------------------------------------------
// TETRAHEDRA TYPE
// -------------------------------------------------------------------------------------------------------------------------------

/* computes the (oriented) volume of the tet given by the four vertices */
inline CUDA_DECORATOR float
volume(
	const glm::vec3 &P,
	const glm::vec3 &A,
	const glm::vec3 &B,
	const glm::vec3 &C)
{
	return glm::dot(P - A, glm::cross(B - A, C - A));
}

inline CUDA_DECORATOR bool
interpolateTetrahedra(
  const uint16_t element_indices[4],
	const glm::vec4 *vertex_buffer,
	const glm::vec3 &P,
	float &value)
{
	const glm::vec4 V0 = vertex_buffer[element_indices[0]];
	const glm::vec4 V1 = vertex_buffer[element_indices[1]];
	const glm::vec4 V2 = vertex_buffer[element_indices[2]];
	const glm::vec4 V3 = vertex_buffer[element_indices[3]];

	const glm::vec3 P0 = (const glm::vec3 &)V0;
	const glm::vec3 P1 = (const glm::vec3 &)V1;
	const glm::vec3 P2 = (const glm::vec3 &)V2;
	const glm::vec3 P3 = (const glm::vec3 &)V3;

	const float vol_all = volume(/*point*/ P0, /*base-tri*/ P1, P3, P2);
	if (vol_all == 0.f)
		return false;

	const float bary0 = volume(/*point*/ P, /*base-tri*/ P1, P3, P2) / vol_all;
	if (bary0 < 0.f)
		return false;

	const float bary1 = volume(/*point*/ P, /*base-tri*/ P0, P2, P3) / vol_all;
	if (bary1 < 0.f)
		return false;

	const float bary2 = volume(/*point*/ P, /*base-tri*/ P0, P3, P1) / vol_all;
	if (bary2 < 0.f)
		return false;

	const float bary3 = volume(/*point*/ P, /*base-tri*/ P0, P1, P2) / vol_all;
	if (bary3 < 0.f)
		return false;

// If we're running the point sampling benchmark at this point we just
// return true and skip doing the interpolation
#ifdef POINT_QUERY_BENCHMARK
	return true;
#else
	// attributes stored in w component of vertices
	value = (bary0 * V0.w +
			 bary1 * V1.w +
			 bary2 * V2.w +
			 bary3 * V3.w);
	return true;
#endif
}

// -------------------------------------------------------------------------------------------------------------------------------
// PYRAMID TYPE
// -------------------------------------------------------------------------------------------------------------------------------

inline CUDA_DECORATOR void
pyramidInterpolationFunctions(
	float pcoords[3],
	float sf[5])
{
	float rm, sm, tm;

	rm = 1.f - pcoords[0];
	sm = 1.f - pcoords[1];
	tm = 1.f - pcoords[2];

	sf[0] = rm * sm * tm;
	sf[1] = pcoords[0] * sm * tm;
	sf[2] = pcoords[0] * pcoords[1] * tm;
	sf[3] = rm * pcoords[1] * tm;
	sf[4] = pcoords[2];
}

inline CUDA_DECORATOR void
pyramidInterpolationDerivs(
	float pcoords[3],
	float derivs[15])
{
	// r-derivatives
	derivs[0] = -(pcoords[1] - 1.f) * (pcoords[2] - 1.f);
	derivs[1] = (pcoords[1] - 1.f) * (pcoords[2] - 1.f);
	derivs[2] = pcoords[1] - pcoords[1] * pcoords[2];
	derivs[3] = pcoords[1] * (pcoords[2] - 1.f);
	derivs[4] = 0.f;

	// s-derivatives
	derivs[5] = -(pcoords[0] - 1.f) * (pcoords[2] - 1.f);
	derivs[6] = pcoords[0] * (pcoords[2] - 1.f);
	derivs[7] = pcoords[0] - pcoords[0] * pcoords[2];
	derivs[8] = (pcoords[0] - 1.f) * (pcoords[2] - 1.f);
	derivs[9] = 0.f;

	// t-derivatives
	derivs[10] = -(pcoords[0] - 1.f) * (pcoords[1] - 1.f);
	derivs[11] = pcoords[0] * (pcoords[1] - 1.f);
	derivs[12] = -pcoords[0] * pcoords[1];
	derivs[13] = (pcoords[0] - 1.f) * pcoords[1];
	derivs[14] = 1.f;
}

#define PYRAMID_DIVERGED 1.e6f
#define PYRAMID_MAX_ITERATION 10
#define PYRAMID_CONVERGED 1.e-04f
#define PYRAMID_OUTSIDE_CELL_TOLERANCE 1.e-06f

inline CUDA_DECORATOR bool
interpolatePyramid(
  const uint16_t element_indices[5],
	const glm::vec4 *vertex_buffer,
	const glm::vec3 &P,
	float &value)
{
	const glm::vec4 V0 = vertex_buffer[element_indices[0]];
	const glm::vec4 V1 = vertex_buffer[element_indices[1]];
	const glm::vec4 V2 = vertex_buffer[element_indices[2]];
	const glm::vec4 V3 = vertex_buffer[element_indices[3]];
	const glm::vec4 V4 = vertex_buffer[element_indices[4]];

	const glm::vec3 b0 = (const glm::vec3 &)V0;
	const glm::vec3 b1 = (const glm::vec3 &)V1;
	const glm::vec3 b2 = (const glm::vec3 &)V2;
	const glm::vec3 b3 = (const glm::vec3 &)V3;
	const glm::vec3 t0 = (const glm::vec3 &)V4;

	float params[3] = {0.5, 0.5, 0.5};
	float pcoords[3] = {0.5, 0.5, 0.5};
	float derivs[15];
	float weights[5];

	glm::vec3 vertices[5] = {b0, b1, b2, b3, t0};

	memset(derivs, 0, sizeof(derivs));
	memset(weights, 0, sizeof(weights));

	const int edges[8][2] = {{0, 1}, {1, 2}, {2, 3}, {3, 0}, {0, 4}, {1, 4}, {2, 4}, {3, 4}};

	float longestEdge = 0;
	for (int i = 0; i < 8; i++)
	{
		glm::vec3 p0 = vertices[edges[i][0]];
		glm::vec3 p1 = vertices[edges[i][1]];

		float dist = glm::length(p1 - p0);
		if (longestEdge < dist)
			longestEdge = dist;
	}
	float volumeBound = pow(longestEdge, 3);
	float determinantTolerance =
		1e-20f < .00001f * volumeBound ? 1e-20f : .00001f * volumeBound;

	//  Enter iteration loop
	bool converged = false;
	for (int iteration = 0; !converged && (iteration < PYRAMID_MAX_ITERATION); iteration++)
	{
		//  calculate element interpolation functions and derivatives
		pyramidInterpolationFunctions(pcoords, weights);
		pyramidInterpolationDerivs(pcoords, derivs);
		//  calculate newton functions
		glm::vec3 fcol = glm::vec3(0.f, 0.f, 0.f);
		glm::vec3 rcol = glm::vec3(0.f, 0.f, 0.f);
		glm::vec3 scol = glm::vec3(0.f, 0.f, 0.f);
		glm::vec3 tcol = glm::vec3(0.f, 0.f, 0.f);
		for (int i = 0; i < 5; i++)
		{
			glm::vec3 pt = (glm::vec3)vertices[i];
			fcol = fcol + pt * weights[i];
			rcol = rcol + pt * derivs[i];
			scol = scol + pt * derivs[i + 5];
			tcol = tcol + pt * derivs[i + 10];
		}
		fcol = fcol - P;
		// compute determinants and generate improvements
		//  float d = gdt::LinearSpace3f(rcol, scol, tcol).det();
		float d = glm::determinant(glm::mat3(rcol, scol, tcol)); // gdt::LinearSpace3f(rcol, scol, tcol).det();
		if (fabs(d) < determinantTolerance)
		{
			return false;
		}
		pcoords[0] = params[0] - glm::determinant(glm::mat3(fcol, scol, tcol)) / d; //gdt::LinearSpace3f(fcol, scol, tcol).det() / d;
		pcoords[1] = params[1] - glm::determinant(glm::mat3(rcol, fcol, tcol)) / d; //gdt::LinearSpace3f(rcol, fcol, tcol).det() / d;
		pcoords[2] = params[2] - glm::determinant(glm::mat3(rcol, scol, fcol)) / d; //gdt::LinearSpace3f(rcol, scol, fcol).det() / d;
		// convergence/divergence test - if neither, repeat
		if (((fabs(pcoords[0] - params[0])) < PYRAMID_CONVERGED) &&
			((fabs(pcoords[1] - params[1])) < PYRAMID_CONVERGED) &&
			((fabs(pcoords[2] - params[2])) < PYRAMID_CONVERGED))
		{
			converged = true;
		}
		else if ((fabs(pcoords[0]) > PYRAMID_DIVERGED) ||
				 (fabs(pcoords[1]) > PYRAMID_DIVERGED) ||
				 (fabs(pcoords[2]) > PYRAMID_DIVERGED))
		{
			return false;
		}
		else
		{
			params[0] = pcoords[0];
			params[1] = pcoords[1];
			params[2] = pcoords[2];
		}
	}
	if (!converged)
	{
		return false;
	}
	float attrs[5];
	attrs[0] = V0.w;
	attrs[1] = V1.w;
	attrs[2] = V2.w;
	attrs[3] = V3.w;
	attrs[4] = V4.w;

	float lowerlimit = 0.0f - PYRAMID_OUTSIDE_CELL_TOLERANCE;
	float upperlimit = 1.0f + PYRAMID_OUTSIDE_CELL_TOLERANCE;
	if (pcoords[0] >= lowerlimit && pcoords[0] <= upperlimit &&
		pcoords[1] >= lowerlimit && pcoords[1] <= upperlimit &&
		pcoords[2] >= lowerlimit && pcoords[2] <= upperlimit)
	{
		// evaluation
		value = 0.f;
		pyramidInterpolationFunctions(pcoords, weights);
		for (int i = 0; i < 5; i++)
		{
			value += weights[i] * attrs[i];
		}
		return true;
	}
	return false;
}

// -------------------------------------------------------------------------------------------------------------------------------
// WEDGE TYPE
// -------------------------------------------------------------------------------------------------------------------------------
inline CUDA_DECORATOR void
wedgeInterpolationFunctions(
	float pcoords[3],
	float sf[6])
{
	sf[0] = (1.0f - pcoords[0] - pcoords[1]) * (1.0f - pcoords[2]);
	sf[1] = pcoords[0] * (1.0f - pcoords[2]);
	sf[2] = pcoords[1] * (1.0f - pcoords[2]);
	sf[3] = (1.0f - pcoords[0] - pcoords[1]) * pcoords[2];
	sf[4] = pcoords[0] * pcoords[2];
	sf[5] = pcoords[1] * pcoords[2];
}

inline CUDA_DECORATOR void
wedgeInterpolationDerivs(
	float pcoords[3],
	float derivs[18])
{
	// r-derivatives
	derivs[0] = -1.0f + pcoords[2];
	derivs[1] = 1.0f - pcoords[2];
	derivs[2] = 0.0;
	derivs[3] = -pcoords[2];
	derivs[4] = pcoords[2];
	derivs[5] = 0.0;

	// s-derivatives
	derivs[6] = -1.0f + pcoords[2];
	derivs[7] = 0.0f;
	derivs[8] = 1.0f - pcoords[2];
	derivs[9] = -pcoords[2];
	derivs[10] = 0.0f;
	derivs[11] = pcoords[2];

	// t-derivatives
	derivs[12] = -1.0f + pcoords[0] + pcoords[1];
	derivs[13] = -pcoords[0];
	derivs[14] = -pcoords[1];
	derivs[15] = 1.0f - pcoords[0] - pcoords[1];
	derivs[16] = pcoords[0];
	derivs[17] = pcoords[1];
}

#define WEDGE_DIVERGED 1.e6f
#define WEDGE_MAX_ITERATION 10
#define WEDGE_CONVERGED 1.e-05f
#define WEDGE_OUTSIDE_CELL_TOLERANCE 1.e-06f

inline CUDA_DECORATOR bool
interpolateWedge(
  const uint16_t element_indices[6],
	const glm::vec4 *vertex_buffer,
	const glm::vec3 &P,
	float &value)
{
  // value = .5f; return true;
  const glm::vec4 V0 = vertex_buffer[element_indices[0]];
	const glm::vec4 V1 = vertex_buffer[element_indices[1]];
	const glm::vec4 V2 = vertex_buffer[element_indices[2]];
	const glm::vec4 V3 = vertex_buffer[element_indices[3]];
	const glm::vec4 V4 = vertex_buffer[element_indices[4]];
	const glm::vec4 V5 = vertex_buffer[element_indices[5]];

	const glm::vec3 b0 = (const glm::vec3 &)V0;
	const glm::vec3 b1 = (const glm::vec3 &)V1;
	const glm::vec3 b2 = (const glm::vec3 &)V2;
	const glm::vec3 t0 = (const glm::vec3 &)V3;
	const glm::vec3 t1 = (const glm::vec3 &)V4;
	const glm::vec3 t2 = (const glm::vec3 &)V5;

	float params[3] = {0.5f, 0.5f, 0.5f};
	float pcoords[3] = {0.5f, 0.5f, 0.5f};
	float derivs[18];
	float weights[6];

	glm::vec3 vertices[6] = {b0, b1, b2, t0, t1, t2};

	memset(derivs, 0, sizeof(derivs));
	memset(weights, 0, sizeof(weights));

	const int edges[9][2] = {{0, 1}, {1, 2}, {2, 0}, {3, 4}, {4, 5}, {5, 3}, {0, 3}, {1, 4}, {2, 5}};
	float longestEdge = 0;
	for (int i = 0; i < 9; i++)
	{
		glm::vec3 p0 = vertices[edges[i][0]];
		glm::vec3 p1 = vertices[edges[i][1]];

		float dist = glm::length(p1 - p0);
		if (longestEdge < dist)
			longestEdge = dist;
	}

	float volumeBound = pow(longestEdge, 3);
	float determinantTolerance =
		1e-20f < .00001f * volumeBound ? 1e-20f : .00001f * volumeBound;

	//  enter iteration loop
	bool converged = false;
	for (int iteration = 0; !converged && (iteration < WEDGE_MAX_ITERATION); iteration++)
	{
		//  calculate element interpolation functions and derivatives
		wedgeInterpolationFunctions(pcoords, weights);
		wedgeInterpolationDerivs(pcoords, derivs);

		//  calculate newton functions
		glm::vec3 fcol = glm::vec3(0.f, 0.f, 0.f);
		glm::vec3 rcol = glm::vec3(0.f, 0.f, 0.f);
		glm::vec3 scol = glm::vec3(0.f, 0.f, 0.f);
		glm::vec3 tcol = glm::vec3(0.f, 0.f, 0.f);
		for (int i = 0; i < 6; i++)
		{
			glm::vec3 pt = vertices[i];
			fcol = fcol + pt * weights[i];
			rcol = rcol + pt * derivs[i];
			scol = scol + pt * derivs[i + 6];
			tcol = tcol + pt * derivs[i + 12];
		}

		fcol = fcol - P;

		// compute determinants and generate improvements
		float d = glm::determinant(glm::mat3(rcol, scol, tcol));
		if (fabs(d) < determinantTolerance)
		{
			return false;
		}
		pcoords[0] = params[0] - glm::determinant(glm::mat3(fcol, scol, tcol)) / d;
		pcoords[1] = params[1] - glm::determinant(glm::mat3(rcol, fcol, tcol)) / d;
		pcoords[2] = params[2] - glm::determinant(glm::mat3(rcol, scol, fcol)) / d;

		// convergence/divergence test - if neither, repeat
		if (((fabs(pcoords[0] - params[0])) < WEDGE_CONVERGED) &&
			((fabs(pcoords[1] - params[1])) < WEDGE_CONVERGED) &&
			((fabs(pcoords[2] - params[2])) < WEDGE_CONVERGED))
		{
			converged = true;
		}
		else if ((fabs(pcoords[0]) > WEDGE_DIVERGED) ||
				 (fabs(pcoords[1]) > WEDGE_DIVERGED) ||
				 (fabs(pcoords[2]) > WEDGE_DIVERGED))
		{
			return false;
		}
		else
		{
			params[0] = pcoords[0];
			params[1] = pcoords[1];
			params[2] = pcoords[2];
		}
	}

	if (!converged)
	{
		return false;
	}

	float attrs[6];
	attrs[0] = V0.w;
	attrs[1] = V1.w;
	attrs[2] = V2.w;
	attrs[3] = V3.w;
	attrs[4] = V4.w;
	attrs[5] = V5.w;

	float lowerlimit = 0.0f - WEDGE_OUTSIDE_CELL_TOLERANCE;
	float upperlimit = 1.0f + WEDGE_OUTSIDE_CELL_TOLERANCE;
	if (pcoords[0] >= lowerlimit && pcoords[0] <= upperlimit &&
		pcoords[1] >= lowerlimit && pcoords[1] <= upperlimit &&
		pcoords[2] >= lowerlimit && pcoords[2] <= upperlimit &&
		pcoords[0] + pcoords[1] <= upperlimit)
	{
		// evaluation
		value = 0.f;
		wedgeInterpolationFunctions(pcoords, weights);
		for (int i = 0; i < 6; i++)
		{
			value += weights[i] * attrs[i];
		}

		// value = .5;
		return true;
	}
	return false;
}

// -------------------------------------------------------------------------------------------------------------------------------
// HEXAHEDRA TYPE
// -------------------------------------------------------------------------------------------------------------------------------
inline CUDA_DECORATOR void
hexInterpolationFunctions(
	float pcoords[3],
	float sf[8])
{
	float rm, sm, tm;

	rm = 1.f - pcoords[0];
	sm = 1.f - pcoords[1];
	tm = 1.f - pcoords[2];

	sf[0] = rm * sm * tm;
	sf[1] = pcoords[0] * sm * tm;
	sf[2] = pcoords[0] * pcoords[1] * tm;
	sf[3] = rm * pcoords[1] * tm;
	sf[4] = rm * sm * pcoords[2];
	sf[5] = pcoords[0] * sm * pcoords[2];
	sf[6] = pcoords[0] * pcoords[1] * pcoords[2];
	sf[7] = rm * pcoords[1] * pcoords[2];
}

inline CUDA_DECORATOR void
hexInterpolationDerivs(
	float pcoords[3],
	float derivs[24])
{
	float rm, sm, tm;

	rm = 1.f - pcoords[0];
	sm = 1.f - pcoords[1];
	tm = 1.f - pcoords[2];

	// r-derivatives
	derivs[0] = -sm * tm;
	derivs[1] = sm * tm;
	derivs[2] = pcoords[1] * tm;
	derivs[3] = -pcoords[1] * tm;
	derivs[4] = -sm * pcoords[2];
	derivs[5] = sm * pcoords[2];
	derivs[6] = pcoords[1] * pcoords[2];
	derivs[7] = -pcoords[1] * pcoords[2];

	// s-derivatives
	derivs[8] = -rm * tm;
	derivs[9] = -pcoords[0] * tm;
	derivs[10] = pcoords[0] * tm;
	derivs[11] = rm * tm;
	derivs[12] = -rm * pcoords[2];
	derivs[13] = -pcoords[0] * pcoords[2];
	derivs[14] = pcoords[0] * pcoords[2];
	derivs[15] = rm * pcoords[2];

	// t-derivatives
	derivs[16] = -rm * sm;
	derivs[17] = -pcoords[0] * sm;
	derivs[18] = -pcoords[0] * pcoords[1];
	derivs[19] = -rm * pcoords[1];
	derivs[20] = rm * sm;
	derivs[21] = pcoords[0] * sm;
	derivs[22] = pcoords[0] * pcoords[1];
	derivs[23] = rm * pcoords[1];
}

#define HEX_DIVERGED 1.e6f
#define HEX_MAX_ITERATION 10
#define HEX_CONVERGED 1.e-05f
#define HEX_OUTSIDE_CELL_TOLERANCE 1.e-06f

inline CUDA_DECORATOR bool
interpolateHexahedra(
  const uint16_t element_indices[8],
	const glm::vec4 *vertex_buffer,
	const glm::vec3 &P,
	float &value)
{
	const glm::vec4 V0 = vertex_buffer[element_indices[0]];
	const glm::vec4 V1 = vertex_buffer[element_indices[1]];
	const glm::vec4 V2 = vertex_buffer[element_indices[2]];
	const glm::vec4 V3 = vertex_buffer[element_indices[3]];
	const glm::vec4 V4 = vertex_buffer[element_indices[4]];
	const glm::vec4 V5 = vertex_buffer[element_indices[5]];
	const glm::vec4 V6 = vertex_buffer[element_indices[6]];
	const glm::vec4 V7 = vertex_buffer[element_indices[7]];

	const glm::vec3 b0 = (const glm::vec3 &)V0;
	const glm::vec3 b1 = (const glm::vec3 &)V1;
	const glm::vec3 b2 = (const glm::vec3 &)V2;
	const glm::vec3 b3 = (const glm::vec3 &)V3;
	const glm::vec3 t0 = (const glm::vec3 &)V4;
	const glm::vec3 t1 = (const glm::vec3 &)V5;
	const glm::vec3 t2 = (const glm::vec3 &)V6;
	const glm::vec3 t3 = (const glm::vec3 &)V7;

	float params[3] = {0.5, 0.5, 0.5};
	float pcoords[3];
	float derivs[24];
	float weights[8];

	glm::vec3 vertices[8] = {b0, b1, b2, b3, t0, t1, t2, t3};

	// Should precompute these
	const int diagonals[4][2] = {{0, 6}, {1, 7}, {2, 4}, {3, 5}};
	float longestDiagonal = 0;
	for (int i = 0; i < 4; i++)
	{
		const glm::vec3 p0 = vertices[diagonals[i][0]];
		const glm::vec3 p1 = vertices[diagonals[i][1]];
		float dist = glm::length(p1 - p0);
		if (longestDiagonal < dist)
			longestDiagonal = dist;
	}

	const float volumeBound = longestDiagonal * longestDiagonal * longestDiagonal;
	const float determinantTolerance =
		1e-20f < .00001f * volumeBound ? 1e-20f : .00001f * volumeBound;

	// Set initial position for Newton's method
	pcoords[0] = pcoords[1] = pcoords[2] = 0.5;

	// Enter iteration loop
	bool converged = false;
	for (int iteration = 0; !converged && (iteration < HEX_MAX_ITERATION); iteration++)
	{
		// Calculate element interpolation functions and derivatives
		hexInterpolationFunctions(pcoords, weights);
		hexInterpolationDerivs(pcoords, derivs);

		// Calculate newton functions
		glm::vec3 fcol = glm::vec3(0.f, 0.f, 0.f);
		glm::vec3 rcol = glm::vec3(0.f, 0.f, 0.f);
		glm::vec3 scol = glm::vec3(0.f, 0.f, 0.f);
		glm::vec3 tcol = glm::vec3(0.f, 0.f, 0.f);
		for (int i = 0; i < 8; i++)
		{
			const glm::vec3 pt = vertices[i];
			fcol = fcol + pt * weights[i];
			rcol = rcol + pt * derivs[i];
			scol = scol + pt * derivs[i + 8];
			tcol = tcol + pt * derivs[i + 16];
		}

		fcol = fcol - P;

		// Compute determinants and generate improvements
		const float d = glm::determinant(glm::mat3(rcol, scol, tcol));
		if (fabs(d) < determinantTolerance)
		{
			return false;
		}

		pcoords[0] = params[0] - glm::determinant(glm::mat3(fcol, scol, tcol)) / d;
		pcoords[1] = params[1] - glm::determinant(glm::mat3(rcol, fcol, tcol)) / d;
		pcoords[2] = params[2] - glm::determinant(glm::mat3(rcol, scol, fcol)) / d;

		// Convergence/divergence test - if neither, repeat
		if (((fabs(pcoords[0] - params[0])) < HEX_CONVERGED) &&
			((fabs(pcoords[1] - params[1])) < HEX_CONVERGED) &&
			((fabs(pcoords[2] - params[2])) < HEX_CONVERGED))
		{
			converged = true;
		}
		else if ((fabs(pcoords[0]) > HEX_DIVERGED) ||
				 (fabs(pcoords[1]) > HEX_DIVERGED) ||
				 (fabs(pcoords[2]) > HEX_DIVERGED))
		{
			return false;
		}
		else
		{
			params[0] = pcoords[0];
			params[1] = pcoords[1];
			params[2] = pcoords[2];
		}
	}

	if (!converged)
	{
		return false;
	}

	float attrs[8];
	attrs[0] = V0.w;
	attrs[1] = V1.w;
	attrs[2] = V2.w;
	attrs[3] = V3.w;
	attrs[4] = V4.w;
	attrs[5] = V5.w;
	attrs[6] = V6.w;
	attrs[7] = V7.w;

	const float lowerlimit = 0.0f - HEX_OUTSIDE_CELL_TOLERANCE;
	const float upperlimit = 1.0f + HEX_OUTSIDE_CELL_TOLERANCE;
	if ((pcoords[0] >= lowerlimit && pcoords[0] <= upperlimit &&
		 pcoords[1] >= lowerlimit && pcoords[1] <= upperlimit &&
		 pcoords[2] >= lowerlimit && pcoords[2] <= upperlimit))
	{
		// Evaluation
		value = 0.f;
		hexInterpolationFunctions(pcoords, weights);
		for (int i = 0; i < 8; i++)
		{
			value += weights[i] * attrs[i];
		}

		return true;
	}
	return false;
}

// indices set to -1 at the end of element_indices are used to detect element type.
// 4 -1's at the end represent a tetrahedra, 3 represent a pyramid, 2 represent a wedge, 
// and none represent a hex
inline CUDA_DECORATOR bool
interpolateElement(
    const uint16_t element_indices[8],
    const glm::vec4 *vertex_buffer,
    const glm::vec3 &P,
    float &value)
{
  int type = get_type(element_indices);
  if (type == TYPE_TETRAHEDRA)        return interpolateTetrahedra(element_indices, vertex_buffer, P, value);
  else if (type == TYPE_PYRAMID)      return interpolatePyramid(element_indices, vertex_buffer, P, value);
  else if (type == TYPE_WEDGE)        return interpolateWedge(element_indices, vertex_buffer, P, value);
  else if (type == TYPE_HEXAHEDRA)    return interpolateHexahedra(element_indices, vertex_buffer, P, value);
  else return false;   
}
