#pragma once

#include "./TemplateComponent.hxx"

TemplateComponent TemplateComponent::template_components[MAX_TEMPLATE_COMPONENTS];
TemplateComponentStruct TemplateComponent::template_component_structs[MAX_TEMPLATE_COMPONENTS];
std::map<std::string, uint32_t> TemplateComponent::lookupTable;
std::shared_ptr<std::mutex> TemplateComponent::creation_mutex;
bool TemplateComponent::Initialized = false;
bool TemplateComponent::Dirty = true;

TemplateComponent::TemplateComponent() {
	this->initialized = false;
}

TemplateComponent::TemplateComponent(std::string name, uint32_t id) {
	this->initialized = true;
	this->name = name;
	this->id = id;
    template_component_structs[id].data_per_vertex = false;
    template_component_structs[id].num_attributes = 0;
}

std::string TemplateComponent::to_string()
{
	std::string output;
	output += "{\n";
	output += "\ttype: \"TemplateComponent\",\n";
	output += "\tname: \"" + name + "\",\n";
	output += "\tid: \"" + std::to_string(id) + "\",\n";
	output += "}";
	return output;
}

TemplateComponentStruct &TemplateComponent::get_struct() {
	return template_component_structs[id];
}

void TemplateComponent::Initialize()
{
	if (IsInitialized()) return;
    creation_mutex = std::make_shared<std::mutex>();
	Initialized = true;
}

bool TemplateComponent::IsInitialized()
{
	return Initialized;
}

void TemplateComponent::CleanUp()
{
	if (!IsInitialized()) return;

	for (auto &template_component : template_components) {
		if (template_component.initialized) {
			TemplateComponent::Delete(template_component.id);
		}
	}
    Initialized = false;
}

/* Static Factory Implementations */
TemplateComponent* TemplateComponent::Create(std::string name)
{
	try {
	    auto template_component =  StaticFactory::Create(creation_mutex, name, "TemplateComponent", lookupTable, template_components, MAX_TEMPLATE_COMPONENTS);
		return template_component;
	} catch (...) {
		StaticFactory::DeleteIfExists(creation_mutex, name, "TemplateComponent", lookupTable, template_components, MAX_TEMPLATE_COMPONENTS);
		throw;
	}
}

TemplateComponent* TemplateComponent::Get(std::string name) {
	return StaticFactory::Get(creation_mutex, name, "TemplateComponent", lookupTable, template_components, MAX_TEMPLATE_COMPONENTS);
}

TemplateComponent* TemplateComponent::Get(uint32_t id) {
	return StaticFactory::Get(creation_mutex, id, "TemplateComponent", lookupTable, template_components, MAX_TEMPLATE_COMPONENTS);
}

void TemplateComponent::Delete(std::string name) {
	StaticFactory::Delete(creation_mutex, name, "TemplateComponent", lookupTable, template_components, MAX_TEMPLATE_COMPONENTS);
	Dirty = true;
}

void TemplateComponent::Delete(uint32_t id) {
	StaticFactory::Delete(creation_mutex, id, "TemplateComponent", lookupTable, template_components, MAX_TEMPLATE_COMPONENTS);
	Dirty = true;
}

TemplateComponentStruct* TemplateComponent::GetFrontStruct() {
	return template_component_structs;
}

TemplateComponent* TemplateComponent::GetFront() {
	return template_components;
}

uint32_t TemplateComponent::GetCount() {
	return MAX_TEMPLATE_COMPONENTS;
}
