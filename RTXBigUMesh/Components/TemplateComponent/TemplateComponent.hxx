// ┌───────────────────────────────────────────────────────────────────────────┐
// │  TemplateComponent                                                        │
// │                                                                           │
// └───────────────────────────────────────────────────────────────────────────┘

#pragma once

#include "RTXBigUMesh/Tools/StaticFactory.hxx"
#include "./TemplateComponentStruct.hxx"

class TemplateComponent : public StaticFactory {
    friend class StaticFactory;
private:
    /* Static fields */
	static std::shared_ptr<std::mutex> creation_mutex;
	static bool Initialized;

    static TemplateComponent template_components[MAX_BUMESHES];
    static TemplateComponentStruct template_component_structs[MAX_BUMESHES];
    static std::map<std::string, uint32_t> lookupTable;

    TemplateComponent();
    TemplateComponent(std::string name, uint32_t id);

    /* Indicates that one of the components has been edited */
    static bool Dirty;

    /* Indicates this component has been edited */
    bool dirty = true;

	void mark_dirty() {
		Dirty = true;
		dirty = true;
	};
public:
    static TemplateComponent* Create(std::string name);
    static TemplateComponent* Get(std::string name);
    static TemplateComponent* Get(uint32_t id);
    static TemplateComponentStruct* GetFrontStruct();
    static TemplateComponent* GetFront();
    static uint32_t GetCount();
    static void Delete(std::string name);
    static void Delete(uint32_t id);

    static void Initialize();
	static bool IsInitialized();
    static void CleanUp();	

    std::string to_string();
    TemplateComponentStruct &get_struct();
};