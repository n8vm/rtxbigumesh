/* File shared by both host and device */
#pragma once

#include <stdint.h>
#define MAX_TEMPLATE_COMPONENTS 32

/* 
   Contains data that would be uploaded in an array to the GPU
*/
struct TemplateComponentStruct {
	int32_t data;
};